#include <bits/stdc++.h>
#include <cstring>
// #include <iostream>
// #include <algorithm>
// int a = INT_MAX;//当用这个定义最大值时再加一个数会溢出
// int a = 0x3f3f3f3f;//四个3f不可缺少//尽量用这个形式去定义最大值 0x3f * 2 = INT_MAX;这两个是一个量级的数都是1e9;
//a * a = a^2^1//(a * a) ^ 2 = a ^ 2 ^ 2;.......
//row -- 行
//col -- 列
//1既不是质数也不是合数
//#include <cmath> arccos(-1) = PI;
//当题目规定时间为1s时计算量在1e8以内就可以过
//整数向上取整//x = (a - 1) / b + 1;
//小数向下取整//x = int(a);
//小数向上取
//x = ceil(a);
//__gcd(a, b) 返回a与b的最大公约数
//a * b / __gcd(a, b) 返回a与b的最小公倍数
//求最大公约数模板 int gcd(int a, int b){return b ? gcd(b, a % b) : a;}
//int a = x >> i & 1//取x的第i位的二进制数是什么

//高精度加法
// using namespace std;

// vector<int> add(vector<int> & A, vector<int> &B){

// }

// int main(){
//     string a, b;
//     cin >> a >> b;

//     vector<int> A, B;
//     for(int i = a.size() - 1; i >= 0; i --) A.push_back(a[i] - '0');

//     return 0;
// }
//走迷宫
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e3;

// int n, m;
// int a[N][N];
// PII q[N * N], Prev[N][N];
// int d[N][N];

// void print(int a, int b){
//     if(a != 0 && b != 0) print(Prev[a][b].first, Prev[a][b].second);
//     printf("%d %d\n", a, b);
// }

// void bfs(){
//     int hh = 0, tt = 0;
//     q[0] = {0, 0};
//     memset(d, -1, sizeof d);

//     d[0][0] = 0;

//     while(hh <= tt){
//         PII t = q[hh ++];

//         int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

//         for(int i = 0; i < 4; i ++){
//             int x = t.first + dx[i], y = t.second + dy[i];
            
//             if(x >= 0 && x < n && y >= 0 && y < m && d[x][y] == -1 && a[x][y] == 0){
//                 d[x][y] = d[t.first][t.second] + 1;
//                 Prev[x][y] = t;
//                 q[++ tt] = {x, y};
//             }
//         }
//     }

//     int x = n - 1, y = m - 1;
//     while(x || y){
//         cout << x << " " <<  y << endl;
//         x = Prev[x][y].first;
//         y = Prev[x][y].second;
//     }
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 0; i < n; i ++){
//         for(int j = 0; j < m; j ++){
//             cin >> a[i][j];
//         }
//     }   

//     bfs();
    
//     print(n - 1, m - 1);

//     cout << d[n - 1][m - 1];
//     return 0;
// }
//spfa求最短路径
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int d[N];
// int h[N], e[N], w[N], ne[N], idx = 0;
// bool st[N];

// void add(int a, int b, int c){
//     e[idx] = b; w[idx] = c; ne[idx] = h[a]; h[a] = idx ++;
// }

// void spfa(){
//     memset(d, 0x3f, sizeof d);
//     queue<int> q;
//     d[1] = 0;

//     st[1] = true;

//     q.push(1);

//     while(q.size()){
//         int t = q.front();
//         q.pop();

//         st[t] = false;

//         for(int i = h[t]; i != -1; i = ne[i]){
//             int j = e[i];

//             if(d[j] > d[t] + w[i]){
//                 d[j] = d[t] + w[i];
//                 if(!st[j]){
//                     q.push(j);
//                     st[j] = true;
//                 }
//             }
//         }
//     }
// }

// int main(){
//     cin >> n >> m;

//     memset(h, -1, sizeof h);

//     while(m --){
//         int a, b, c;
//         cin >> a >> b >> c;
//         add(a, b, c);
//     }

//     spfa();

//     if(d[n] == 0x3f3f3f3f) printf("impossible");
//     else printf("%d", d[n]);
    
//     return 0;
// }
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e5 + 10;

// int h[N], e[N], w[N], ne[N], idx = 0;
// int d[N];
// int n, m;
// bool st[N];

// void add(int a, int b, int c){
//     e[idx] = b; w[idx] = c;ne[idx] = h[a];h[a] = idx ++;
// }

// int spfa(){
//     queue<PII> q;
//     memset(d, 0x3f, sizeof d);
//     d[1] = 0;
//     q.push({0, 1});
//     st[1] = true;

//     while(q.size()){
//         PII p = q.front();
//         q.pop();
//         int t = p.second;
//         st[t] = false;
//         for(int i = h[t]; i != -1; i = ne[i]){
//             int j = e[i];
//             if(d[j] > d[t] + w[i]){
//                 d[j] = d[t] + w[i];
//                 if(!st[j]){
//                     st[j] = true;
//                     q.push({d[j], j});
//                 }
//             }
//         }
//     }


// }

// int main(){
//     cin >> n >> m;

//     memset(h, -1, sizeof h);
//     while(m --){
//         int a, b, c;
//         cin >> a >> b >> c;

//         add(a, b, c);
//     }

//     int res = spfa();

//     if(res == -1) printf("impossible");
//     else printf("%d", res);

//     return 0;
// }
//有边数限制的最短路问题--bellman_ford
// using namespace std;

// const int N = 1e5 + 10;

// struct edg{
//     int a, b, c;
// }e[N];

// int n, m, k;
// int d[N];
// int back[N];

// void bellman_ford(){

//     memset(d, 0x3f3f3f3f, sizeof d);

//     d[1] = 0;

//     for(int i = 1; i <= k; i ++){

//         memcpy(back, d, sizeof d);

//         for(int j = 1; j <= m; j ++){
//             d[e[j]. b] = min(d[e[j].b], back[e[j].a] + e[j].c);
//         }
//     }

// }

// int main(){
//     cin >> n >> m >> k;

//     for(int i = 1; i <= m; i ++) cin >> e[i].a >> e[i].b >> e[i].c;

//     bellman_ford();

//     if(d[n] > 0x3f3f3f3f / 2) printf("impossible");
//     else printf("%d", d[n]);

//     return 0;
// }
//5倍经验日--变种的01背包
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// int lose[N], win[N], use[N];
// int dp[N];

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> lose[i] >> win[i] >> use[i];

//     for(int i = 1; i <= m; i ++){
//         for(int j = 1; j <= n; j ++){
//             if(i >= use[j]){
//                 dp[i] = max(dp[i - use[j]] + win[j], dp[i] + lose[j]);
//             }else{
//                 dp[i] = dp[i] + lose[j];
//             }
//         }
//     }
//     return 0;
// }
//最大子段和
// using namespace std;

// const int N = 2e5 + 10;

// int n;
// int a[N];
// int dp[N];

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     int sum = -10001;

//     for(int i = 1; i <= n; i ++){
//         dp[i] = max(a[i], dp[i - 1] + a[i]);
//         sum = max(sum, dp[i]);
//     }

//     cout << sum;
//     return 0;
// }
//kkksc03临时抱佛脚
// using namespace std;

// const int N = 30;

// int x1, x2, x3, x4;
// int a[N], b[N], c[N], d[N];

// int main(){
//     cin >> x1 >> x2 >> x3 >> x4;

//     for(int i = 1; i <= x1; i ++) cin >> a[i];

//     for(int i = 1; i <= x2; i ++) cin >> b[i];

//     for(int i = 1; i <= x3; i ++) cin >> c[i];

//     for(int i = 1; i <= x4; i ++) cin >> d[i];

//     sort(a + 1, a + x1 + 1);
//     sort(b + 1, b + x2 + 1);
//     sort(c + 1, c + x3 + 1);
//     sort(d + 1, d + x4 + 1);
 
//     if(x1 > 2){
//         sum += a[x1];
//         for(int i = 1; i <= x1 - 2; i ++){
//             sum += a[i];
//         }
//     }
    
//     return 0;
// }
//选数
// using namespace std;

// const int N = 5e6 + 10;

// int n, k;
// int a[N];

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n ; i++){
//         for(int j = i + 1; j <= n; j ++){
//             for(int k = j + 1; k <= n; k ++){

//             }
//         }
//     }
//     return 0;
// }
//最大食物链计数
// using namespace std;

// const int N = 5e3 + 10;

// int n, m;
// int dp[N];
// int a[N][N];

// int main(){
//     cin >> n >> m;

//     while(m --){
//         int a, b;
//         cin >> a >> b;
//         g[b][a] = 1;
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j < =n; j ++){
//             if(g[i][j] == 1) dp[a] = (dp[a] + dp[b]) % 80112002;
//         }
//     }
    
//     return 0;
// }
//平方和
// using namespace std;

// typedef long long ll;

// int main(){
//     ll sum = 0, po = 0;

//     for(int i = 1; i <= 2019; i ++){
//         string x = to_string(i);
//         for(char it:x){
//             if(it == '2' || it == '1' || it == '0' || it == '9'){
//                 sum += i;
//                 po += pow(i, 2);
//                 break;
//             }
//         }
//     }

//     cout << po << endl;
//     return 0;
// }
//滑雪--dp
// using namespace std;

// const int N = 1e5 + 10;

// int r, c;
// int a[N];

// int main(){
//     cin >> r >> c;

//     for(int i = 1; i <= r; i ++){
//         for(int j = 1; j < =c; j ++){
//             cin >> a[i][j];
//         }
//     }

//     return 0;
// }
// using namespace std;

// int main(){
//     float a, b = 10;
//     for(a = 1; a == 10 ; a ++){
//         printf("%f", a);
//     }
//     printf("%f", a);

//     return 0;
// }
//蛇形填数二
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;

// int a[N][N];

// int sum = 0;

// void d(){
//     for(int i = 1; i <= n; i ++){
//         a[i][m] = i ++;
//         sum ++;
//     }
// }

// void l(){
//     for(int i = )
// }

// void u(){

// }

// void r(){

// }

// int main(){
//     cin >> n >> m;

//     char last;
//     last = 'R';

//     while(sum <= n * m){
//         if(last == 'R'){
//             d();
//             last = 'D';
//         }else if(last == 'D'){
//             l();
//             last = 'L';
//         }else if(last == 'L'){
//             u();
//             last = 'U';
//         }else if(last == 'U'){
//             r();
//             last = 'R';
//         }
//     }

//     return 0;
// }
//蛇形填数
// using namespace std;

// const int N = 50;

// int n;

// int a[N][N];
// int b[N][N];
// int main(){
    
//     a[1][1] = 1;
//     a[1][2] = 2;
//     int num = 1;

//     b[1][1] = 1;

//     num = 1;
    
//     for(int i = 2; i <= 20; i ++){
//         b[i][i] = b[i - 1][i - 1] + num * 4;
//         num ++;
//     }
//     // for(int j = 3; j <= 40; j ++){
//     //     if((j + 1) % 2 == 0){
//     //         a[1][j] = 4 * num + a[1][j - 1];
//     //         num ++;
//     //     }
//     //     else a[1][j] = a[1][j - 1] + 1;
//     // }

//     cout << b[20][20];
//     return 0;
// }
// using namespace std;

// //日期公式
// // bool is_leap(int y){
// //     if(y % 4 == 0 && y % 100 != 0 || y % 400 == 0)
// //         return 1;
    
// //     return 0;
// // }

// // int get_days(int y, int m){
// //     if(m == 2) return month[2] + is_leap(y);
// //     return month[m]
// // }

// // void next_day(int y, int m, int d){
// //     d ++;
// //     if(d > month[m]){
// //         d = 1;
// //         m ++;
// //         if(m > 12){
// //             m = 1;
// //             y ++;
// //         }
// //     }
// // }

// // int calc(int y, int m, int d){
// //     for(int i = 1; i < y; i ++)
// //         sum += 365 + is_leap(i);

// //     for(int i = 1; i < m; i ++)
// //         sum += get_day(y, i);

// //     sum += d;
// // }

// int main(){

//     return 0;
// }
// using namespace std;

// int cnt = 0;
// int main(){
//     string str;
//     getline(cin, str);
//     // int n = stoi(str);

//     cout << str.size() << endl;
//     for(int i = 0; i < str.length(); i ++){
//         if(str[i] != ' '){
//             cnt ++;
//         }
//     }

//     cout << cnt;
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// // string str;
// // int sum = 0;
// char str[N];
// int sum = 0;

// int main(){
    
//     cin >> str;

//     for(int i = 0; i < strlen(str); i ++){
//         if((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z'))
//             sum ++;
//     }
//     cout << str[4];
//     // while(cin >> str){
//     //     cout << str << " ";
//     //     sum += str.size();
//     // }
    
//     cout << sum;
//     return 0;
// }
//dijkstra
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 2e5 + 10;

// int n, m;
// int h[N], e[N], w[N], ne[N], idx; 
// int dist[N];
// bool st[N];

// void add(int a, int b, int c){
//     w[idx] = c;
//     e[idx] = b;
//     ne[idx] = h[a];
//     h[a] = idx ++;
// }

// int dijkstra(){
//     memset(dist, 0x3f, sizeof dist);
//     dist[1] = 0;
//     priority_queue<PII, vector<PII>, greater<PII>> heap;

//     heap.push({0, 1});

//     while(heap.size()){

//         auto t = heap.top();
//         heap.pop();

//         int ver = t.second, dist[ver] = t.first;

//         if(st[ver]) continue;
//         st[ver] = true;

//         for(int i = h[ver]; i != -1; i = ne[i]){
//             int j = e[i];
//             if(dist[j] > dist[ver] + w[i]){
//                 dist[j] = dist[ver] + w[i];
//                 heap.push({dist[j], j});
//             }
//         }
//     }

//     if(dist[n] == 0x3f3f3f3f) return -1;
    
//     return dist[n];
// }

// int main(){
//     cin >> n >> m;

//     memset(h, -1, sizeof(h));

//     for(int i = 1; i <= m; i ++){
//         int a, b, c;
//         cin >> a >> b >> c;
//         add(a, b, c);
//     }

//     printf("%d", dijkstra());

//     return 0;
// }

//dp问题
//最优包含
//迷宫
// #include<iostream>
// #include<cstring>
// #include<algorithm>

// using namespace std;
// const int N = 110;
// typedef pair<int, int> PII;
// PII q[N*N],Prev[N][N];
// int g[N][N], d[N][N];
// int n, m;

// //正序打印--追溯到靠近第一个节点才打印
// void print(int a, int b){
//     if(a != 0 && b != 0) print(Prev[a][b].first, Prev[a][b].second);
//     printf("%d %d\n", Prev[a][b].first, Prev[a][b].second);
// }

// int bfs()
// {
//     int hh = 0, tt = 0;
//     q[0] = {0, 0};
//     memset(d, -1, sizeof d);

//     int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};
//     d[0][0] = 0;
//     while(hh <= tt)
//     {
//         PII t = q[hh ++ ];
//         for(int i = 0; i < 4; i ++ )
//         {
//             int x = dx[i] + t.first, y = t.second + dy[i];

//             if(x >= 0 && x < n && y >= 0 && y < m && g[x][y] == 0 && d[x][y] == -1)
//             {
//                 d[x][y] = d[t.first][t.second] + 1;
//                 Prev[x][y] = t;
//                 q[++ tt] = {x, y};
//             }
//         }
//     }
    
//     //逆序打印
//     int x = n - 1, y = m - 1;
//     while(x || y)//有一个d不等于0
//     {
//         cout << x << ' ' << y << endl;
//         PII t = Prev[x][y];
//         x = t.first, y = t.second;
//     }

//     //正序打印print(n - 1, m - 1);
    
//     return d[n - 1][m - 1];
// }
// int main()
// {
//     cin >> n >> m;
//     for(int i = 0; i < n; i ++ )
//         for(int j = 0; j < m;j ++)
//             cin >> g[i][j];

//     cout << bfs() << endl;

//     return 0;
// }
//波动数列
// using namespace std;

// int main(){

//     return 0;
// }
//最长上升子序列
// using namespace std;

// const int N = 1e3 + 10;

// int n;
// int a[N];
// int dp[N];
// int res = 0;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) {cin >> a[i];}

//     for(int i=2;i<=n;++i){
// 		dp[i]=0;
// 		for(int j=i-1;j>0;--j){
// 			if(a[i]>a[j]){
// 				dp[i]=max1(dp[i],dp[j]);
// 			}
// 		}
// 		dp[i]++;
// 		if(dp[i]>ans) ans=dp[i];
// 	}
//     // for(int i = 1; i <= n; i ++){
//     //     for(int j = i - 1; j >= 1; j --){
//     //         if(a[j] < a[i]) dp[i] = max(dp[j] + 1, d[i]);
//     //     }

//     //     res = max(res, dp[i]);
//     // }

//     cout << res;

//     return 0;
// }
//挖地雷
// using namespace std;

// const int N = 30;

// int n;
// int a[N];
// int st[N][N];
// int res = 0;
// int dp[N];
// int pos;
// int pre[N];
// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n - 1; i ++){
//         for(int j = i + 1; j <= n; j ++){
//             cin >> st[i][j];        
//         }
//     }

//     for(int i = 1; i <= n; i ++){
//         dp[i] = a[i];
//         int j;
//         for(j = i + 1; j <= n; j ++){
//             // if(!st[i][j]) dp[j] = max(dp[j], a[j]);
//             if(dp[i] + a[j] >= dp[j] && st[i][j]){
//                 dp[j] = max(dp[j], dp[i] + a[j]);
//                 pre[j] = i;
//             }
//         }
//         if(dp[j] > res){
//             res = dp[j];
//             pos = j;
//         }
//     }

//     for(int i = 1; i <= n; i ++) res = max(res, dp[i]);

//     cout << res;
//     return 0;
// }
//采药
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// int t[N], w[N];
// int dp[N];

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= m; i ++)
//         cin >> t[i] >> w[i];
    
//     // for(int i = 1; i <= m; i ++){
//     //     for(int j = 1; j <= n; j ++){
//     //         if(j < t[i]) dp[i][j] = dp[i - 1][j];
//     //         else{
//     //             dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - t[i]] + w[i]);
//     //         }
//     //     }
//     // }

//     for(int i = 1; i <= m; i ++){
//         for(int j = n; j >= t[i]; j --){
//             dp[j] = max(dp[j - t[i]] + w[i], dp[j]);
//         }
//     }

//     cout << dp[n];
    
//     return 0;
// }
//采药
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// int v[N];
// int w[N];
// int dp[N][N];

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++)
//         cin >> v[i] >> w[i];

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             if(j < v[i])dp[i][j] = dp[i - 1][j];
//             else{
//                 dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - v[i]] + w[i]);
//             }
//         }
//     }

//     cout << dp[n][m];

//     return 0;
// }
//数字三角形
// using namespace std;

// typedef long long ll;

// const int N = 1e3 + 10;

// int n;
// ll res = 0;
// int a[N][N];
// ll dp[N][N];

// int main(){
//     cin >> n;
    
//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= i; j ++){
//             cin >> a[i][j];
//             dp[i][j] = a[i][j];
//         }
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= i; j ++){
//             dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - 1]) + dp[i][j];
//         }
//     }

//     for(int i = 1; i <= n; i ++) res = max(res, dp[n][i]);

//     cout << res;

//     return 0;
// }
//最大公约数
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;

// ll gcd(ll a, ll b){
//     return b ? gcd(b, a %b) : a;
// }

// int main(){
//     cin >> n;

//     while(n --){
//         ll a, b;
//         cin >> a >> b;
//         printf("%lld\n", gcd(a, b));
//     }

//     return 0;
// }
//快速幂
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;

// ll qmi(ll a, ll b, ll p){
//     ll res = 1;

//     while(b){
//         if(b & 1) res = (ll)res * a % p;
//         b >>= 1;
//         a = (ll)a * a % p;
//     }

//     return res;
// }

// // ll qmi(int a, int b, int p){
// //     ll res = 1;

// //     while(b){
// //         if(b & 1) res = res * a % p;
// //         b >>= 1;
// //         a = a * a % p;
// //     }

// //     return res;
// // }

// int main(){
//     cin >> n;

//     while(n --){
//         ll a, b, p;
//         cin >> a >> b >> p;

//         printf("%lld\n", qmi(a, b, p));
//     }

//     return 0;
// }
//最大异或对
// using namespace std;

// const int N = 1e5 + 10, M = 31 * N;

// int n;
// int a[N];
// int son[M][2], idx = 0;

// void insert(int x){
//     int p = 0;
//     for(int i = 30; i >= 0; i --){
//         int u = x >> i & 1;
//         if(!son[p][u]) son[p][u] = ++ idx;
//         p = son[p][u];
//     }
// }

// int search(int x){
//     int p = 0; int res = 0;
//     for(int i = 30; i >= 0; i --){
//         int u = x >> i & 1;
//         if(son[p][!u]){
//             res = res * 2 + 1;
//             p = son[p][!u];
//         }else{
//             res = res * 2;
//             p = son[p][u];
//         }
//     }

//     return res;
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) {
//         cin >> a[i];
//         insert(a[i]);    
//     }
    
//     int res = 0;
//     for(int i = 1; i <= n; i ++){
//         res = max(res, search(a[i]));
//     }

//     cout << res;

//     return 0;
// }
//最大异或对
// using namespace std;

// const int N = 1e5 + 10, M = 31 * N;

// int n;
// int a[N];
// int son[M][2], idx;

// void insert(int x){
//     int p = 0;
//     for(int i = 30; i >= 0; i --){
//         int u = x >> i & 1;
//         if(!son[p][u]) son[p][u] = ++ idx;
//         p = son[p][u];
//     }
// }

// int search(int x){
//     int p = 0; int res = 0;
//     for(int i = 30; i >= 0; i --){
//         int u = x >> i & i;
//         if(son[p][!u]){
//             p = son[p][!u];
//             res = res * 2 + 1;
//         }else{
//             p = son[p][u];
//             res = res * 2 + 0;
//         }
//     }

//     return res;
// }

// int main(){
//     cin >> n;
//     idx = 0;
//     for(int i = 0; i < n; i ++){
//         cin >> a[i];
//         insert(a[i]);
//     }
//     int res = 0;

//     for(int i = 0; i < n; i ++){
//         res = max(res, search(a[i]));
//     }

//     cout << res << endl;

//     return 0;
// }
//食物链
// using namespace std;


// int main(){
//     cout << c.time << endl;
//     return 0;
// }
//食物链
// using namespace std;

// const int N = 1e5 + 10;

// int n, k;
// int same[N];
// int eat[N];

// int find1(int x){
//     if(same[x] != x) same[x] = find1(same[x]);
//     return same[x];
// }

// int find2(int x){

// }

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++) {ne[i] = i; same[i] = i;}

//     int sum = 0;

//     while(k --){
//         int t; 
//         cin >> t >> x >> y;

//         if(x > n || y > N) {sum ++; continue;}
        
//         if(x == y){sum ++; continue;}

//         if(t == 1){
//            same[find1(x)] = find1(y);
//         }
//         else {
//             if(find1(x) == find1(y)){
//                 sum ++;
//             }
//         }
//     }

//     cout << sum;

//     return 0;
// }
//连通块中点的数量
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int ne[N];
// int sz[N];

// int find(int x){
//     if(ne[x] != x) ne[x] = find(ne[x]);
//     return ne[x];
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++){
//         ne[i] = i;
//         sz[i] = 1;
//     }

//     while(m --){        
//         char op[3];

//         cin >> op;

//         int a, b;

//         if(op[0] == 'C'){
//             cin >> a >> b;
//             sz[find(b)] += sz[find(a)];
//             ne[find(a)] = find(b);
//         }
//         else if (op[1] == '1')
// 		{
// 			cin >> a >> b;
// 			if (find(a) == find(b))cout << "Yes" << endl;
// 			else cout << "No" << endl;
// 		}else if(op[1] == '2'){
//             cin >> a;
//             printf("%d\n",sz[find(a)]);
//         }

//     }
    
//     return 0;
// }
//连通块中点的数量
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int cnt[N];
// int ne[N];

// int find(int x){
//     if(ne[x] != x) ne[x] = find(ne[x]);
//     return ne[x];
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i++) ne[i] = i;

//     while(m --){
//         char op;
//         int t;
//         int a, b;
//         cin >> op;

//         if(op == 'C'){
//             cin >> a >> b;
//             ne[find(a)] = find(b);
//         }
//         if(op == 'Q'){
//             cin >> t;

//             if(t = 1) {
//                 cin >> a >> b;
//                 if(find(a) == find(b))printf("Yes\n");
//                 else printf("No\n");
//             }
//             if(t = 2){ 
//                 cin >> a;
//                 printf("%d", cnt[find(a)]);
//             }
//         }
//     }

//     return 0;
// }
// Trie字符串统计
// using namespace std;

// const int N = 1e5 + 10;

// char ch[N];
// int son[N][27];
// int idx;
// int cnt[N];

// void insert(char *ch){
//     int p = 0;
//     for(int i = 0; ch[i]; i ++){
//         int u = ch[i] - 'a';
//         if(!son[p][u]) son[p][u] = ++ idx;
//         p = son[p][u]; 
//     }

//     cnt[p] ++;
// }

// int query(char *ch){
//     int p = 0;
//     for(int i = 0; ch[i]; i ++){
//         int u = ch[i] - 'a';
//         if(!son[p][u]) return 0;
//         p = son[p][u];
//     }

//     return cnt[p];
// }

// int main(){
//     int n;
//     cin >> n;

//     while(n --){
//         char op[2];
//         // cin >> op >> ch;
//         scanf("%s%s", op, ch);

//         if(op[0] == 'I') insert(ch);
//         else printf("%d\n", query(ch));
//     }

//     return 0;
// }
//四平方和
// using namespace std;

// const int N = 5e6 + 10;

// int a, b, c, d;
// int n;

// int r[N * 2];

// int main(){
//     cin >> n;

//     memset(r, -1, sizeof r);

//     for(int c = 0; c * c <= n; c ++){
//         for(int d = c; d * d + c * c <= n; d ++){
//             int t = c * c + d * d;
//             if(r[t] == -1){
//                 r[t] = c;
//             }
//         }
//     }

//     for(int a = 0; a * a <= n; a ++){
//         for(int b = a; b * b + a * a <= n; b ++){
//             int t = n - a * a - b * b;
//             int c = r[t];
//             if(r[t] != -1){
//                 int d = sqrt(t - c * c);
//                 printf("%d %d %d %d\n", a, b, c, d);
//                 return 0;
//             }
//         }
//     }

//     return 0;
// }
//星空之夜
// using namespace std;

// const int N = 5e6 + 10;

// int n;
// int d;

// bool check(int a, int b, int c){
//     if(((sqrt(n - a * a - b * b - c * c) - (int)sqrt(n - a * a - b * b - c * c) == 0) && sqrt(n - a * a - b * b - c * c) >= c)|| sqrt(n - a * a - b * b - c * c) < c) return true;
    
//     return false;
// }

// int main(){
//     cin >> n;

//     for(int i = 0; i <= sqrt(n);  i++){
//         for(int j = i; j * j <= n; j ++){
//             int l = j, r = n;
//             while(l < r){
//                 int mid = l + r >> 1;
//                 if(check(i, j, mid)) r = mid;
//                 else l = mid + 1;
//             }
//             printf("%d %d %d %.0f", i, j, l, sqrt(n - i * i - j * j - l * l));
//         }
//     }
//     return 0;
// }
//字符串哈希
// using namespace std;

// typedef unsigned long long ull;

// const int N = 1e5 + 10, P = 131;

// int n, m;
// ull p[N], h[N];

// ull query(int l, int r){
//     return h[r] - h[l - 1] * p[r - l + 1];
// }

// int main(){
//     cin >> n >> m;

//     string str;
//     cin >> str;

//     p[0] = 1;
//     h[0] = 0;

//     for(int i = 0; i <= n - 1; i ++){
//         p[i + 1] = p[i] * P;
//         h[i + 1] = h[i] * P + str[i];
//     }

//     while(m --){
//         int l1, r1, l2, r2;
//         cin >> l1 >> r1 >> l2 >> r2;

//         if(query(l1, r1) == query(l2, r2)) printf("Yes\n");
//         else printf("No\n");
//     }

//     return 0;
// }
//字符串哈希
// using namespace std;

// typedef unsigned long long ull;

// const int N = 1e5 + 5, P = 131;

// ull h[N], p[N];

// ull query(int l, int r){
//     return h[r] - h[l - 1] * p[r - l + 1];
// }

// int main(){
//     int n, m;
//     cin >> n >> m;

//     string x;
//     cin >> x;

//     p[0] = 1;
//     h[0] = 0;

//     for(int i = 0; i < n; i ++){
//         p[i + 1] = p[i] * P;
//         h[i + 1] = h[i] * p + x[i];
//     }

//     while(m --){
//         int l1, r1, l2, r2;
//         cin >> l1 >> r1 >> l2 >> r2;
//         if(query(l1, r1) == query(l2, r2)) printf("Yes\n");
//         else printf("No\n");
//     }

//     return 0;
// }
//模拟散列表

//开放寻址法
// using namespace std;

// const int N = 2e5 + 3;
// const int null = 0x3f3f3f3f;

// int h[N];

// int find(int x){
//     int k = (x % N + N) % N;
//     while(h[k] != null && h[k] != x){
//         t ++;
//         if(t == N){
//             t = 0;
//         }
//     }

//     return t;
// }

// int main(){
//     cin >> n;

//     memset(h, 0x3f, sizeof h);

//     while(n --){
//         string op;

//         int x;
//         cin >> op >> x;
//         if(op == "I"){
//             h[find(x)] = x;
//         }else{
//             if(h[find(x)] == null){
//                 puts("No");
//             }else{
//                 puts("Yes");
//             }
//         }
//     }

//     return 0;
// }
//拉链法
// #include<iostream>
// #include<algorithm>
// #include<cstring>

// using namespace std;

// const int N = 1e3 + 10;

// int h[N], e[N], ne[N], idx;

// void insert(int x){
//     int k = (x % N + N) % N;
//     e[idx] = x;
//     ne[idx] = h[k];
//     h[k] = idx ++;
// }

// bool find(int x){
//     int k = (x % N + N) % N;
//     for(int i = h[k]; i != -1; i = ne[i]){
//         if(e[i] == x){
//             return true;
//         }
//     }

//     return false;
// }

// int n;

// int main(){
//     cin >> n;

//     memset(h, -1, sizeof h);

//     while(n --){
//         string op;
//         int x;
//         cin >> op >> x;
//         if(op == 'I'){
//             insert(x);
//         }else{
//             if(find(x)){
//                 puts("Yes");
//             }else{
//                 puts("No");
//             }
//         }
//     }

//     return 0;
// }
//日志统计
// #define x first
// #define y second

// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e5 + 10;

// vector<PII> p;

// int t, id;
// int n, d, k;
// int num[N];
// int hh;
// int j;
// int q[N];
// int l = 1, r = 0;
// int b[N];

// bool cmp(PII x, PII y){
//     return x.y < y.y;
// }

// int main(){
//     cin >> n >> d >> k;
    
//     for(int i = 1; i <= n; i ++) {cin >> t >> id; p.push_back({t, id});}

//     sort(p.begin(), p.end(), cmp);

//     int i = 1;

//     while(i <= n){
//         hh = 0;
//         for(j = i; j <= n; j ++) {
//             b[++ hh] = p[j].x;
//             if(p[j + 1].y != p[j].y) 
//                 break;
//         }

//         sort(b + 1, b + hh + 1);
        
//         int flag = 0;

//         for(int i = 1; i <= n; i ++){
//             while(l <= r && r - l + 1 > k) l ++;
//             while(l <= r && b[i] - b[q[l]] + 1 <= d) r ++;
//             q[++ r] = i;
//             if(r - l + 1 >= k) {printf("%d\n", p[j - 1].y);break;}
//         }

//         i = j;
//     }

//     return 0;
// }
//牛的学术圈
// using namespace std;


// int main(){

//     return 0;
// }
//牛的学术圈
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int a[N];
// int num[N];
// int mx;

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     sort(a + 1, a + n + 1);

//     for(int i = 1; i <= n; i ++) num[a[i]] = n - i + 1, mx = max(mx, num[a[i]]);
    
//     int l = 1, r = i + c;
//     b[l] ++;
//     b[r + 1] --;
    
//     if(m == 0) {cout << mx; return 0;}
    
//     // for(ini i = 1; i <= n - m + 1; i ++){
//     //     int l = i, r = l +
//     // }
//     // while(l <= r && r <= n){
        
//     // }
//     // for(int i = 1; i <= min(n - m + 1, n); i ++){
//     //     for(int j = i; j <= i + m - 1; j ++){

//     //     }
//     // }
//     return 0;
// }
//牛的学术圈

// using namespace std;

// const int N = 1e5 + 10;

// int n, l;
// int a[N];
// int num[N];
// int mx;

// int main(){
//     cin >> n >> l;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     sort(a + 1, a + n + 1);

//     for(int i = 1; i <= n; i ++) num[a[i]] = n - i + 1;

//     int l = 1, r = 1;

//     while(r <= n && l <= r){
//         if(r - l + 1 > c) l ++, num[a[l - 1] --] = n - l;
//         while(r - l + 1 <= c) num[a[r] ++] = n - r + 1, r ++;
//         for(int i = n; i >= 1; i --) mx = max(mx, num[a[i]]);
//     }

//     cout << mx;
//     return 0;
// }
//判断子序列
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int a[N], b[N];

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= m; i ++) cin >> b[i];

//     int l = 1, r = 1;

//     while(r <= m){
//         while(a[l] != b[r]) r ++;
//         while(a[l] == b[r])r ++, l ++;
//     }

//     if(l != n + 1) printf("No");
//     else printf("Yes");

//     return 0;
// }
//判断子序列
// using namespace std;

// const int N = 1e5 + 10;

// int a[N], b[N];
// int n, m;

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int j = 1; j <= m; j ++) cin >> b[j];

//     int l = 1, r = 1;

//     while(r <= m){
//         if(b[r] < a[l]) r ++;
//         if(b[r] > a[l]) break;
//         if(a[l] == b[r])l ++, r ++;
//     }

//     if(l == n) printf("Yes");
//     else printf("No");

//     return 0;
// }
//筛指数
// using namespace std;

// const int N = 1e6 + 10;

// int n;

// bool st[N];
// int prime[N];
// int j = -1;

// void merge(int x){
//     for(int i = 2; i <= x; i ++){
//         if(!st[i]) prime[++ j] = i;
//         for(int k = 0; prime[k] <= x / i; k ++){
//             st[prime[k] * i] = 1;
//             if(i % prime[k] == 0) break;
//         }
//     }
// }

// int main(){
//     cin >> n;

//     merge(n);

//     cout << j + 1;
    
//     return 0;
// }
//筛质数
// using namespace std;

// const int N = 1e6 + 10;

// int n;

// bool a[N];
// int peime[N];
// int j = -1;

// int main(){
//     cin >> n;

//     merge(n);

//     cout << j + 1;

//     return 0;
// }
//乌龟棋
// #include<stdio.h>
// int main() {
//     int a, b;
//     printf("please enter values:");
//     scanf_s("%d %d", &a, &b);
//     while (a % b) {
//         int tmp = a % b;
//         a = b;
//         b = tmp;
//     }
//     printf("the max common divisor is %d\n", b);
//     return 0;
// }
//乌龟棋
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// int a[N];
// int b[N];
// bool st[N];
// int sum = 0;
// int mx = 0;

// void dfs(int i, int num){
//     if(num == 5) {mx = max(mx,sum); return;}

//     for(int i = 1; i <= m; i ++){
//         if(!st[i]){
//             sum += a[1 + b[i]];
//             st[i] = 1;
//             dfs(i, num + 1);
//         }
//     }

// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int j = 1; j <= m; j ++) cin >> b[j];

//     for(int i = 1; i <= m; i ++){
//         sum = a[1];
//         st[i] = 1;
//         dfs(i, 1);
//         st[i] = 0;
//         // cout << endl;
//     }

//     cout << mx;
    
//     return 0;
// }
//最长公共子序列
// using namespace std;

// const int N = 1e3 + 10;

// char A[N], B[N];
// int n, m;
// int dp[N][N];

// int main(){
//     cin >> n >> m >> A + 1 >> B + 1;

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             dp[i][j] = max(dp[i - 1][j],dp[i][j - 1]);
//             if(a[i] == a[j]) dp[i][j] = max(dp[i][j], dp[i - 1][j -- 1] + 1);
//         }
//     }

//     printf("%d", dp[n][m]);

//     return 0;
// }
//最长上升子序列
// using namespace std;

// typedef long long ll;

// const int N = 1e3 + 10;

// int n;
// ll a[N];
// int dp[N];

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) {cin >> a[i]; dp[i] = 1;}

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j < i; j ++){
//             if(a[j] < a[i]) dp[i] = max(dp[j] + 1, dp[i]);
//         }
//         mx = max(mx, dp[i]);
//     }

//     printf("%d", mx);

//     return 0;
// }
//母亲的牛奶
// using namespace std;

// const int N = 1e2 + 10;

// int A, B, C;
// struct Node
// {
//     int a, b, c;
// }q[N * N * N];
// bool st[N][N][N];

// void dfS(){
//     int hh = 0, tt = 0;
//     q[0] = {0, 0, C};
//     st[0][0][C] = true;
//     int W[3] = {A, B, C};

//     while(hh <= tt){
//         auto t = q[hh ++];
//         for(int i = 0; i < 3; i ++)
//             for(int j = 0; j < 3; j ++){
//                 int w[3] = {t.a, t.b, t.c};
//                 int cur = min(w[i], W[i] - w[j]);
//                 w[i] -= cur, w[j] += cur;
//                 int a = w[0], b = w[1], c = w[2];
//                 if(!st[a][b][c]){
//                     st[a][b][c] = true;
//                     q[++ tt] = {a, b, c};
//                 }
//             }
//     }
// }

// int main(){
//     cin >> A >> B >> C;

//     bfs();

//     for(int c = 0; c <= C; c ++)
//         for(int b = 0; b <= B; b ++)
//             if(st[0][b][c]){
//                 printf("%d", c);
//                 break;
//             }


//     return 0;
// }
//母亲的牛奶
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e3 + 10;

// int num[4];
// int laxt[N];
// set<int> p;

// void bfs(){
//     queue<PII> q;
//     q.push({0, 0});

//     for(int i = 1; i <= 3; i ++){
//         if(num[1] == 0)p.insert(num[3]); 
//     }

//     while(q.size()){

//     }

// }

// int main(){
//     cin >> num[1] >> num[2] >> num[3];

//     bfs();

//     for(auto i : p) cout << i;
//     // printSet(p);
//     // for(int i = 0; i < p.size(); i ++) cout << p[i];

//     return 0;
// }
//走迷宫
// #define x first
// #define y second

// using namespace std;

// const int N = 1e3 + 10;

// typedef pair<int, int> PII;

// int n, m;
// int g[N][N];
// int d[N][N];

// void bfs(){
//     queue<PII> q;
//     q.push({0, 0});
//     memset(d, -1, sizeof d);
//     d[0][0] = 0;

//     int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

//     while(q.size()){
//         auto t = q.front();
//         q.pop();

//         for(int i = 0; i < 4; i ++){
//             int x = t.x + dx[i], y = t.y + dy[i];

//             if(x >= 0 && x < n && y >= 0 && y < m && g[x][y] != 1 && d[x][y] == -1){
//                 d[x][y] = d[t.x][t.y] + 1;
//                 q.push({x,y});
//             }
//         }
//     }
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 0; i < n; i ++)
//         for(int j = 0; j < m; j ++)
//             cin >> g[i][j];

//     bfs();

//     cout << d[n - 1][m - 1];

//     return 0;
// }
//走迷宫
// #define x first
// #define y second

// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e3 + 10;

// int n, m;
// int g[N][N];
// int d[N][N];

// int bfs(){
//     queue<PII> q;
//     q.push({0, 0});    
//     memset(d, -1, sizeof d);
//     d[0][0] = 0;

//     int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

//     while(q.size()){
//         auto t = q.front();
//         q.pop();

//         for(int i = 0; i < 4; i ++){
//             int x = t.x + dx[i], y = t.y + dy[i];
//             if(x >= 0 && x < n && y >= 0 && y < m && g[x][y] == 0 && d[x][y] == -1){
//                 d[x][y] = d[t.x][t.y] + 1;
//                 q.push({x, y});
//             }
//         }
//     }
     
//     return d[n - 1][m - 1];
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 0; i < n; i ++)
//         for(int j = 0; j < m; j ++)
//             cin >> g[i][j];

//     cout << bfs();

//     return 0;
// }
//编辑距离
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// char str[N][N];
// char b[N];
// int dp[N][N];
// int res = 0;
// int num = 0;

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> str[i] + 1;

//     while(m --){
//         res = 0;
//         cin >> b + 1 >> num;

//         for(int i = 1; i <= n; i ++){

//             for(int x = 0; x <= strlen(b + 1); x ++) dp[0][x] = x;
//             for(int x = 0; x <= strlen(str[i] + 1); x ++) dp[x][0] = x;
            
//             for(int j = 1; j <= strlen(str[i] + 1); j ++){
//                 for(int k = 1; k <= strlen(b + 1); k ++){
//                     dp[j][k] = min(dp[j - 1][k] + 1, dp[j][k - 1] + 1);
//                     dp[j][k] = min(dp[j][k], dp[j - 1][k - 1] + (str[i][j] != b[k]));
//                 }
//             }

//             if(dp[strlen(str[i] + 1)][strlen(b + 1)] <= num) res ++;
//         }

//         cout << res << endl;
//     }

//     return 0;
// }
//买不到的数目
// #include <iostream>
// #include <algorithm>
// using namespace std;
// int n, m, minn, maxx, ans;
// bool dp[1000000];
// int main() {
//     int ans = 1;
//     cin >> n >> m;
//     dp[0] = true;
//     minn = min(n, m);
//     maxx = max(n, m);
//     for (int i = minn; i < n * m; i++) {
//         if (dp[i - minn]) {
//             dp[i] = true;
//         } else if (i >= maxx && dp[i - maxx]) {
//             dp[i] = true;
//         } else {
//             ans = i;
//         }
//     }
//     cout << ans;
//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int dp[N][N];

// int a, b;

// int main(){
//     cin >> a >> b;

//     for(int i = 0; i <= 1000; i ++){
//         for(int j = 0; j <= 1000; j ++){
//             dp[i][j] = 
//         }
//     }
//     return 0;
// }
//最短编辑距离

// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// char a[N], b[N];
// int dp[N][N];

// int main(){
//     scanf("%d%s", &n, a + 1);
//     scanf("%d%s", &m, b + 1);
    
//     for(int i = 0; i <= m; i ++) dp[0][i] = i;
//     for(int i = 0; i <= n; i ++) dp[i][0] = i;

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             f[i][j] = min(f[i - 1][j] + 1, f[i][j - 1] + 1);
//             if(a[i] == b[j]) f[i][j] = min(f[i][j], f[i - 1][j - 1]);
//             else f[i][j] = min(f[i][j], f[i - 1][j - 1] + 1);
//         }
//     }

//     printf("%d\n", f[n][m]);

//     return 0;
// }
//最长公共子序列
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// char a[N], b[N];
// int f[N][N];

// int main(){
//     cin >> n >> m >> a + 1 >> b + 1;

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             f[i][j] = max(f[i - 1][j], f[i][j - 1]);
//             if(a[i] == b[j]) f[i][j] = max(f[i][j], f[i - 1][j - 1] + 1);
//         }
//     }

//     printf("%d", f[n][m]);

//     return 0;
// }
//砍竹子--题解
// #include <iostream>
// #include <set> //集合 
// #include <cmath>
// using namespace std;
// int main(int argc, char** argv) {
//     int n,ans=0;
//     set<long long>front; //前一个数的集合 
//     cin >> n;
//     for(int i=1;i<=n;i++)
//     {
//         long long temp;
//         set<long long>now; //当前数的集合
//         cin >> temp;
//         while(temp>1){
//             now.insert(temp); //当前数被砍历程组成集合 
//             if(!front.count(temp)) //当前数被砍历程如与前一个数相同，则可以一起砍，不必再加次数 
//             ans++;
//             temp=sqrtl(temp/2+1);
//         }
//         front=now; //改变前一个数集合位置 
//     }
//     cout << ans;
//     return 0;
// }
//砍竹子
// using namespace std;

// typedef long long ll;

// const int N = 2e5 + 10;

// vector<unordered_set<ll>> hs;

// int n;
// ll ans, hei[N];

// ll justDoit(ll x){
//     return sqrtl(x / 2 + 1);
// }

// int main(){
//     cin >> n;

//     hs.resize(n + 1);

//     for(int i = 1; i <= n; i ++){
//         ll x;
//         cin >> x;

//         while(x > 1){
//             if(!hs[i - 1].count(x)) ans ++;

//             hs[i].insert(x);
//             x = justDoit(x);
//         }
//     }

//     cout << ans;

//     return 0;
// }
//砍竹子
// using namespace std;

// typedef long long ll;

// const int N = 2e5 + 10;

// int n;
// ll h[N];
// int sum = 0;

// void dfs(int sum){
//     for(int i = 1; i <= n; i ++){
//         if(h[i] != 1) break;
//         else {
//             printf("%d", sum);
//             return ;        
//         }   
//     }

//     for(int i = 1; i <= n; i ++){
//         if(h[i] > h[])
//     }
// }

// int main(){
//     cin >> n;

//     h[0] = 0, h[n + 1] = 0;
//     for(int i = 1; i <= n; i ++) cin >> h[i];

//     dfs(0);

//     return 0;
// }
//最大上升子序列2
// using namespace std;

// const int N = 1e6 + 10;

// int n;
// int a[N], q[N];
// int hh = 0, tt = -1;
// int mx = 0;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++){
//         mx = max(mx, tt + 1);
//         // while(hh <= tt && i - q[hh] + 1 > k) hh ++:
//         if(hh <= tt && a[i] > a[q[tt]]) {q[++ tt] = i; continue;} 
//         while(hh <= tt && a[i] <= a[q[tt]]) tt --;
//         q[++ tt] = i;

//         // if(i >= k - 1) cout << q[hh];
//     }

//     cout << mx;

//     return 0;
// }
//最长上升子序列
// using namespace std;

// const int N = 1e3 + 10;

// int dp[N];
// int n;
// int a[N];
// int mx = 0;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n ; i ++) {cin >> a[i]; dp[i] = 1;}

//     for(int i = 1; i <= n; i ++){
//         for(int j = 0; j <= n; j ++){
//             if(a[j] < a[i]) {dp[i] = max(dp[i], dp[j] + 1);}
//         }
//         mx = max(mx, dp[i]);
//     }

//     printf("%d", mx);

//     return 0;
// }
//走迷宫
// using namespace std;

// typedef pair<int, int> PII;
// const int N = 110;

// int n, m;
// int g[N][N], dist[N][N];

// int main(){
//     cin >> n >> m;
//     for(int i = 0; i < n; i ++)
//         for(int j = 0; j < m; j ++)
//             cin >> g[i][j];
    
//     bfs();

//     printf("%d\n", dist[n - 1][m - 1]);
//     return 0;
// }
//走迷宫
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1000;

// int n, m;
// int a[N][N];
// int d[N][N], g[N][N];
// int hh = 0, tt = 0;

// vector<PII> q;

// int bfs(){
//     q[0] = {0, 0};

//     memset(d, -1, sizeof(d));

//     d[0][0] = 0;

//     int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

//     while(q.size()){
//         auto t = q[hh];

//         for(int i = 0; i < 4; i ++){
//             int x = t.first + dx[i], y = t.second + dy[i];

//             if(x >= 0 && x < n && y >= 0 && y < n && d[x][y] == -1 && a[x][y] == 0){
//                 d[x][y] = d[t.first][t.second] + 1;
//                 q.push_back({x, y});
//             }
//         }
//     }

//     return d[n - 1][m - 1];
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 0; i < n; i ++){
//         for(int j = 0; j < n; j ++){
//             cin >> a[i][j];
//         }
//     }

//     cout << bfs();

//     return 0;
// }
//扫雷游戏
// using namespace std;

// const int N = 1e3 + 10;

// int n, t;
// char str[N][N];
// int g[N][N];

// void dfs(int x, int y){
//     int t = g[x][y];
//     g[x][y] = -1;
//     if(t) return ;

//     for(int i = x - 1; i <= x + 1; i ++){
//         for(int j = y - 1; j <= y + 1; j ++){
//             if(i >= 0 && i < n && j >= 0 && j < n && g[i][j] != -1){
//                 dfs(i, j);
//             }
//         }
//     }

// }

// int main(){
//     cin >> t;

//     for(int x = 1; x <= t; x ++){
//         cin >> n;

//         for(int i = 0; i < n; i ++){
//             for(int j = 0; j < n; j ++){
//                 cin >> str[i][j];
//             }
//         }

//         for(int i = 0; i < n; i ++){
//             for(int j = 0; j < n; j ++){
//                 if(str[i][j] == '*') g[i][j] = -1;
//                 else{
//                     g[i][j] = 0;
//                     for(int k = i - 1; k <= i + 1; k ++){
//                         for(int l = j - 1; l <= j + 1; l ++){
//                             if(k >= 0 && k < n && l >= 0 && l < n && str[k][l] == '*'){
//                                 g[i][j] ++;
//                             }
//                         }
//                     }
//                 }
//             }
//         }   

//         int res = 0;

//         for(int i = 0; i < n; i ++){
//             for(int j = 0; j < n; j ++){
//                 if(g[i][j] == 0){
//                     res ++;
//                     dfs(i, j);
//                 }
//             }
//         }

//         for(int i = 0; i < n; i ++){
//             for(int j = 0; j < n; j ++){
//                 if(g[i][j] != -1){
//                     res ++;
//                 }
//             }
//         }

//         printf("Case #%d: %d\n", x, res);
//     }

//     return 0;
// }
//flood - fill  扫雷游戏
// using namespace std;

// const int N = 1000;

// int n, t;
// int a[N][N];
// char g[N][N];

// void dfs(int j, int k){
//     int t = a[j][k];
//     a[j][k] = -1;
//     if(t) return ;

//     for(int i = j - 1; i <= j + 1; i ++){
//         for(int l = k - 1; l <= k + 1; l ++){
//             if(i >= 0 && i < n && l >= 0 && l < n && a[i][l] != -1){
//                 dfs(i, l);
//             }
//         }
//     }
// }

// int main(){
//     cin >> t;

//     for(int i = 1; i <= t; i ++){

//         cin >> n;

//         for(int j = 0; j < n; j ++){
//             for(int k = 0; k < n; k ++){
//                 cin >> g[j][k];
//                 if(g[j][k] == '*') a[j][k] = -1;
//             }
//         }

//         int res = 0;

//         for(int j = 0; j < n; j ++){
//             for(int k = 0; k < n; k ++){
//                 if(g[j][k] != '*'){
//                     res ++;
//                     dfs(j, k);
//                 }
//             }
//         }

//         for(int j = 0; j < n; j ++){
//             for(int k = 0; k < n; k ++){
//                 if(a[j][k] != -1)
//                     res ++;
//             }
//         }

//         printf("Case #%d: %d", i, res);
//     }

//     return 0;
// }
//奇妙的数字
// using namespace std;

// int main(){

//     for(int i = 30; i <= 70; i ++){
//         cout << i * i << " " << i * i * i << endl;
//     }

//     return 0;
// }
//星系炸弹
// using namespace std;

// int months[] = {
//     0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
// };

// int is_leap(int y){
//     if((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)
//         return 1;
//     return 0;
// }

// int get_days(int y, int m){
//     if(m == 2) return (months[m] + is_leap(y));
//     return months[m];
// }

// void next_day(int& y, int& m, int& d){
//     d ++;

//     if(d > months[m]){
//         d = 1;
//         m ++;

//         if(m > 12){
//            m = 1;
//            y ++;
//         }
//     }
// }

// int calc(int y, int m, int d){
//     int res = 0;

//     for(int i = 1; i < y; i ++)
//         res += 365 + is_leap(i);

//     for(int i = 1; i < m; i ++)
//         res += get_days(y, i);

//     return  res + d; 
// }

// int main(){

//     int y = 2014, m = 11, d = 9;
//     int sum = 0;
//     while(1){
//         sum ++;
//         if(sum == 1000) {printf("%d-%02d-%02d", y, m, d); return 0;}
//         next_day(y, m, d);
//     }

//     return 0;  
// }
//方程整数解
// using namespace std;

// int main(){

//     for(int i = 1; i <= 40; i ++){
//         for(int j = i; j <= 40; j ++){
//             for(int k = j; k <= 40; k ++){
//                 if(i * i + j * j + k * k == 1000){
//                     if(i != 6 && j != 8 && k != 30){
//                         cout << i;
//                         return 0;
//                     }
//                 }
//             }
//         }
//     }
//     return 0;
// }
//平方怪圈
// using namespace std;

// int mx = 0;

// void dfs(int a, int num){
//     int b = a;
//     int sum = 0;

//     if(num == 50){
//         if(a != 1)printf("%d", mx);
//         else printf(0);
//     }

//     while(b){
//         sum += (b % 10) * (b % 10);
//         b /= 10;
//     }
//     mx = max(mx, sum);

//     a = sum;
//     dfs(a, num + 1);
// }

// int main(){
//     dfs(12, 0);

//     return 0;
// }
//煤球数目
// using namespace std;

// typedef long long ll;

// int main(){

//     ll x = 1;
//     ll sum = 1;
//     for(int i = 2; i <= 100; i ++){
//         x += i;
//         sum += x;
//     }

//     printf("%lld", sum);

//     return 0;
// }
// vxvxvxvxvxvxvvx
//有奖竞猜
// using namespace std;

// typedef long long ll;

// char str[16];

// int main(){
//     ll a = 777;

//     cin >> str + 1;

//     for(int i = 1; i <= 15; i ++){
//         if(str[i] == 'v') a *= 2;
//         else a -= 555;
//     }

//     printf("%lld", a);

//     return 0;
// }
//子矩阵
// using namespace std;

// typedef long long ll;

// const int N = 3e5 + 10;

// ll a[N];
// int n, m;
// ll res = INT_MIN;

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++) a[i] += a[i - 1];

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             res = max(res, (a[i + j - 1] - a[i - 1]));
//         }
//     }

//     cout << res;

//     return 0;
// }
// int n, m;
// ll a[N];
// int q[N];
// int res = 0;
// int num = 0;

// int sum(int q[]){
//     for(int i = 0; i < q.size(); i ++){
//         num += q[i];
//     }
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 0; i < n; i ++) cin >> a[i];

//     hh = 0, tt = -1;

//     for(int i = 1; i <= n; i ++){

//         while(hh <= tt && i - q[hh] + 1 > m) hh ++;
//         while(hh <= tt && a[i] >= a[q[tt]]) tt --;
//         q[++ tt] = i;
//         if(i >= m) res = max(res, sum(q));
//     }
    
//     return 0;
// }
//矩形牛棚
// using namespace std;

// const int N = 3010;

// int g[N][N], h[N][N];
// int R, c, p;
// int x, y;
// int top = 0;
// int sta[N];//单调栈 -- 里面存储的是下标
// int l[N], r[N];

// int work(int x){
//     h[x][0] = h[x][c + 1] = -1;

//     top = 0;
//     sta[++ top] = 0;
//     for(int i = 1; i <= c; i ++){
//         while(top > 0 && h[x][sta[top]] >= h[x][i]) top --;
//         l[i] = sta[top];
//         sta[++ top] = i;
//     }

//     top = 0;
//     sta[++ top] = c + 1;
//     for(int i = c; i >= 1; i --){
//         while(top > 0 && h[x][sta[top]] >= h[x][i]) top --;
//         r[i] = sta[top];
//         sta[++ top] = i;
//     }

//     int maxx = 0;
//     for(int i = 1; i <= c; i ++){
//         maxx = max((r[i] - l[i] - 1) * h[x][i], maxx);
//     }

//     return maxx;
// }

// int main(){
//     cin >> R >> c >> p;

//     for(int i = 1; i <= p; i ++){
//         cin >> x >> y;
//         g[x][y] = 1;
//     }

//     for(int i = 1; i <= R; i ++){
//         for(int j = 1; j <= c; j ++){
//             if(!g[i][j]){
//                 h[i][j] = h[i - 1][j] + 1;
//             }
//         }
//     }

//     int mx = 0;
//     for(int i = 1; i <= c; i ++) mx = max(mx, work(i));
//     cout << mx;
    
//     return 0;
// }
// #include <iostream>
// using namespace std;
// const int N = 3003;

// int R,C,P;
// bool g[N][N];
// int h[N][N];
// int stk[N],tp=-1; // 单调递减栈(严格)
// int l[N],r[N];

// int cal(int a[]){
//     a[0] = -1; 
//     tp=-1,stk[++tp]=0;
//     for(int i=1;i<=C;i++){
//         while(a[stk[tp]]>=a[i]) --tp;
//         l[i] = stk[tp];
//         stk[++tp] = i;
//     }

//     a[C+1] = -1;
//     tp=-1,stk[++tp]=C+1;
//     for(int i=C;i>=1;i--){
//         while(a[stk[tp]]>=a[i]) --tp;
//         r[i] = stk[tp];
//         stk[++tp] = i;
//     }

//     int res = 0;
//     for(int i=1;i<=C;i++) res = max(res,a[i] * (r[i]-l[i]-1)); // (r[i]-1)-(l[i]+1)+1
//     return res;
// }

// int main(){
//     cin>>R>>C>>P;
//     for(int i=0;i<P;i++){
//         int r1,c1;
//         cin>>r1>>c1;
//         g[r1][c1] = true;
//     }

//     for(int i=1;i<=R;i++){
//         for(int j=1;j<=C;j++){
//             if(!g[i][j]) h[i][j] = h[i-1][j]+1;
//             else h[i][j] = 0;
//         }
//     }

//     int res = 0;
//     for(int i=1;i<=R;i++) res = max(res,cal(h[i]));
//     cout<<res<<endl;
//     return 0;
// }

//矩形牛棚
// using namespace std;

// const int N = 3010;

// int g[N][N], h[N][N];
// int sta[N], top;
// int l[N], r[N];
// int n, m, p;

// int area(int x){
//     h[x][0] = h[x][m + 1] = -1;

//     top = 0;
//     sta[++ top] = 0;
//     for(int i = 1; i <= m; i ++){
//         while(top > 0 && h[x][sta[top]] >= h[x][i]){
//             top --;
//         }
//         l[i] = sta[top];
//         sta[++ top] = i;
//     }
//     top = 0;
//     sta[++ top] = m + 1;
//     for(int i = m + 1; i >= 1; i --){
//         while(top > 0 && h[x][sta[top]] >= h[x][i]){
//             top --;
//         }
//         r[i] = sta[top];
//         sta[++ top] = i;
//     }
    
//     int maxx = 0;
//     for(int i = 1; i <= m; i ++){
//         maxx = max((r[i] - l[i] + 1) * h[x][i], maxx);
//     }
//     return maxx; 
// }
// int main(){
//     cin >> n >> m >> p;
//     while(p --){
//         int x, y;
//         cin >> x >> y;
//         g[x][y] = 1;
//     }
//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             if(!g[i][j]){
//                 h[i][j] = h[i - 1][j] + 1;//求出直观图的每一列的高度
//             }
//         }
//     }
//     int res = 0;
//     for(int i = 1; i <= n; i ++) res = max(res, area(i));
//     cout << res << endl;

//     return 0;
// }
//矩形牛棚
// using namespace std;

// const int N = 1e4 + 10;

// int R, C, P;
// int r, c;
// int a[N][N];

// int main(){
//     cin >> R >> C >> P;

//     for(int i = 1; i <= R; i ++)
//         for(int j = 1; j <= C; j ++)
//             a[i][j] = 1;

//     while(P --){
//         cin >> r >> c;
//         a[r][c] = 0;
//     }

//     for(int i = 1; i <= R; i ++)
//         for(int j = 1; j <= C; j ++)
//             a[i][j] += a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1];
    
//     for(int i = 1; i <= R; i ++){
//         for(int j = 1; j <= C; j ++){

//         }
//     }
//     return 0;
// }
//单调栈
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int tt = -1;
// int q[N];

// int main(){
//     cin >> n;
    
//     while(n --){
//         int x;
//         cin >> x;

//         while(tt != -1 && q[tt] >= x) tt --;
//         if(tt != -1)cout << q[tt] << " ";
//         else cout << "-1 ";
//         q[++ tt] = x;
//     }

//     return 0;
// }
//单调栈
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int a[N];
// int tt = -1;

// int main(){
//     cin >> n;

//     while(n --){
//         int x;
//         cin >> x;

//         while(tt != -1 && q[tt] >= x) tt --;
//         if(tt != -1) cout << q[tt] << " ";
//         else cout << "-1";
//         q[++ tt] = x;
//     }
    
//     return 0;
// }
//滑动窗口
// using namespace std;

// const int N = 1e6 + 10;

// int n, k;
// int a[N], q[N];
// int hh, tt = -1;

// int main(){
//     cin >> n >> k;
//     for(int i = 0; i < n; i ++)cin >> a[i];

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) hh ++;
//         while(hh <= tt && a[i] < a[q[tt]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
//     puts("");
//     int hh = 0, tt = -1;

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) hh ++;
//         while(hh <= tt && a[i] > a[q[tt]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
//     return 0;
// }
//滑动窗口
// using namespace std;

// const int N = 1e6 + 10;

// int n, k;
// int a[N], q[N];
// int hh, tt = -1;

// int main(){
//     cin >> n >> k;
//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) ++ hh;
//         while(hh <= tt && a[i] <= a[q[tt]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
//     puts("");
//     int hh = 0, tt = -1;

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) ++ hh;
//         while(hh <= tt && a[i] >= a[q[tt]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
    
//     return 0;
// }
//奶酪
// using namespace std;

// typedef long long ll;

// const int N = 1010;

// struct sphere{
//     int x, y, z;
// }q[N];

// int p[N];
// int n, h, r;
// int t;

// int find(int x){
//     if(p[x] != x)p[x] = find(p[x]);
//     return p[x];
// }

// int main(){
//     cin >> t;

//     while(t --){
//         cin >> n >> h >> r;
//         for(int i = 0; i <= n + 1; i ++)p[i] = i;


//         for(int i = 1; i <= n; i ++){
//             int x, y, z;
//             scanf("%d%d%d", &x, &y, &z);
//             q[i] = {x, y, z};

//             if(abs(z) <= r)p[find(i)] = find(0);
//             if(abs(z - h) <= r)p[find(i)] = find(n + 1);
//         }

//         for(int i = 1; i <= n; i ++)
//             for(int j = 1; j < i; j ++){
//                 ll dx = q[i].x - q[j].x;
//                 ll dy = q[i].y - q[j].y;
//                 ll dz = q[i].z - q[j].z;

//                 if(dx * dx + dy * dy + dz * dz <= 4 * (ll) r * r)
//                     p[find(i)] = find(j);
//             }

//         if(find(0) == find(n + 1)) puts("Yes");
//         else puts("No");
//     }

//     return 0;
// }
//奶酪
// using namespace std;

// typedef long long ll;

// const int N = 1e3 + 10;

// ll n, h, r;
// ll x[N], y[N], z[N];
// ll t;
// ll mxz = 0;
// ll mxx = 0;
// ll mxy = 0;
// bool used[N];
// int sum = 0;

// void dfs(int i, int mxz){
//     for(int j = i + 1; j <= n; j ++){
//         if((x[j] - mxx) * (x[j] - mxx) + (y[j] - mxy) * (y[j] - mxy) + (z[j] - mxz) * (z[j] - mxz) <= 2 * r * 2 * r) mxx = x[j], mxy = y[j], mxz = z[j];
//         used[j] = 1;
//         dfs(j, mxz);
//         used[j] = 0;
//     }

//     if(mxz + r >= h) {sum ++ ; if(sum == 1)printf("Yes\n");}
// }

// int main(){
//     cin >> t;

//     while(t --){
//         sum = 0;
//         int flag = 0;
//         cin >> n >> h >> r;

//         for(int i = 1; i <= n; i ++){
//             cin >> x[i] >> y[i] >> z[i];
//         }

//         for(int i = 1; i <= n - 1; i ++){
//             for(int j = 1; j <= n - i; j ++){
//                 if(z[j] >= z[j + 1]){
//                     swap(x[j], x[j + 1]);
//                     swap(y[j], y[j + 1]);
//                     swap(z[j], z[j + 1]);
//                 }
//             }
//         }

//         if(z[1] - r > 0 || z[n] + r < h) {printf("No\n"); continue;}

//         else{
//             for(int i = 1; i <= n; i ++){
//                 if(z[i] - r > 0) break;
//                 mxx = x[i];
//                 mxy = y[i];
//                 mxz = z[i];
//                 dfs(i, mxz);
//             }
//         }

//         if(sum == 0) printf("No\n");
//     }

//     return 0;
// }
//奶酪
// using namespace std;

// typedef long long ll;

// const int N = 1e3 + 10;

// ll n, h, r;
// ll x[N], y[N], z[N];
// ll t;
// ll mxz = 0;
// ll mxx = 0;
// ll mxy = 0;

// int main(){
//     cin >> t;

//     while(t --){
//         int flag = 1;
//         cin >> n >> h >> r;

//         for(int i = 1; i <= n; i ++){
//             cin >> x[i] >> y[i] >> z[i];
//         }

//         for(int i = 1; i <= n - 1; i ++){
//             for(int j = 1; j <= n - i; j ++){
//                 if(z[j] >= z[j + 1]){
//                     swap(x[j], x[j + 1]);
//                     swap(y[j], y[j + 1]);
//                     swap(z[j], z[j + 1]);
//                 }
//             }
//         }

//         if(z[1] - r > 0 || z[n] + r < h) flag = 1;

//         else{
//             for(int i = 1; i <= n; i ++){
//                 if(z[i] - r > 0) break;
//                 mxx = x[i];
//                 mxy = y[i];
//                 mxz = z[i];
//                 for(int j = i + 1; j <= n; j ++){
//                     if((x[j] - mxx) * (x[j] - mxx) + (y[j] - mxy) * (y[j] - mxy) + (z[j] - mxz) * (z[j] - mxz) <= 2 * r * 2 * r) mxx = x[j], mxy = y[j], mxz = z[j];
//                 }
//                 if(mxz + r >= h) {flag = 0; break;}
//             }
//         }

//         if(flag == 1) printf("No\n");
//         else printf("Yes\n");
//     }

//     return 0;
// }

//合并集合
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int ne[N];
// char ch;

// int find(int x){
//     if(ne[x] != x) ne[x] = find(ne[x]);
//     return ne[x];
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) ne[i] = i;

//     while(m --){
//         int a, b;

//         cin >> ch >> a >> b;
        
//         //此处错误
//         // if(ch == 'M'){
//         //     ne[a] = b;
//         // }
//         //正确写法
//         if(ch == 'M') ne[find(x)] = find(x);

//         if(ch == 'Q'){
//             if(find(a) == find(b)) printf("Yes\n");
//             else printf("No\n");
//         }
//     }

//     return 0;
// }
//连号区间数
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int a[N];
// int mx, mn;
// int res = 0;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++){
//         mx = a[i];
//         mn = a[i];

//         for(int j = i; j <= n; j ++){
//             // sort(a + i, a + j + 1);
//             // if(a[j] - a[i] != j - i) break;
//             // res ++;
//             // if(a[j] - a[j] != j - i) {res ++; printf("%d %d\n", j, i);}
//             // else break;
//             // if(a[j] == a[i]){ res ++; continue;}

//             if(a[j] + 1 == mn){
//                 mn = a[j];
//                 res ++;
//                 continue;
//             }
//             if(a[j] - 1 == mx){
//                 mx = a[j];
//                 res ++;
//                 continue;
//             }

//             break;
//         }
//     }

//     cout << res;
//     return 0;
// }
//买不到的数目
// using namespace std;

// bool dfs(int m, int p, int q){
//     if(m == 0) return true;

//     if(m >= p && dfs(m - p, p, q)) return true;
//     if(m >= q && dfs(m - q, p, q)) return true;

//     return false;
// }

// int main(){
//     int p, q;
//     cin >> p >> q;
//     int res = 0;
//     for(int i = 1; i <= 1000; i ++){
//         if(!dfs(i, p, q)) res = i;
//     }

//     cout << res << endl;

//     return 0;
// }
//求二进制数里的0与1的个数
// using namespace std;

// int n;
// int sum1;
// int sum0;

// void lowbit(int x){
//     while(x != 0){
//         if(x % 2 == 1) sum1 ++;
//         else sum0 ++;
//         x /= 2;
//     }

//     printf("%d %d", sum1, sum0);
// }

// int main(){
//     cin >> n;

//     lowbit(n);

//     return 0;
// }
//带分数
// using namespace std;

// const int N = 10;

// int target;
// int num[N];
// bool used[N];
// int cnt;

// int calc(int l, int r){
//     int res = 0;
//     for(int i = l; i <= r; i ++){
//         res = res * 10 + num[i];
//     }

//     return res;
// }

// void dfs(int u){
//     if(u == 9){
//         for(int i = 0; i < 7; i ++){
//             for(int j = i + 1; j < 8; j ++){
//                 int a = calc(0, i);
//                 int b = calc(i + 1, j);
//                 int c = calc(j + 1, 8);

//                 if(a == 0 || b == 0 || c == 0){
//                     continue;
//                 }

//                 if(a * c + b == c * target){
//                     cnt ++;
//                 }
//             }
//         }
//     }

//     for(int i = 1; i <= 9; i ++){
//         if(!used[i]){
//             used[i] = 1;
//             num[u] = i;
//             dfs(u + 1);
//             used[i] = 0;
//         }
//     }
// }

// int main(){
//     scanf("%d", &target);
//     dfs(0);
//     printf("%d\n", cnt);

//     return 0;
// }
//正则问题
// using namespace std;

// int k = 0;
// string str;

// int dfs(){
//     int res = 0;
//     while(k < str.size()){
//         if(str[k] == '('){
//             k ++;
//             res += dfs();
//             k ++;
//         }else if(str[k] == ')'){
//             break;
//         }else if(str[k] == '|'){
//             k ++;
//             res = max(res, dfs());
//         }else{
//             k ++;
//             res ++:
//         }
//     }

//     return res ;
// }

// int main(){
//     cin >> str;

//     cout << dfs() << endl;

//     return 0;
// }
//有序分数--递归
// using namespace std;

// int n;

// void dfs(int a, int b, int c, int d){
//     if(b + d > n) return ;

//     dfs(a, b, a + c, b + d);
//     printf("%d/%d\n", a + c, b + d);
//     dfs(a + c, b + d, c, d);

// }

// int main(){
//     cin >> n;

//     printf("0/1\n");

//     dfs(0, 1, 1, 1);

//     cout << 1 << "/" << 1;
//     return 0;
// }
//有序分数
// #define x first
// #define y second

// using namespace std;

// typedef pair<int, int> PII;

// vector<PII> p;

// int n;

// int gcd(int a, int b){
//     return b ? gcd(b, a % b) : a;
// }

// bool cmp(PII a, PII b){
//     return a.x * b.y < b.x * a.y;
// }

// int main(){
//     cin >> n;

//     printf("0/1");

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= i; j ++){
//             if(gcd(i, j) == 1){
//                 p.push_back({i, j});
//             }
//         }
//     }

//     sort(p.begin(), p.end(), cmp);

//     for(int i = 0; i < p.size(); i ++) printf("%d/%d", p[i].y, p[i].x);

//     return 0;
// }
//有序分数
// using namespace std;

// struct node{
//     double st;
//     int a;
//     int b;
// } a[100001];

// bool cmp(node a, node b){
//     return a.st < b.st;
// }

// int main(){
//     int n, c = 0;
//     cin >> n;
//     cout << "0/1\n";

//     for(int i = 1; i <= n; i ++){
//         for(int j = i; j <= n; j ++){
//             if(i < j && _gcd(i, j) == 1){
//                 c ++;
//                 a[c].a = i;
//                 a[c].b = j;
//                 a[c].st = double((i * 1.0) / (j * 1.0));
//             }
//         }
//     }
    
//     sort(a + 1, a + 1 + c, cmp);
    
//     for(int i = 1; i <= c; i ++) printf("%d/%d\n", a[i].c, a[i].b);
    
//     return 0;
// }
//有序分数
// using namespace std;

// const int N = 1e5;

// int n;

// int m = -1;
// int l = -1;
// int A[N], B[N];
// double C[N];
// int mx = 2;
// int mxi = 0;

// int main(){
//     cin >> n;

//     printf("0/1\n");
//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= i; j ++){
//             for(int k = 2; k <= j; k ++){
//                 while(i % k == 0 && j % k == 0){
//                     i /= k;
//                     j /= k;
//                 }
//             }
//             A[++ m] = j;
//             B[++ l] = i;          
//         }
//     }

//     for(int i = 0; i < m; i ++) C[i] = A[i] * 1.0 / B[i];

//     for(int i = 0; i < m; i ++){
//         mx = 2;

//         for(int j = 0; j <= m; j ++){
//             if(C[i] < mx && C[i] <= 1){
//                 mx = C[i];
//                 mxi = i;
//                 C[i] = 2.0;
//             }
//         }

//         printf("%d/%d", A[mxi], B[mxi]); 
//     }

//     return 0;
// }
//最佳牛围栏
// using namespace std;

// const int N = 1e5 + 10;

// int n, f;
// int a[N];
// int mx = 0;

// int main(){
//     cin >> n >> f;

//     for(int i = 1; i <= n; i ++) {cin >> a[i]; a[i] += a[i - 1];}

//     for(int i = 1; i <= n - f + 1; i ++){
//         mx = max(mx, a[i + f - 1] - a[i - 1]);
//     }

//     printf("%lf", mx * 1.0 / f * 1000);
    
//     return 0;
// }
//技能升级
// using namespace std;

// #define x first
// #define y second

// typedef long long ll;

// typedef pair<int, int> PII;

// const int N = 1e5 + 10;
// // const int P = 1e8;

// ll n, m;
// int A[N], B[N];
// // int c[P];
// int k = -1;
// ll sum = 0;
// int a, b;

// vector<PII> p;

// int main(){
//     cin >> n >> m;

//     for(int i = 0; i < n; i ++){
//         cin >> a >> b;
//         p.push_back({a, b});
//     }

//     for(int i = 0; i < m; i ++){
//         sort(p.begin(), p.end());
//         sum += p[n - 1].x;
//         p[n - 1].x -= p[n - 1].y;
//     }

//     printf("%lld", sum);
//     // for(int i = 1; i <= n; i ++){
//     //     while(A[i] > 0){
//     //         c[++ k] = A[i];
//     //         A[i] -= B[i];
//     //     }
//     // }
    
//     // sort(c, c + k, greater<int>());

//     // for(int i = 0; i < m; i ++){
//     //     sum += c[i];
//     // }

//     // printf("%lld", sum);

//     return 0;
// }
//冶炼金属
// using namespace std;

// const int N = 1e4 + 10;

// int n;
// int A[N], B[N];
// int mx = 0;

// bool check1(int mid){
//     for(int i = 1; i <= n; i ++){
//         if(A[i] / mid > B[i]) return false;
//     }

//     return true;
// }

// bool check2(int mid){
//     for(int i = 1; i <= n; i ++){
//         if(A[i] / mid < B[i]) return false;
//     }

//     return true;
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         cin >> A[i] >> B[i];
//         mx = max(mx, A[i]);
//     }

//     int l = 1, r = mx;

//     while(l < r){
//         int mid = l + r >> 1;
//         if(check1(mid)) r = mid;
//         else l = mid + 1;
//     }

//     cout << l << " ";

//     l = 1, r = mx;

//     while(l < r){
//         int mid = l + r + 1 >> 1;
//         if(check2(mid)) l = mid;
//         else r = mid - 1;
//     }

//     cout << r;

//     return 0;
// }
//三国游戏
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;
// ll A[N], B[N], C[N];
// ll a, b, c;

// bool check(int i){
//     if(C[i] > (A[i] + B[i]) || A[i] > (C[i] + B[i]) || B[i] > (A[i] + C[i])) return true;
//     return false;
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         cin >> a;
//         A[i] = A[i - 1] + a;
//     }

//     for(int i = 1; i <= n; i ++){
//         cin >> b;
//         B[i] = B[i - 1] + b;
//     }

//      for(int i = 1; i <= n; i ++){
//         cin >> c;
//         C[i] = C[i - 1] + c;
//     }

//     for(int i = n; i >= 1; i --){
//         if(check(i)){
//             cout << i;
//             break;
//         }
//     }

//     return 0;
// }

// #include <iostream>
// #include<cmath>
// using namespace std;
// typedef long long LL;
// const int N=1e5+5;
// LL a[N],n;
// int main()
// {
//   cin>>n;
//   for(int i=1;i<=n;i++)
//   {
//     cin>>a[i];
//     a[i]=abs(a[i]);
//      a[i]+=a[i-1];
//   }
//   cout<<a[n];
//   return 0;
// }
//数学奇才
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;
// ll a[N] = {0};
// int first ;
// int x;

// int main(){
//     cin >> n;

//     x = n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++){
//         if(a[i] < 0) {first = 0; break;}
//         else if(a[i] > 0) {first = 1; break;}
//     }

//     if(first){
//         n /= 2;
//         for(int i = 1; i <= n; i ++){
//             if(n <= 0) break;
//             if(a[i] < 0){
//                 a[i] *= -1;
//                 if(a[i + 1] > 0) n --;
//             }
//         }
//     }
//     else{
//         n = (n + 1) / 2;
//         for(int i = 1; i <= n; i ++){
//             if(n <= 0) break;
//             if(a[i] < 0){
//                 a[i] *= -1;
//                 if(a[i + 1] > 0) n --;
//             }
//         }
//     }

//     for(int i = 1; i <= x; i ++) a[i] += a[i - 1];

//     printf("%lld", a[x]);

//     return 0;
// }
//猜灯谜
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int a[N];
// int b[N];

// int main(){
//     cin >> n;

//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0; i < n; i ++) cout << a[(i - 1 + n) % n] + a[(i + 1 + n) % n] << " ";

//     return 0;
// }
//航班时间--天才的做法//
// #include<bits/stdc++.h>
// using namespace std;
// int getTime(void)
// {
//     int h1,m1,s1,h2,m2,s2,d=0;
//     scanf("%d:%d:%d %d:%d:%d (+%d)",&h1,&m1,&s1,&h2,&m2,&s2,&d);
//     int time=d*24*3600+h2*3600+m2*60+s2-(h1*3600+m1*60+s1);
//     return time;
// }
// int main()
// {
//     int t;
//     scanf("%d",&t);
//     for(int i = 0; i < t; i++)
//     {
//         int time1=getTime();
//         int time2=getTime();
//         int t=(time1+time2)/2;
//         printf("%02d:%02d:%02d\n", t/3600, t/60%60, t%60);
//     }
//     return 0;
// }
//航班时间
// using namespace std;

// int t;
// int nums, numm, numh;
// int sumh, summ, sums;

// int main(){
//     scanf("%d", &t);

//     for(int i = 1; i <= 2 * t; i ++){
//         int h1, m1, s1, h2, m2, s2, d = 0;// h3, m3, s3, h4, m4, s4, d = 0;
//         scanf("%02d:%02d:%02d %d:%d:%d (+%d)", &h1, &m1, &s1, &h2, &m2, &s2, &d);
//         if(d != 0){
//             if(s2 < s1){
//                 m2 --;
//                 nums = s2 + 60 - s1;                
//             }
//             else nums = s2 - s1;

//             if(m2 < m1){
//                 h2 --;
//                 numm = m2 + 60 - m1;
//             }
//             else numm = m2 - m1;

//             numh = h2 - h1 + 24 * d;

//             sumh += numh;
//             summ += numm;
//             sums += nums;
//         }
//         else if(d == 0){
//         }
//         if(i % 2 == 0){
//             printf("%02d:%02d:%02d\n", sumh / 2, summ / 2, sums / 2);

//             sumh = 0;
//             summ = 0;
//             sums = 0;
//         }
//     }

//     return 0;
// }
//刷题统计

// using namespace std;

// typedef long long ll;

// ll a, b, n;
// ll num = 0;

// int main(){
//     cin >> a >> b >> n;

//     ll sum = a * 5 + b * 2;

//     num += (n / sum) * 7;
//     n %= sum;

//     int l = 5, r = 2;

//     while(n > 0){
//         num ++;
//         if(l) {n -= a; l --;}
//         else {n -= b; r --;} 
//     }


//     cout << num;

//     return 0;
// }
//倍数问题

// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n, k;
// ll a[N];
// ll mx = 0;
// int j, l;

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++) cin >> a[i];   
    
//     sort(a + 1, a + n + 1);

//     for(int i = n; i >= 3; i --){
//         for(j = i - 1; j >= 2; j --){
//             l = j - 1;
//             if((a[i] + a[j] + a[l]) < mx) break;
//             for(l = j - 1; l >= 1; l --){
//                 if((a[i] + a[j] + a[l]) % k == 0) {mx = max(mx, a[i] + a[j] + a[l]);}
//                 if((a[i] + a[j] + a[l]) < mx) break;
//             }
//         }
//     }

//     printf("%lld", mx);

//     return 0;
// }
//k倍区间
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n, k;
// ll a[N];
// ll b[N];
// ll res = 0;

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++){
//         a[i] += a[i - 1];
//         b[a[i] % k] ++;
//     }

//     for(int i = 0; i <= k - 1; i ++){
//         res += (b[i] * (b[i] - 1)) / 2;
//     }

//     cout << res + b[0];

//     return 0;
// }
//倍数问题
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n, k;
// ll a[N];
// ll mx = 0;

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++) cin >> a[i];   
    
//     sort(a + 1, a + n + 1);

//     for(int i = n; i >= 3; i --){
//         for(int j = i - 1; j >= 2; j --){
//             for(int l = j - 1; l >= 1; l --){
//                 if((a[i] + a[j] + a[l]) % k == 0) {mx = max(mx, a[i] + a[j] + a[l]);}
//             }
//         }
//     }

//     printf("%lld", mx);

//     return 0;
// }
//倍数问题

// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n, k;
// ll a[N];

// bool check(int mid ,int i, int j){
//     if((a[mid] + a[i] + a[j]) % k == 0) return true;
// }

// int main(){
//     cin >> n >> k;

    // for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = n; i >= 1; i --){
//         for(int j = i - 1; j >= 1; j --){
//             int l = 1, r = j - 1;
//             while(l < r){
//                 int mid = l + r + 1 >> 1;
//                 if(check(mid, i, j)) l = mid;
//                 else r = mid - 1;
//             }

//             if((a[i] + a[j] + a[l]) % k == 0){printf("%d", a[i] + a[j] + a[l]); return 0;}
//         }
//     }

//     return 0;
// }
//借教室

// using namespace std;

// typedef long long ll;

// const int N = 1e6 + 10;

// int n, m;
// ll a[N];
// ll d[N], s[N], t[N];
// ll b[N];

// bool check(int mid){
//     memset(b, 0, sizeof b);

//     for(int i = 1; i <= mid; i ++){
//         b[s[i]] += d[i];
//         b[t[i] + 1] -= d[i];
//     }

//     for(int i = 1; i <= n; i ++){
//         b[i] += b[i - 1];
//         if(b[i] > a[i]) return false;
//     }

//     return true;
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++){
//         cin >> a[i];
//     }

//     for(int i = 1; i <= m; i ++){
//         cin >> d[i] >> s[i] >> t[i];
//     }

//     int l = 0, r = m;

//     while(l < r){
//         int mid = l + r + 1 >> 1;
//         if(check(mid)) l = mid;
//         else r = mid - 1;
//     }

//     if(l == m) printf("0");
//     else printf("-1\n%d", l + 1);

//     return 0;
// }
//数的范围
// using namespace std;

// const int N = 1e5 + 10;

// int n, q, k;
// int a[N];

// int main(){
//     cin >> n >> q;

//     for(int i = 1; i <= n; i ++){
//         cin >> a[i];
//     }

//     while(q --){
//         cin >> k;

//         int l = 1, r = n; 

//         while(l < r){
//             int mid = l + r >> 1;
//             if(a[mid] >= k) r = mid;
//             else l = mid + 1;
//         }
        
//         if(a[l] != k) {cout << "-1 -1" << endl; continue;}
//         else cout << l - 1 << " ";

//         l = 1, r = n;

//         while(l < r){
//             int mid = l + r + 1 >> 1;
//             if(a[mid] <= k) l = mid;
//             else r = mid - 1;
//         }

//         cout << r - 1 << endl;
//     }

//     return 0;
// }
//分巧克力
// using namespace std;

// const int N = 1e5 + 10;

// int n, k;
// int h[N], w[N];
// int mx;

// bool check(int x){
//     int sum = 0;

//     for(int i = 1; i <= n; i ++){
//         sum += (h[i] / x) * (w[i] / x);    
//     }

//     if(sum >= k) return true;
//     else return false;

// }

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++){
//         cin >> h[i] >> w[i]; 
//         mx = max(max(h[i], w[i]), mx);
//     }

//     int l = 1, r = mx;

//     while(l < r){
//         int mid = l + r + 1 >> 1;
//         if(check(mid)) l = mid;
//         else r = mid - 1;
//     }

//     cout << l;

//     return 0;
// }
//兰顿蚂蚁
// using namespace std;

// const int N = 1e3 + 10;

// int a[N][N];
// int m, n;

// int x, y, k;
// char s;
// int main(){
//     cin >> m >> n;

//     for(int i = 0; i < m; i ++)
//         for(int j = 0; j < n; j ++)
//             cin >> a[i][j];

//     cin >> x >> y >> s >> k;

//     while(k --){
//         if(a[x][y] == 1){
//             a[x][y] = 0;
//             if(s == 'L') {x --, s = 'U'; continue;}
//             if(s == 'U') {y ++, s = 'R'; continue;}
//             if(s == 'R') {x ++, s = 'D'; continue;}
//             if(s == 'D') {y --, s = 'L'; continue;}
//         }
//         else if(a[x][y] == 0){
//             a[x][y] = 1;
//             if(s == 'L') {x ++, s = 'D'; continue;}
//             if(s == 'U') {y --, s = 'L'; continue;}
//             if(s == 'R') {x --, s = 'U'; continue;}
//             if(s == 'D') {y ++, s = 'R'; continue;}
//         }
//     }

//     cout << x << " " << y;
//     return 0;
// }
//跳石头
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// ll a[N];
// ll L, n, m;

// bool check(int mid){
//     int cnt = 0;
//     int i = 0, now = 0;

//     while(i < n + 1){
//         i ++;
//         if(a[i] - a[now] < mid) cnt ++;
//         else now = i;
//     }

//     if(cnt <= m) return true;

//     return false;

// }

// int main(){
//     cin >> L >> n >> m;

//     for(int i = 1; i <= n; i ++){
//         cin >> a[i];
//     }

//     a[0] = 0;
//     a[n + 1] = L;

//     ll l = 1, r = L;

//     while(l < r){
//         int mid = l + r + 1 >> 1;
//         if(check(mid)) l = mid;
//         else r = mid - 1;
//     }

//     cout << l;

//     return 0;
// }

//跳石头
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// ll l, n, m;
// ll a[N];

// int main(){
//     cin >> l >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

    
//     return 0;
// }
// #include<iostream>
// using namespace std;

// const int N=50010;
// int a[N],n,len,m,mina=1e9+1,b[N];

// int check(int mid)	//检查，是否最短距离为mid，如果两石头间距小于mid，不满足，移走 
// {
// 	int cnt=0;
// 	int i=0,now=0;	//i表示目标位置，now为当前位置。
// 	while(i<n+1){
// 		i++;
// 		if(a[i]-a[now]<mid){ //两石头间距离小于mid，mid不是最短距离，不满足，移走该石头 
// 			cnt++;
// 		}
// 		else{	//符合，跳过去 
// 			now=i;
// 		}
// 	}
// 	if(cnt<=m) return 1;	//移走的石头个数小于 M，就能保证了任意两剩下的石头间距大于等于最短距离mid，那移走M个，更能保证 
// 	return 0;
// }

// int main(){
// 	cin>>len>>n>>m;
// 	for(int i=1;i<=n;i++){
// 		cin>>a[i];
// 		if(a[i]<mina) mina=a[i];
// 	}
// 	a[0]=0,a[n+1]=len; //首尾都有石头
	
// 	if(n==0){ //特判掉起点和终点之间没有石头的情况，可以想一下为什么。评论区中有答案。感谢 luojias 同学的hack数据！
// 		cout<<len; return 0;
// 	}

// 	//二分答案：检查每一个答案（最短距离mid）是否符合要求 
// 	long long l=1,r=1e10;
// 	while(l<r) //模板2
// 	{
// 		int mid=l+r+1>>1;
// 		if(check(mid)) l=mid; //要的是距离的最大，所以尽可能地往右走 
// 		else r=mid-1;
// 	}
// 	cout<<l;
// 	return 0;
// } 

//木材加工
// using namespace std;

// typedef long long ll;

// const int N = 1e7 + 10;

// int n, k;
// int a[N];
// ll sum1;

// bool check(int x){
//     int sum = 0;

//     for(int i = 0; i < n; i ++){
//         sum += a[i] / x;
//     }

//     if(sum >= k) return true;

//     return false;
// }

// int main(){
//     cin >> n >> k;

//     for(int i = 0; i < n; i ++){
//         cin >> a[i];
//         sum1 += a[i];
//     }
    
//     if(sum1 < k) {cout << 0; return 0;}
    
//     sort(a, a + n); 
    
//     if(k <= n){
//         printf("%d", a[n - k]);
//         return 0;
//     }

//     int l = 1, r = a[n - 1];

//     while(l < r){
//         int mid = l + r + 1 >> 1;
//         if(check(mid)) l = mid;
//         else r = mid - 1;
//     }

//     printf("%d", l);

//     return 0;
// }
//管道
// using namespace std;

// typedef pair<int, int> PII;

// typedef long long ll;

// const int N = 1e6 + 10;

// vector<PII> segs;

// ll n, len;
// ll l, r;
// ll mx = 0;
// ll a[N];
// ll b[N];

// void merge(vector<PII>& segs){
//     vector<PII> res;

//     sort(segs.begin(), segs.end());

//     int st = -2e9, ed = -2e9;

//     for(auto seg : segs){
//         if(ed < seg.first){
//             if(st != -2e9) res.push_back({st, ed});
//             st = seg.first, ed = seg.second;
//         }
//         else ed = max(ed, seg.second);
//     }

//     if(st != -2e9) res.push_back({st, ed});

//     segs = res;
// }

// int main(){
//     cin >> n >> len;

//     for(int i = 0; i < n; i ++){
//         cin >> l >> r;
//         a[i] = l + r;
//         b[i] = l - r;
//         mx = max(mx, r);
//     }

//     for(ll i = mx; i <= mx + 1;i ++){
//         segs.clear();
//         for(ll j = 0; j < n; j ++) segs.push_back({a[j] - i, b[j] + i});
        
//         merge(segs);
//         cout << segs.size() << endl;
//         // sort(segs.begin(), segs.end());

//         // cout << segs.size() << endl;

//         // int l = segs[0].first, r = segs[0].second;

//         // for(ll i = 1; i < n; i ++){
//         //     if(r < segs[i].first - 1)break;
//         //     else r = max(r, segs[i].second);
//         // }

//         if(segs.size() == 1){
//             printf("%lld", i);
//             return 0;
//         }

//     }

//     return 0;
// }
//校门外的树
// using namespace std;

// typedef pair<int, int> PII;

// int x, m;
// int l, r;

// vector<PII> segs;

// int main(){
//     cin >> x >> m;

//     for(int i = 1; i <= m; i ++){
//         cin >> l >> r;
//         segs.push_back({l, r});        
//     }

//     sort(segs.begin(), segs.end());

//     int l = segs[0].first, r = segs[0].second;

//     int sum = 0;

//     for(int i = 0; i < m; i ++){
//         if(r < segs[i].first){
//             sum += r - l + 1;
//             l = segs[i].first, r = segs[i].second;
//         }
//         else r = max(r, segs[i].second);
//     }

//     sum += r - l + 1;

//     printf("%d", x - sum + 1);

//     return 0;
// }
//挤牛奶
// using namespace std;

// typedef pair<int, int> PII;

// int n;
// int l, r;

// vector<PII> segs;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) {
//         cin >> l >> r;
//         segs.push_back({l, r});
//     }

//     sort(segs.begin(), segs.end());

//     int l = segs[0].first, r = segs[0].second;

//     int mx = 0, mn = 0;

//     for(int i = 1; i < n; i ++){
//         if(r < segs[i].first){
//             mx = max(mx, r - l);
//             mn = max(mn, segs[i].first - r);
//             l = segs[i].first, r = segs[i].second;
//         }
//         else r = max(r, segs[i].second);
//     }
    
//     mx = max(mx, r - l);

//     cout << mx << " " << mn;

//     return 0;
// }
// using namespace std;

// typedef pair<int, int> PII;

// int n, l, r;
// vector<PII> segs;

// int main(){
//     cin >> n;

//     for(int i = 0; i < n; i ++){
//         cin >> l >> r;
//         segs.push_back({l, r});
//     }

//     sort(segs.begin(), segs.end());

//     int res1 = 0, res2 = 0;
//     int l = segs[0].first, r = segs[0].second;

//     for(int i = 1; i < n; i ++){
//         if(segs[i].first <= r) r = max(r, segs[i].second);
//         else{
//             res1 = max(res1, r - l);
//             res2 = max(res2, segs[i].first - r);
//             l = segs[i].first, r = segs[i].second;
//         }
//     }

//     res1 = max(res1, r - l);

//     printf("%d %d\n", res1, res2);

//     return 0;
// }
// using namespace std;

// typedef pair<int, int> PII;

// vector<PII> segs;

// int n;
// int l, r;
// int mx = 0, mn = 0x3f3f3f3f;

// void merge(vector<PII>& segs){
//     vector<PII> res;

//     int st = -2e9, ed = -2e9;
//     sort(segs.begin(), segs.end());

//     for(auto seg : segs){
//         if(ed < seg.first){
//             if(st != -2e9) res.push_back({st, ed});
//             st = seg.first, ed = seg.second;
//         }

//         else ed = max(ed, seg.second);
//     }

//     if(st != -2e9) res.push_back({st, ed});

//     segs = res;

// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         cin >> l >> r;
//         segs.push_back({l, r});
//     }

//     merge(segs);

//     for(int i = 0; i < segs.size(); i ++) mx = max(mx, segs[i].second - segs[i].first); 

//     for(int i = 0; i < segs.size() - 1; i ++) mn = min(mn, segs[i + 1].first - segs[i].second);

//     if(segs.size() == 1) printf("%d %d", mx, 0);
//     else printf("%d %d", mx, mn);

//     return 0;
// }

//净化邪恶能量
// using namespace std;

// int x;
// int sum = 0;

// int main(){
//     cin >> x;

//     int a, b, c;

//     while(x){
//         a = x / 10 % 10;
//         b = x / 100;
//         c = x % 10;
//         x -= (a + b + c);
//         sum ++;
//     }

//     printf("%d", sum + 1);

//     return 0;
// }
//区间合并
// using namespace std;

// typedef pair<int, int> PII;

// int n;
// int l, r;

// vector<PII> segs;

// void merge(vector<PII> & segs){
//     vector<PII> res;

//     int st = -2e9, ed = -2e9;
//     sort(segs.begin(), segs.end());

//     for(auto seg : segs){
//         if(ed < seg.first){
//             if(st != -2e9) res.push_back({st,ed});
//             st = seg.first, ed = seg.second;
//         }
//         else ed = max(ed, seg.second);
//     }

//     if(st != -2e9) res.push_back({st, ed});

//     segs = res;
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         cin >> l >> r;
//         segs.push_back({l, r});
//     }

//     merge(segs);

//     cout << segs.size();
//     return 0;
// }

//日期计算
// using namespace std;

// const int months[] = {
//     0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
// };

// int is_leap(int y){
//     if(y % 4 == 0 && y % 100 != 0 || y % 400 == 0) 
//         return 1;

//     return 0;
// }

// int get_days(int y, int m){
//     if(m == 2) return months[m] + is_leap(y);
//     return months[m];
// }

// void next_day(int& y, int& m, int& d){
//     d ++;

//     if(d > get_days(y, m)){
//         d = 1;
//         m ++;

//         if(m > 12){
//             m = 1;
//             y ++;
//         }
//     }
// }

// int main(){
//     int num = 1;
//     int y, ds;
//     cin >> y >> ds;

//     int m = 1, d = 1;
//     while(num < ds){
//         next_day(y, m, d);
//         num ++;
//     }

//     printf("%d\n%d", m, d);
    
//     return 0;
// }

//回文日期
// using namespace std;

// const int months[] = {
//     0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
// };

// int is_leap(int y){
//     if(y % 4 == 0 && y % 100 != 0 || y % 400 == 0)
//         return 1;
//     return 0;
// }

// int get_days(int y, int m){
//     if(m == 2) return months[m] + is_leap(y);
//     return months[m];
// }

// void next_day(int& y, int& m, int& d){
//     d ++;

//     if(d > get_days(y, m)){
//         d = 1;
//         m ++;
//         if(m > 12){
//             m = 1;
//             y ++;
//         }
//     }
// }

// // int calc(int y, int m, int d){
// //     int res = 0;

// //     for(int i = 1; i < y; i ++)
// //         res += 365 + is_leap(i);
    
// //     for(int i = 1; i < m; i ++)
// //         res += get_days(y, i);

// //     res += d;

// //     return res;
// // }

// bool check(int y, int m, int d){
//     if(y / 1000 != d % 10 || y / 100 % 10 != d / 10 || y / 10 % 10 != m % 10 || y % 10 != m / 10) return false;
//         return true;
// }

// int cmp(int y, int m, int d){
//     return y * 10000 + m * 100 + d;
// }

// int main(){
//     int y1, m1, d1, y2, m2, d2;
    
//     scanf("%04d%02d%02d\n%04d%02d%02d", &y1, &m1, &d1, &y2, &m2, &d2);
    
//     int sum = 0;
//     // int sum2 = calc(y2, m2, d2); 
//     // int sum3 = 0;

//     if(check(y1, m1, d1)) sum ++;

//     while(cmp(y1, m1, d1) <= cmp(y2, m2, d2)){
//         // sum3 = calc(y1, m1, d1);
//         next_day(y1, m1, d1);
//         if(check(y1, m1, d1)) sum ++;
//     }

//     printf("%d", sum);

//     return 0;
// }
//日期问题
// using namespace std;

// const int N = 1e2;

// int y, m, d;
// int A, B,C;
// int p[N];

// const int months[] = {
//     0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
// };

// int is_leap(int y){
//     if(y % 4 == 0 && y % 100 != 0 || y % 400 == 0)
//         return 1;
//     return 0;
// }

// int get_days(int y, int m){
//     if(m == 2) return months[m] + is_leap(y);
//     return months[m];
// }

// void next_day(int& y, int& m, int& d){
//     d ++;

//     if(d > get_days(y, m)){
//         d = 1;
//         m ++;

//         if(m > 12){
//             m = 1;
//             y ++;
//         }
//     }
// }

// bool check(int y, int m, int d){
//     if(y % 100 == A && m == B && d == C) return true;
//     if(y % 100 == C && m == A && d == B) return true;
//     if(y % 100 == C && m == B && d == A) return true;

//     return false;
// }

// int main(){

//     scanf("%02d/%02d/%02d", &A, &B, &C);

//     int y = 1960, m = 1, d = 1;

//     while(y <= 2059){
//         if(check(y, m, d)){
//             if(y % 100 >= 60) printf("19%02d-%02d-%02d\n", y % 100, m, d);
//             else printf("20%02d-%02d-%02d\n", y % 100, m, d);
//         }

//         next_day(y, m, d);
//     }
    
//     return 0;

// }
// using namespace std;

// int a, b, c, d, e;
// int sum = 0;
// int m[5];
// int num = 0;

// bool check(int x){
//     int k = 0;
    
//     while(x){
//         m[++ k] = x % 10;
//         x /= 10;
//     }

//     for(int i = 1, j = k; i <= j; i ++, j --) if(m[i] != m[j]) return false;

//     return true;
// }

// bool rvs(int x, int sum){
//     if(sum == 5){
//         if(check(x)) return true;
//         else return false;
//     }
//     a = x % 10;
//     b = x / 10 % 10;
//     c = x / 100 % 10;
//     d = x / 1000;
//     x += a * 1000 + b * 100 + c * 10 + d;
//     return  rvs(x, sum + 1);
// }

// int main(){

//     for(int i = 1; i <= 9999; i ++){
//         if(rvs(i, 0)){
//             cout << i << endl;
//             num ++;
//         }
//     }

//     printf("%d", num + 1);

//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int n, m, x;
// int a[N];
// int b[N];

// int main(){
//     cin >> n >> m >> x;   

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int j = 1; j <= m; j ++) cin >> b[j];

//     for(int i = 1, j = m; i <= n; i ++){
//         while(j >= 0 && a[i] + b[j] > x) j --;
//         if(j >= 0 && a[i] + b[j] == x) printf("%d %d\n", i - 1, j - 1);
//     }
//     // for(int i = 1, j = m; i <= n;){
//     //     while(j >= 1 && b[j] + a[i] > x) j --;
//     //     if(b[j] + a[i] < x) {i ++; continue;}

//     //     if(b[j] + a[i] == x) cout << i - 1 << " " << j - 1;
//     // }


//     return 0;
// }
//日期差值
// using namespace std;

// int y, m, d;

// const int months[] = {
//     0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
// };

// int is_leap(int y){
//     if(y % 4 == 0 && y % 100 != 0 || y % 400 == 0)
//         return 1;
//     return 0;
// }

// int get_days(int y, int m){
//     if(m == 2) return months[m] + is_leap(y);
//     return months[m];
// }

// // void next_day(int y, int m, int d){
// //     d ++;
// //     if(d > get_days(y, m)){
// //         d = 1;
// //         m ++;

// //         if(m > 12){
// //             m = 1;
// //             y ++;
// //         }
// //     }
// // }

// int calc(int y, int m, int d){
//     int res = 0;

//     for(int i = 1; i < y; i ++){
//         res += 365 + is_leap(i);
//     }

//     for(int j = 1; j < m; j ++){
//         res += get_days(y, j);
//     }

//     return res + d;
// }

// int main(){
//     int y1, m1, d1, y2, m2, d2;
//     while(~scanf("%04d%02d%02d\n%04d%02d%02d", &y1, &m1, &d1, &y2, &m2, &d2))
//         printf("%d\n", abs(calc(y1, m1 ,d1) - calc(y2, m2, d2)) + 1);

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int a[N];

// bool cmp(int x, int y){
//     return x > y;
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) a[i] = i;

//     while(m --){
//         int p, q;
//         cin >> p >> q;

//         if(p == 0) sort(a + 1, a + q + 1, greater<int>());
//         else if(p == 1) sort(a + q, a + n + 1);
//     }

//     for(int i = 1; i <= n; i ++) cout << a[i] << " ";

//     return 0;
// }
// using namespace std;

// typedef long long ll;

// ll sum = 0;

// bool merge(int x){
//     if(x == 2 || x == 0 || x == 1 || x == 9) return true;
//     else return false;
// }

// int main(){
//     int a;

//     for(int i = 1; i <= 2019; i ++){
//         int b = i;

//         while(b){
//             a = b % 10;
//             if(merge(a)){
//                 sum += pow(i, 2);
//                 break;
//             } 
//             b /= 10;
//         }
//     }


//     cout << sum;

//     return 0;
// }
// using namespace std;

// int months[] = {
//     0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
// };

// int is_leap(int y){
//     if((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) 
//         return 1;

//     return 0;
// }

// int get_days(int y, int m){
//     if(m == 2) return months[m] + is_leap(y);
//     return months[m];
// }

// void next_day(int &y, int &m, int &d){
//     d ++ ;
//     if(d > get_days(y, m)){
//         d = 1;
//         m ++;
//         if(m > 12){
//             y ++;
//             m = 1;
//         }
//     }
// }

// int main(){
//     int sumday = 0;

//     int y = 1901, m = 1, d = 1;

//     while(y <= 2000){
//         sumday ++;
//         next_day(y, m ,d);
//     }

//     cout << (sumday - 6) / 7;
//     // while(y <= 2000){
//     //     if((d + 2 * m + 3 * (m + 1) / 5 + y + y / 4 - y / 100 + y / 400 )% 7 == 0) sum ++;
//     //     next_day(y, m, d);
//     // }

//     // cout << sum;
    
//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int n;
// int a[N];
// int b[N];
// int sum = 0;

// int main(){
//     cin >> n;   
    
//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n - 2; i ++){
//         for(int j = i + 1; j <= n - 1; j ++){
//             for(int k = j + 1; k <= n; k ++){
//                 if(a[i] < a[j] && a[j] < a[k]){
//                     b[j] ++;
//                     if(b[j] == 1) sum ++;
//                 }
//             }
//         }
//     }

//     cout << sum;
//     return 0;
// }
//质数游戏8
// using namespace std;

// typedef long long ll;

// const int N = 1e3 + 10;

// int n;
// ll a[N];
// ll num = 0;
// ll sum = 0;
// ll mx = 0;

// ll findsize(ll x){
//     num = 0;

//     while(x){
//         x /= 10;
//         num ++;
//     }

//     return num;

// }

// bool isprime(ll x){
//     if(x < 2) return false;

//     for(int i = 2; i <= x / i; i ++){
//         if(x % i == 0) return false;
//     }

//     return true;
// }

// void dfs(ll x){
//     sum = 0;

//     while(x <= n){
//         if(isprime(a[x])) sum += a[x];
//         x += findsize(a[x]);
//     }    

//     mx = max(mx, sum);
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++){
//         sum = 0;
//         dfs(i);
//         // for(int j = i; j <= n;){
//         //     if(isprime(a[j])) sum += a[j];
//         //     j += findsize(a[j]);
//         // }

//         mx = max(mx, sum);
//     }
//     // for(int i = 1; i <= n; i ++){
//     //     if(a[i] >= 2) dfs(i);
//     // }

//     cout << mx;

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// vector<int> g[N];

// int n;

// int bfs(int u){
//     int ans = 0;
//     int cnt = g[u].size();

//     for(int i = 0; i < g[u].size(); i ++){
//         ans = max(ans, bfs(g[u][i]) + cnt);
//     }

//     return ans;
// }

// int main(){
//     cin >> n;

//     for(int i = 2; i <= n; i ++){
//         int x;
//         cin >> x;
//         g[x].push_back(i);
//     }

//     cout << bfs(1);

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// vector<int> g[N];

// int main(){
//     cin >> n;

//     for(int i = 2; i <= n; i ++){
//         int x;
//         cin >> x;
//         g[x].push_back(i);
//     }

//     int ans = dfs(1);

//     cout << ans;
//     return 0;
// }
//bfs - 八数码

// using namespace std;

// int a[10];
// char x;

// int bfs(){

// }

// int main(){

//     for(int i = 1; i <= 9; i ++){
//         cin >> x;
//         if(x == 'x') a[i] = 10;
//         else a[i] = x - '0';
//     }

//     cout << bfs();

//     return 0;
// }
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e3 + 10;

// int g[N][N];
// int d[N][N];
// int n, m;
// PII p[N * N], pre[N][N];

// int bfs(){
//     int hh = 0, tt = 0;
//     p[0] = {0, 0};

//     memset(d, -1, sizeof d);

//     d[0][0] = 0;
//     int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

//     while(hh <= tt){
//         auto t = p[hh ++];

//         for(int i = 0; i < 4; i ++){
//             int x = t.first + dx[i];
//             int y = t.second + dy[i];

//             if(x >= 0 && x < n && y >= 0 && y < m && g[x][y] == 0 && d[x][y] == -1){
//                 p[++ tt] = {x, y};
//                 d[x][y] = d[t.first][t.second] + 1;

//                 pre[x][y] = t;
//             }
//         }
//     }

//     int x = n - 1, y = m - 1;

//     vector<PII> l;
//     while(x || y){
//         l.push_back({x, y});
//         // cout << x << " " << y << endl;//倒着打印路径
//         auto t = pre[x][y];
//         x = t.first, y = t.second;
//     }

//     reverse(l.begin(), l.end());//将路径逆序一下再在下面正序打印
//     for(auto t : l) cout << t.first << " " << t.second << endl;
    
//     return d[n - 1][m - 1];
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 0; i < n; i ++)
//         for(int j = 0; j < m; j ++)
//             cin >> g[i][j];

//     cout << bfs();

//     return 0;
// }
// using namespace std;

// typedef pair<int,int> PII;

// const int N = 10;

// int n, m;
// int g[N][N];
// int d[N][N];
// PII q[N * N], prev[N][N];

// void bfs(){
//     int hh = 0, tt = 0;
//     q[0] = {0, 0};

//     memset(d, -1, sizeof d);

//     d[0][0] = 0;

//     int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

//     while(hh <= tt){
//         auto t = q[hh ++];

//         for(int i = 0; i < 4; i ++){
//             int x = dx[i] + t.first;
//             int y = dy[i] + t.second;

//             if(x > 1 && x <= n && y > 1 && y <= n && g[x][y] == 0 && d[x][y] == -1){
//                 q[++ tt] = {x, y};
//                 d[x][y] = d[t.first][t.second] + 1;

//                 // prev[x][y] = t;

//             }
//         }    
//     }

//     // int x = n - 1, y = m - 1;

//     // while(x || y){
//     //     auto t = prev[x][y];
//     //     x = t.first, y = t.second;
//     // }

//     return d[n - 1][m - 1];
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++)
//             cin >> g[i][j];

//     cout << bfs() << endl;
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int nt[N];
// int bent[N];
// int res = 0;

// void dfs(int x, int start, int size){
//     if(nt[x] == start){
//         res = max(res, size);
//         return ;
//     }

//     dfs(nt[x], start, size + 1);
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         cin >> nt[i];
//         bent[nt[i]] ++;        
//     }

//     for(int i = 1; i <= n; i ++){
//         if(bent[i] != 0){
//             dfs(i, i, 1);
//         }
//     }

//     cout << res;
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int a[N];
// int k = 0;
// int sum = 0;
// bool num, num2;

// int main(){
//     cin >> n;   

//     // while(scanf("%d", &a[k]) != EOF)){
//     //     k ++;
//     // }
//     // while(cin >> a[k]) k ++;
//     // for(int i = 1; i <= n; i ++){
        
//     //     // while(cin >> a[++ k]){
//     //     //     if(cin.get() == '\n') break;
//     //     // }
//     // }

//     sort(a, a + k + 1);

//     for(int i = 0; i < k; i ++){
//         if(!num && (a[i + 1] - a[i]) > 1) {cout << a[i] + 1 << " "; num = 1;}
//         if(!num2 && a[i + 1] == a[i]) {cout << a[i] << " "; num2 = 1;}
//         if(num && num2) return 0;
//     }

//     return 0;
// }
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e5 + 10;

// int a[N];
// int n;
// int l, r;
// void merge(vector<PII> &segs){

//     vector<PII> res;

//     sort(segs.begin(), segs.end());

//     int st = -2e9, ed = -2e9;

//     for(auto seg : segs){
//         if(ed < seg.first){
//             if(st != -2e9) res.push_back({st, ed});
//             st = seg.first, ed = seg.second;
//         }
//         else ed = max(ed, seg.second);
//     }

//     if(st != -2e9) res.push_back({st, ed});

//     segs = res;
// }

// int main(){

//     vector<PII> segs;

//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         cin >> l >> r;
//         segs.push_back({l, r});
//     }

//     merge(segs);

//     cout << segs.size();
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m, k;
// int a[N], b[N];


// int main(){
//     scanf("%d %d %d", &n, &m, &k);

//     for(int i = 1; i <= n; i ++) cin >> a[i];
//     for(int i = 1; i <= m; i ++) cin >> b[i];

//     for(int i = 1, j = m - 1; i < n; i ++){
//         while(j >= 0 && a[i] + b[j] > k) j --;
//         if(j >= 0 && a[i] + b[j] == k) printf("%d %d\n", i, j);
//     }
    
//     return 0;
// }
// #include <iostream>
// #include <algorithm>
// using namespace std;
// int main()
// {
//     double num[33][33] = { 0 };
//     for (int i = 1; i < 30; i++)
//     {
//         for (int j = 1; j <= i; j++)
//         {
//             cin >> num[i][j];
//         }
//     }
//     for (int i = 1; i <= 30; i++)
//     {
//         for (int j = 1; j <= i; j++)
//         {
//             num[i][j] += (num[i-1][j - 1] / 2) + (num[i-1][j] / 2);
//         }
//     }
//     sort(num[30],num[30] + 31);
//     long long int sum = (num[30][30] * (2086458231 * 1.0 / num[30][1]));
//     cout << num[30][30] << endl << num[30][1] << endl;
//     //cout << sum << endl;
//     // cout << 72665192664 << endl;
//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// double dp[N][N];

// double mx, ma;

// int main(){
//     for(int i = 1; i <= 29 ; i ++){
//         for(int j = 1; j <= i; j ++){
//             cin >> dp[i][j];
//         }
//     }

//     for(int i = 2; i <= 30; i ++){
//         for(int j = 1; j <= i; j ++){
//             dp[i][j] += dp[i - 1][j] / 2.0 + dp[i - 1][j - 1] / 2.0;
//         }
//     }

//     mx = dp[30][1];
//     ma = dp[30][1];

//     for(int i = 1; i <= 30; i ++) {
//         mx = min(dp[30][i], mx);
//         ma = max(dp[30][i], ma);
//     }

//     printf("%.0lf", 2086458231  * 1.0 / mx * ma);
    
//     // cout << mx << endl << ma; 
//     return 0;
// }
//回文日期
// using namespace std;

// const int months[] = {
//     0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
// };

// int is_leap(int year){
//     if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
//         return 1;
//     return 0;
// }

// int get_days(int y, int m){
//     if(m == 2) return 28 + is_leap(y);
//     return months[m];
// }

// void next_day(int & y, int & m, int & d){
//     d ++;
//     if(d > get_days(y, m)){
//         d = 1;
//         m ++;
//         if(m > 12){
//             m = 1;
//             y ++;
//         }
//     } 
// }

// bool check1(char s []){
//     for(int i = 0, j = 7; i <= j; i ++, j --){
//         if(s[i] != s[j])
//             return false;
//     } 

//     return true;
// }

// bool check2(char s[]){
//     char a = s[0], b = s[1], c = s[2], d = s[3];
//     if(a == c && b == d && a != b) 
//         return true;

//     return false;
// }

// int main(){
//     int y, m, d;
//     scanf("%04d%02d%02d", &y, &m, &d);

//     bool found1 = false, found2 = false;
//     while(!found1 || !found2){
//         next_day(y, m, d);
//         char s[10] = {0};
//         sprintf(s, "%04d%02d%02d", y, m, d);
//         if(check1(s)){
//             if(!found1){
//                 puts(s);
//                 found1 = true;
//             }
//             if(!found2 && check2(s)){
//                 found2 = true;
//                 puts(s);
//             }
//         }
//     }

//     return 0;
// }

// #include<bits/stdc++.h>
// using namespace std;
// int b[]={0,31,28,31,30,31,30,31,31,30,31,30,31};
// int a[50];
// int h,m,s;
// set<int>q;//用来排重
// int main()
// {  
//     for(int i=1;i<=40;i++)//年份已固定，只用从后40位枚举
//     {
//         cin>>a[i];
//     }
//     for(int i=1;i<=40;i++)
//         for(int j=i+1;j<=40;j++)
//          for(int k=j+1;k<=40;k++)
//              for(int p=k+1;p<=40;p++)
//              {
//                  h=a[i]*1000+a[j]*100+a[k]*10+a[p];
//                  m=a[i]*10+a[j];
//                  s=a[k]*10+a[p];
//                  if(m>0&&m<=12&&s>0&&s<=b[m])
//                      q.insert(h);
//              }
//     cout<<q.size()<<endl;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int a[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
// int b[102];
// int c[N];
// int sum = 0;

// int main(){
//     for(int i = 1; i <= 38; i ++) cin >> b[i];

//     for(int i = 1; i <= 38; i ++){
//         if(b[i] == 1){
//             for(int j = 1; j <= 38; j ++){
//                 if(b[j] == 0 || b[j] == 1 || b[j] == 2){
//                     for(int k = 1; k <= 38; k ++){
//                         for(int l = 1; l <= 38; l ++){
//                             if(b[k] * 10 + b[l] == 0) continue;
//                             else if(b[k] * 10 + b[l] <= a[b[i] * 10 + b[j]]) {
//                                 c[b[i] * 1000 + b[j] * 100 + k * 10 + b[j]] ++;
//                                 if(c[b[i] * 1000 + b[j] * 100 + b[k] * 10 + b[j]] == 1) sum ++;
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         else if(b[i] == 0){
//             for(int j = 1; j <= 38; j ++){
//                 if(b[j] == 0) continue;
//                 else{
//                     for(int k = 1; k <= 38; k ++){
//                         for(int l = 1; l <= 38; l ++){
//                             if(b[k] * 10 + b[l] == 0) continue;
//                             else if(b[k] * 10 + b[l] <= a[b[j]]) {
//                                 c[b[i] * 1000 + b[j] * 100 + b[j]] ++;
//                                 if(c[b[i] * 1000 + b[j] * 100 + b[j]] == 1) sum ++;
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//     }
    
//     printf("%d", sum);
//     return 0;
// }
//受伤的皇后 
// using namespace std;

// const int N = 11;

// int n;
// int a[N][N];
// int sum = 0;
// int num = 0;

// bool row(int x){
//     for(int i = 1; i <= n; i ++){
//         if(a[x][i] == 1) return false;
//     }

//     return true;
// }

// bool col(int y){
//     for(int i = 1; i <= n; i ++){
//         if(a[i][y] == 1) return false;
//     }

//     return true;
// }

// bool dg(int x, int y){
//     for(int i = 0; i <= 2; i ++){
//         if(x - i >= 1 && y - i >= 1 && a[x - i][y - i] == 1) return false;
//         if(x + i <= 2 && y + i <= 2 && a[x + i][y + i] == 1) return false;
//     }

//     return true;
// }

// bool udg(int x, int y){
//     for(int i = 0; i <= 2; i ++){
//         if(a[x + i][y - i] == 1 || a[x - i][y + i] == 1) return false;
//     }

//     return true;
// }

// void dfs(int x){
//     if(x == n + 1 && num == 4){
//         sum ++;
//         return ;
//     }

//     for(int i = 1; i <= n; i ++){
//         if(row(i) && col(i) && dg(x, i) && udg(x, i)){
//             a[x][i] = 1;
//             dfs(x + 1);
//             a[x][i] = 0;
//         }
//     }
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= n; j ++)   
//             a[i][j] = 0;

//     for(int i = 1; i <= n; i ++)
//         dfs(i);

//     printf("%d", sum);    

//     return 0;
// }
//直线

// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int m;
// int w[N], h[N];
// int t = 0;
// int sum = 0;
// int mx = 0;

// int main(){
//     cin >> m >> n;

//     for(int i = 1; i <= n; i ++) cin >> w[i] >> h[i];

//     for(int i = 1; i <= n; i ++){
//         sum += w[i];
//         if(sum >= m) break;
//         if(h[i] > = mx) mx = h[i], t = i;
//     }

//     sum = 0;

//     for(int i = 1; i <= n; i ++){
//         sum += w[i];
//         if(sum < w[i]) continue;

//         else  
//     }
//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// double a[N];

// int main(){
//     int l = 0;
//     for(int i = 0; i < 20; i ++)
//         for(int j = 0; j < 21; j ++)
//             for(int k = 0; k < 20; k ++)
//                 for(int m = 0; m < 21; m ++){
//                     a[l ++] = m - j / 
//                 }
// }
//字母数
// using namespace std;

// int sum = 0;

// int main(){

//     for(int i = 0; i <= 15; i ++){
//         sum += 10 * pow(16, i);
//         if(sum >= 2022){
//             printf("%d", sum);
//             break;
//         }
//     }   
    
//     return 0;
// }
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 1e5 + 10;

// int n;
// int l[N], r[N];
// PII p[N * N];

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         cin >> l[i] >> r[i];
//         p.push_back(l[i], r[i]);
//     }

//     int s = 2e-9, t = 2e-9;

//     for(int i = 1; i <= n; i ++){

//     }
//     return 0;
// }
// using namespace std;

// const int N = 1e9 + 10;

// int sum = 0;

// void check(int p, int d){

//     int i = 2;

//     while(i <= min(p, d)){
//         if(p % i == 0 && d % i == 0){
//             p /= i; 
//             d /= i;
//             continue;
//         }
//         i ++;
//     }

//     printf("%d/%d", p, d);
// }

// int main(){
    
//     for(int i = 19; i >= 0; i --)
//         sum += pow(2, i);
    
//     int x = pow(2, 19);
    
//     check(sum, x);

//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int n;
// char a[N][N];
// char b[N][N];
// int sum = 0;

// void dfs(int u){
//     if(u == n + 1) return ;

//     for(int i = 1; i <= n; i ++)
//         if(b[u][i] == '#' && b[u - 1][i] == '.' || b[u][i + 1] == '.' || b[u + 1][i] == '.' || b[u][i - 1] == '.')
//             a[u][i] = '.';
        
//     dfs(u + 1);
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= n; j ++){ 
//             cin >> a[i][j];
//             b[i][j] = a[i][j];
//         }
    
//     dfs(1);

//      for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= n; j ++)
//             if(a[i][j] == '#') sum ++;

//     printf("%d", sum);
//     return 0;
// }
//杨辉三角
// using namespace std;

// typedef long long ll;

// const int M = 1e4;

// ll n;
// ll dp[M][M];
// int sum = 0;

// int main(){
//     cin >> n;

//     dp[1][1] = 1;

//     for(int i = 1; i <= M; i ++){
//         for(int j = 1; j <= i; j ++){
//             if(j == 1) dp[i][j] = 1;
//             else dp[i][j] = dp[i - 1][j] + dp[i - 1][j - 1];
//             sum ++;
//             if(dp[i][j] == n){
//                 printf("%d", sum);
//                 return 0;
//             } 
//         }
//     }

//     return 0;
// }

//九宫重排
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 10;

// int a[N][N];
// // char a[N];
// // char b[N];
// int x[4] = {-1, 0, 1, 0};
// int y[4] = {0, 1, 0, -1};

// PII p[N * N];

// void bfs(int u){

// }

// int main(){

//     for(int i = 1; i <= 3; i ++)
//         for(int j = 1; j <= 3; j ++){
//             cin >> a[i][j];
//             if(a[i][j] == '.') a[i][j] = 0;
//         }

//     return 0;
// }

//移动
// using namespace std;

// const int N = 1e3 + 10;

// char str[N];

// int main(){
//     scanf("%s",str + 1);
    
//     int x = 0, y = 0;

//     int i = 1;

//     while(str[i] != '\0'){
//         if(str[i] == 'D') x ++;
//         if(str[i] == 'R') y ++;
//         if(str[i] == 'L') y --;
//         if(str[i] == 'U') x --;

//         i ++;
//     }

//     printf("%d %d", x, y);

//     return 0;
// }
// using namespace std;

// int sum = 0;

// void isprime(int x){
//     for(int i = 2; i <= x / i; i ++)
//         if(x % i == 0){
//             sum ++;
//             return ;
//         }

// }

// int main(){
//     for(int i = 4; i <= 2020; i ++)
//         isprime(i);

//     printf("%d", sum);
//     return 0;
// }
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// ll n, m, x;
// ll a[N] = {0};
// ll b[N] = {0};

// int main(){
//     cin >> n >> m >> x;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= m; i ++) cin >> b[i];

//     for(int l = 1, r = 1; r <= m; ){

//         if(a[l] + b[r] < x) r ++;
//         else if(a[l] + b[r] > x) r --, l ++;
//         else {
//             printf("%d %d", l - 1, r - 1);
//             l ++;
//             if(a[l] + b[r] > x) break;
//         }

//     }

//     // for(int l = 1, r = 1; r <= n; ){

//     //     if(a[r] + b[l] < x) r ++;
//     //     else if(a[r] + b[l] > x) r --, l ++;
//     //     else {
//     //         printf("%d %d", l - 1, r - 1);
//     //         l ++;
//     //         if(a[r] + b[l] > x) break;
//     //     }

//     // }
//     // for(int l = 1, r = 1; ; ){
//     //     if(a[l] + b[r] < x) r ++;
//     //     else if(a[l] + b[r] > x) {
//     //         if(b[r] - a[l] > 0)r --, l ++;
//     //         else if(a[l] > b[r]) l --, r ++;
//     //         else r --, l --;
//     //     }
//     //     else printf("%lld %lld", l - 1, r - 1), l ++;
//     // }

//     // for(int l = 1, r = 1; l <= n && r <= l; l ++)
//     // {
//     //     if(a[l] + b[r] < x) continue;
//     //     else if(a[l] + b[r] > x) r ++, l --;
//     //     else printf("%lld %lld", l - 1, r - 1);
//     // }
    
//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int n, l;
// int c[N];

// int main(){
//     cin >> n >> l;
    
//     for(int i = 1; i <= n; i ++) cin >> c[i];


//     return 0;   
// }
// using namespace std;

// const int N = 1e5 + 10;

// int a[N];
// int n;
// int b[N];
// int mx = 0;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1, j = 1; j <= n; j ++){
//         b[a[j]] ++;

//         while(b[a[j]] > 1){
//             b[a[i]] --;
//             i ++;
//         }

//         mx = max(mx, j - i + 1);
//     }

//     printf("%d", mx);

//     return 0;
// }
// using namespace std;

// typedef long long ll;

// const int N = 5e3 + 10;

// int n, r, p;
// int x[N], y[N], w[N][N];
// int s[N][N];
// int mx = 0;
// int sum =0;

// int main(){
//     cin >> n >> r;

//     for(int i = 1; i <= n; i ++){
//         cin >> x[i] >> y[i] >> p; 
//         w[++ x[i]][++ y[i]] += p;
//         sum += p;
//     }

//     if(r >= 5001){
//         printf("%d", sum);
//         return 0;
//     }

//     for(int i = 1; i <= 5001; i ++){
//         for(int j = 1; j <= 5001; j ++){
//             w[i][j] += w[i - 1][j] + w[i][j - 1] - w[i - 1][j - 1];
//         }
//     }

//     for(int i = r; i <= 5001; i ++)
//         for(int j = r; j <= 5001; j ++)
//             mx = max(mx, w[i][j] - w[i][j - r] - w[i - r][j] + w[i - r][j - r]);

//     printf("%d", mx);

//     return 0;

// }

// using namespace std;

// typedef long long ll;

// const int N = 1e4 + 10;

// int w[N][N];
// int n, p;
// int x[N], y[N], w[N];
// ll r;
// ll sum = 0;

// int main(){
//     cin >> n >> r;

//     for(int i = 1; i <= n; i ++) {
//         cin >> x[i] >> y[i] >> p;
//         w[x[i]][y[i]] += p;
//         sum += p;
//     }

//     if(r >= 5000) printf("%lld", sum);

//     return 0;
// }
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int a[N], b[N], c[N];
// int n;
// int cnta[N], cntc[N], sa[N], sc[N];

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         scanf("%d", &a[i]);
//         cnta[++ a[i]] ++;
//     }

//     sa[0] = cnta[0];

//     for(int i = 1; i < N; i ++) sa[i] = sa[i - 1] + cnta[i];

//     for(int i = 1; i <= n; i ++) scanf("%d", &b[i]), b[i] ++;

//     for(int i = 1; i <= n; i ++){
//         scanf("%d", &c[i]);
//         cntc[++ c[i]] ++;
//     }

//     sc[0] = cntc[0];

//     for(int i = 1; i < N; i ++) sc[i] = sc[i - 1] + cntc[i];

//     ll ans = 0;
    
//     for(int i = 1; i <= n; i ++){
//         int x = b[i];

//         ans += (ll)sa[x - 1] * (sc[N - 1] - sc[x]);
//     }

//     printf("%lld", ans);

//     return 0;
// }

// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;
// int a[N], b[N], c[N];
// int cnta[N], cntb[N], sa[N], sb[N];

// int main(){

//     scanf("%d", &n);

//     for(int i = 1; i <= n; i ++){
//         scanf("%d", &a[i]);
//         cnta[++a[i]] ++;
//     }

//     sa[0] = cnta[0];

//     for(int i = 1; i <= n; i ++) cin >> b[i];
//     // cin >> n;

//     // for(int i = 1; i <= n; i ++) cin >> a[i];

//     // for(int i = 1; i <= n; i ++) cin >> b[i];

//     // for(int i = 1; i <= n; i ++) cin >> c[i];


//     return 0;
// }
// using namespace std;

// typedef long long ll;

// const int N = 5e2 + 10;

// ll n, m, k;
// int a[N][N];
// ll num = 0;

// int main(){
//     cin >> n >> m >> k;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++){
//             cin >> a[i][j];
//             a[i][j] += a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1];
//         }

//     for(int i = 1; i <= m; i ++)
//         for(int j = i; j <= m; j ++)
//             for(int s = 1, t = 1; t <= n; t ++){
//                 while(s <= t && a[t][j] - a[s - 1][j] - a[t][i - 1] + a[s - 1][i - 1] > k) s ++;
//                 if(s <= t) num += t - s + 1;
//             }

//     printf("%lld", num);

//     return 0;
// }

// using namespace std;

// typedef long long ll;

// const int N = 5e2 + 10;

// int n, m;
// ll k;
// int a[N][N];
// ll num = 0;

// int main(){
//     cin >> n >> m >> k;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++){
//             cin >> a[i][j];
//             a[i][j] += a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1];
//         }

    
//     for(int x1 = 1; x1 <= n; x1 ++)
//         for(int y1 = 1; y1 <= m; y1 ++)
//             for(int x2 = x1; x2 <= n; x2 ++)
//                 for(int y2 = y1; y2 <= m; y2 ++){
//                     if(a[x2][y2] - a[x2][y1 - 1] - a[x1 -1][y2] + a[x1 - 1][y1 - 1] <= k)
//                     num ++;
//                 }    

//     printf("%lld", num);
//     return 0;

// }
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// ll n, k, num;
// ll a[N], res[N];

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++) cin >> a[i]; 

//     for(int i = 1; i <= n; i ++) a[i] += a[i - 1];

//     // for(int i = 1; i <= n; i ++)
//     //     for(int j = i; j <= n; j ++)
//     //         if((a[j] - a[i - 1]) % k == 0) num ++;

//     //(由a[j] - a[i - 1]) % k == 0 优化为当a[j] % k == a[i - 1] % k 去统计与a[j] % k 的值相同的数量
    
//     for(int i = 1; i <= n; i ++){

//         num += res[a[i] % k];

//         res[a[i] % k] ++; //若再次出现res[a[i] % k]则这个res[a[i] % k]与前面所有出现过的res[a[i] % k]都能组成一个区间所以for循环以后是先加入num再进行次数统计

//     }

//     printf("ll%d", num + res[0]);

//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int a[N][N];
// int n, m;
// int q;
// int b[N][N];

// void insert(int x1, int y1, int x2, int y2){

// }

// int main(){
//     cin >> n >> m >> q;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++)
//             cin >> a[i][j];

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++)
//             a[i][j] += a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1];

//     while(q --){
//         int x1, y1, x2, y2;
//         cin >> x1 >> y1 >> x2 >> y2;
        
//         printf("%d\n", a[x2][y2] - a[x1 - 1][y2] - a[x2][y1 - 1] + a[x1 - 1][y1 - 1]);

//     }

//     return 0;
// }
// using namespace std;

// const int N = 5e6 + 10;

// int n;
// int t;
// char str[N];
// int s[N];
// int mx = 0;
// int num = 0;

// int main(){
//     cin >> t;

//     while(t --){

//         mx = 0;

//         num ++;

//         cin >> n;
//         scanf("%s", str + 1);

//         for(int i = 1; i <= n; i ++) s[i] = s[i - 1] + str[i] - '0';

//         for(int i = 1; i <= n - (n + 1) / 2 + 1; i ++)
//             if(s[i + (n + 1) / 2 - 1] - s[i - 1] >= mx) mx = s[i + (n + 1) / 2 - 1] - s[i - 1];

//         printf("Case #%d: %d\n", num, mx);

//     }

//     return 0;
// }
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// ll n, m;
// ll a[N];
// ll sum[N];
// ll num1 = 0, num2 = 0;
// ll b[N];

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++) sum[i] = sum[i - 1] + a[i];

//     cin >> m;

//     while(m --){
//         int l, r;
//         cin >> l >> r;

//         num1 += sum[r] - sum[l - 1];

//         b[l] ++;
//         b[r + 1] --;
//     }

//     for(int i =  1; i <= n; i ++) b[i] += b[i - 1];

//     sort(b + 1, b + n + 1);

//     sort(a + 1, a + n + 1);

//     for(int i = n; i >= 1; i --){
//         num2 += b[i] * a[i];
//     }

//     cout << num2 - num1;

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int a[N];
// int sum[N];

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = n; i >= 1; i --) a[i] -= a[i - 1];

//     while(m --){
//         int l, r, c;
//         cin >> l >> r >> c;
        
//         a[l] += c;
//         a[r + 1] -= c;
//     }

//     for(int i = 1; i <= n; i ++){
//         sum[i] = sum[i - 1] + a[i];
//         printf("%d ", sum[i]);
//     }
    
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int p[N], t[N];
// int pos = 0, neg = 0;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) cin >> p[i];

//     for(int i = 1; i <= n; i ++){
//         int t;
//         cin >> t;
//         p[i] -= t;
//     }

//     for(int i = n; i >= 1; i --) p[i] -= p[i - 1];

//     for(int i = 1; i <= n; i ++){
//         if(p[i] > 0) pos += p[i];
//         else neg -= p[i];
//     }

//     printf("%d", max(pos, neg));

//     return 0;
// }
// using namespace std;

// const int N = 2e3 + 10;

// int n, m;
// int a[N][N];
// int b[N][N];

// void insert(int x1, int y1, int x2, int y2){
//     b[x1][y1] ++;
//     if(b[x1][y2 + 1] == 0) b[x1][y2 + 1] = 1;
//     else b[x1][y2 + 1] --;
//     if(b[x2 + 1][y1] == 0) b[x2 + 1][y1] = 1;
//     else b[x2 + 1][y1] --;
//     b[x2 + 1][y2 + 1] ++;
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= n; j ++)
//             a[i][j] = 0;

//     while(m --){
//         int x1, y1, x2, y2;
//         cin >> x1 >> y1 >> x2 >> y2;
//         insert(x1, y1, x2, y2);
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= n; j ++){
//             b[i][j] += b[i - 1][j] + b[i][j - 1] - b[i - 1][j - 1];
//             printf("%d", b[i][j] % 2);
//         }

//         printf("\n");
//     }

//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int n, m, q;
// int a[N][N];
// int b[N][N];

// void insert(int x1, int y1, int x2, int y2, int c){
//     b[x1][y1] += c;
//     b[x2 + 1][y1] -= c;
//     b[x1][y2 + 1] -= c;
//     b[x2 + 1][y2 + 1] += c;
// }

// int main(){
//     cin >> n >> m >> q;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++)
//             cin >> a[i][j];

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++)
//             insert(i, j, i, j, a[i][j]);

//     while(q --){
//         int x1, y1, x2, y2, c;
//         cin >> x1 >> y1 >> x2 >> y2 >> c;

//         insert(x1, y1, x2, y2, c);
//     } 

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             b[i][j] += b[i - 1][j] + b[i][j - 1] - b[i - 1][j - 1];
//             printf("%d ", b[i][j]);
//         }
//         printf("\n");
//     }

//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int a[N][N];
// int n, m;
// int q;

// int main(){
//     cin >> n >> m >> q;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= m; j ++)
//             cin >> a[i][j];

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             s[i][j] = s[i - 1][j] + s[i][j - 1] - s[i - 1][j - 1] + a[i][j];
//         }
//     }

//     while(q --){
//         int x1, y1, x2, y2;
//         cin >> x1 >> y1 >> x2 >> y2;

        
//     }

//     return 0;
// }

// using namespace std;

// const int N = 3000;

// int a[N][N];

// int n, m;
// int x, y, c, v;

// void insert(int x, int y, int c, int v){
//     a[x][y] ++;
//     if(a[x][v + 1] == 0) a[x][v + 1] = 1;
//     else a[x][v + 1] --;
//     if(a[c + 1][y] == 0) a[c + 1][y] = 1;
//     else a[c + 1][y] --;
//     a[c + 1][v + 1] ++;
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= n; j ++)
//             a[i][j] = 0;
    
//     cout << a[3][3] << " = " << a[3][3];
//     for(int i = 1; i <= m; i ++){
//         cin >> x >> y >> c >> v;

//         insert(x, y, c, v);
//         cout << a[3][3] << " = " << a[3][3];
//         cout << endl;
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; i <= n; i ++){
//             a[i][j] += a[i - 1][j] + a[i][j - 1] -a[i - 1][j - 1];
//         }
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= n; j ++){
//             cout << a[i][j] % 2;
//         }
//         printf("\n");
//     }

//     return 0;
// }
// using namespace std;

// void merge_sort(int a[], int l, int r){
//     if(l >= r) return ;

//     merge(a, l, mid), merge(a, mid + 1, r);

//     int k = 0, i = l, j = mid + 1;

//     while(i <= mid && j <= r){
//         if(a[i] <= a[j])temp[k ++] = a[i ++];
//         else temp[k ++] = a[j ++];
//     }
//     while(i <= mid) temp[k ++] = a[i ++];
//     while(j <= r) tmp[k ++] = a[j ++];

//     for(int i = l, j = 0; i <= r; i ++, j ++) a[i] = temp[j];
// }

// int main(){
//     int n;
//     cin >> n;

//     for(int i = 0; i < n; i ++) cin >> a[i];

//     merge_sort(a, 0, n - 1);
//     for(int i = 0; i < n; i ++) cout << a[i] << " ";

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int p[N], t[N];
// int n;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++) scanf("%d", &p[i]);

//     for(int i = 1; i <= n; i ++) scanf("%d", &t[i]);


//     return 0;
// }
// using namespace std;

// typedef long long ll;

// const int N = 1e6 + 10;

// ll a[N];
// int n, m;
// ll x;

// int main(){
//     cin >> n >> m;
    
//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     while(m --){
//         cin >> x;

//         int l = 1, r = 1e9;
//         while(l < r){
//             int mid = l + r >> 1;

//             if(a[mid] < x) l = mid + 1;
//             else r = mid;
//         }

//         cout << l << " ";
//     }
    
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int a[N];
// int l, r;
// int s[N];

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++) s[i] = s[i - 1] + a[i];
//     while(m --){
//         cin >> l >> r;

//         cout << s[r] - s[l - 1] << endl;
//     }

//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int n, q, k;
// int a[N];

// int SL(int x, int y){

//     if(x > y) return true;
//     else return false;

// }

// int SR(int x){

//     if(x < y) return true;
//     else return false;

// }

// int main(){
//     cin >> n >> q;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     while(q --){
//         cin >> k;

//         int l = 1, r = 1e5 + 10;

//         while(l < r){
//             int mid = l + r >> 1;

//             if(SL(mid, k)) r = mid;
//             else l = mid + 1;
//         }        

        
//         while(l < r){
//             int mid = l + r + 1 >> 1;

//             if(SR(mid, k)) l = mid;
//             else r = mid - 1;
//         }
//     }

//     return 0;

// }
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;
// ll A[N], B[N];
// ll l, r;

// int SL(ll x){
//     for(int i = 1; i <= n; i ++){
//         if(A[i] / x > B[i])
//         return false;
//     }

//     return true;
// }

// int SR(ll x){
//      for(int i = 1; i <= n; i ++){
//         if(A[i] / x < B[i])
//         return false;
//     }

//     return true;
// }

// int main(){
//     cin >> n;
    
//     for(int i = 1; i <= n; i ++) cin >> A[i] >> B[i];

//     l = 1, r = 1e9;

//     while(l < r){
//         int mid = l + r >> 1;
//         if(SL(mid)) r = mid;
//         else l = mid + 1;
//     }   

//     printf("%d ", l);

//     r = 1e9;

//     while(l < r){
//         int mid = l + r + 1 >> 1;
//         if(SR(mid)) l = mid;
//         else r = mid - 1;
//     }

//     printf("%d", l);
//     return 0;
// }
// using namespace std;

// const int N = 1e6 + 10;

// int n, k;
// int h[N], w[N];

// bool check(int a){
//     int num = 0;
//     for(int i = 0; i < n; i ++){
//         num += (w[i] / a) * (h[i] / a);
//         if(num >= k) return true;
//     }

//     return false;
// }

// int main(){
//     cin >> n >> k;

//     for(int i = 0; i < n; i ++){
//         cin >> w[i] >> h[i];
//     }
//     int l = 1, r = 1e5;

//     while(l < r){
//         int mid = l + r + 1 >> 1;

//         if(check(mid)) l = mid;
//         else r = mid - 1;
//     }

//     cout << r;
//     return 0;
// }
// using namespace std;

// typedef long long LL;

// const int N = 1e6 + 10;

// int n, m;
// int w[N];
// int d[N], s[N], t[N];
// LL b[N];

// bool check(int x){
//     memset(b, 0, sizeof b);

//     for(int i = 1; i <= x; i ++){
//         b[s[i]] += d[i];
//         b[t[i] + 1] -= d[i];
//     }

//     for(int i = 1; i <= n; i ++){
//         b[i] += b[i - 1];
//         if(b[i] > w[i]) return false;
//     }

//     return true;
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> w[i];

//     for(int i = 1; i <= m; i ++) cin >> d[i] >> s[i] >> t[i];

//     int l = 0, r = m;
//     while(l < r){
//         int mid = l + r + 1 >> 1;

//         if(check(mid))l = mid;
//         else r = mid - 1;
//     }

//     if(l == m) printf("%d", 0);
//     else printf("-1\n%d", r + 1);

//     return 0;
// }
// using namespace std;

// const int N = 50;

// typedef pair<int, int> PII;

// int n;
// int a[N][N];
// PII q[N * 2];
// int dx[4] = {0, -1, 0, 1}, dy[4] = {-1, 0, 1, 0};
// int d[N][N];
// int b[N][N];

// void bfs(){
//     int hh = 0, tt = 0;
//     b[0][0] = 1;
//     q[0] = {1, 1};

//     memset(d, -1, sizeof d);

//     d[0][0] = 0;

//     while(hh <= tt){
//         auto t = q[hh ++];

//         for(int i = 0; i < 4; i ++){
//             int x = t.first + dx[i], y = t.second + dy[i];
//             if(d[x][y] == -1 && x >= 1 && x <= n && y >= 1 && y <= n && b[x][y] == 0){
//                 b[x][y] = 1;
//                 d[x][y] = d[t.first][t.second] + 1;
//                 q[++ tt] = {x, y};
//             }
//         }
//     }

//     // return d[n - 1][n - 1];
// }

// int main(){

//     cin >> n;
//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= n; j ++){
//             cin >> a[i][j];
//             if(a[i][j] == 1) b[i][j] = 1;
//             else b[i][j] = 0;
//         }

//     bfs();

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= n; j ++){
//             if(!b[i][j]) cout << "2" << " ";
//             else cout << a[i][j] << " ";
//         }

//         cout << endl;
//     }
//     return 0;
// }
// using namespace std;

// const int N = 20;

// int n;
// int a[N];
// int col[N * 2], dg[N * 2], udg[N * 2];
// bool st[N][N];
// int total;

// void print()
// {
//     if(total<=2)//保证只输出前三个解，如果解超出三个就不再输出，但后面的total还需要继续叠加
//     {
//         for(int k=1;k<=n;k++)
//         cout<<a[k]<<" ";//for语句输出
//         cout<<endl;
//     }
//     total++;//total既是总数，也是前三个排列的判断
// }
// void dfs(int u){
//     if(u == n + 1){
//         // for(int i = 0; i < n; i ++) cout << a[i];
//         print();
//         return ;
//     }

//     for(int i = 1; i <= n; i ++){
//         if(!st[u][i] && !col[i] && !dg[i + u] && !udg[n - i + u]){
//             a[u] = i;
//             st[u][i] = col[i] = dg[u + i] = udg[n - i + u] = true;
//             dfs(u + 1);
//             st[u][i] = col[i] = dg[u + i] = udg[n - i + u] = false;
//         }
//     }
// }

// int main(){
//     cin >> n;

//     dfs(1);

//     cout << total;

//     return 0;
// }
// using namespace std;

// const int N = 1e7 + 10;

// int n, m;
// int p[N], a[N];
// int sum = 0;
// int cnt = 0;

// int main(){
//     cin >> n >> m;    

//     for(int i = 1; i <= m; i ++) cin >> p[i] >> a[p[i]];

//     sort(p + 1, p + m + 1);

//     if(n == 0 || m == 0){
//         cout << "0";
//         return 0;
//     }
//     for(int i = 1; i <= m; i ++){
//         while(a[p[i]]){
//             a[p[i]] --;
//             sum ++;
//             cnt += p[i];  
//             if(sum == n){
//                 cout << cnt;
//                 return 0;
//             }
//         }
//     }

//     return 0;
// }

// using namespace std;

// typedef pair<int, int> PII;

// const int N = 110;

// int n, m;
// int g[N][N];
// int d[N][N];
// PII p[N * N], pre[N][N];

// int bfs(){
//     int hh = 0, tt = 0;
//     p[0] = {0, 0};

//     memset(d, -1, sizeof d);
//     d[0][0] = 0;

//     int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

//     while(hh <= tt){
//         auto t = p[hh ++];

//         for(int i = 0; i < 4; i ++){
//             int x = t.first + dx[i], y = t.second + dy[i];

//             if(x >= 0 && x < n && y >= 0 && y < m && d[x][y] == -1 && g[x][y] == 0){
//                 d[x][y] = d[t.first][t.second] + 1;

//                 p[++ tt] = {x, y};
//             } 
//         }
//     }

//     return d[n - 1][m - 1];

// }

// int main(){
//     cin >> n >> m;
//     for(int i = 0; i < n; i ++)
//         for(int j = 0; j < m; j ++)
//             cin >> g[i][j];

//     cout << bfs() << endl;

//     return 0;
// }
// using namespace std;

// const int N = 10;

// int n;
// char a[N][N];
// bool col[N * 2], dg[N * 2], udg[N * 2];

// void dfs(int u){
//     if(u == n + 1){
//         for(int i = 1; i <= n; i ++) puts(a[i]);
//         puts("");
//         return ;
//     }

//     for(int i = 1; i <= n; i ++){
//         if(!col[i] && !dg[u + i] && !udg[n + u - i - 1]){
//             a[u][i] = 'Q';
//             col[i] = dg[u + i] = udg[n + u - i - 1] = true;
//             dfs(u + 1);
//             col[i] = dg[u + i] = udg[n + u - i - 1] = false;
//             a[u][i] = '.';
//         }
//     }   
// }

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++)
//         for(int j = 1; j <= n; j ++)
//             a[i][j] = '.';

//     dfs(1);

//     return 0;
// }

// using namespace std;

// const int N = 110;

// int n;
// int path[N];
// bool st[N];

// void dfs(int u){
//     if(u == n){
//         for(int i = 0; i < n; i ++) printf("%d ", path[i]);
//         puts("");
//         return ;
//     }

//     for(int i = 1; i <= n; i ++){
//         if(!st[i]){
//             st[i] = 1;
//             path[u] = i;
//             dfs(u + 1);
//             st[i] = 0;
//         }
//     }
// }

// int main(){
//     cin >> n;

//     dfs(0);
//     return 0;
// }

// using namespace std;
// const int maxn=50010;
// int pre[maxn];
// int n,len,mx=-1;
// int first[7],last[7];//%7的余数为0~6,所以开7的数组就可以了； 
// int main(){
// 	cin>>n;
// 	for(int i=1;i<=n;i++)
// 	{
// 		cin>>pre[i];
// 		pre[i]=(pre[i]+pre[i-1])%7; //%7意义下的前缀和 ，就变成了前缀和%7的余数；		
// 	}
// 	for(int i=n;i>=1;i--)//(pre[i]是当位置为i时 前缀和%7的余数） 
// 		first[pre[i]]=i;   //第一次出现pre[i]这个余数的时候的位置为i ；
// 	//倒着扫一遍 不断更新最后就是这个余数第一次出现； 
// 	first[0]=0;//从头加到i是7的倍数的情况下，需要把0的第一次出现设为0，即把整个区间[1,i]选上了。
// 	for(int i=1;i<=n;i++)
// 		last[pre[i]]=i;//最后一次出现pre[i]这个余数时位置为i； 
		
// 	for(int i=0;i<=6;i++)   //这里是看哪个余数的长度最大，last[i]-first[i]就是余数i的最大长度
// 	//(最后一次出现减第一次出现 显然是最长的)
// 	//两个位置相减就是长度；因为是前缀和(前缀和为【i+1，j】的区间,所以j-i即为区间的长度） 
// 	//这里不是一般的 j-i+1(末位置 减 首位置+1） 为长度，紧扣前缀和的定义！！ 
// 	mx=max(last[i]-first[i],mx);
// 	//前缀和求原区间长度是 j-i；一般的区间长度是 j-i+1； 
// 	cout<<mx<<endl;
// } 

// using namespace std;

// const int N = 5e5 + 10;
// const int M = 1e8 + 10;

// int n;
// int a[N];
// long long  b[M];
// long long  mx = 0;
// long long  num = 0;

// int main(){
//     cin >> n;
    
//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++) b[i] = b[i - 1] + a[i];

//     // for(int i = 1; i < n; i ++){
//     //     for(int j = i + 1; j <= n; j ++){
//     //         if((b[j] % 7 - b[i] % 7) % 7 == 0) num = j - i;
//     //         mx = max(mx, num);
//     //     }
//     // }

//     for(int i = 1; i <= n; i ++){
//         for(int j = n; j >= i; j --){
//             if(i == j){ 
//                 cout << "0";
//                 return 0;
//             }
//             else if(b[j] % 7 == b[i] % 7){ 
//                 cout << j - i;
//                 return 0; 
//             }
//         }
//     }

//     return 0;
// }
// using namespace std;

// vector<int> add(vector<int> A, vector<int> B){
//     int t = 0;
//     vector<int> C;

//     for(int i = 0; i < A.size() || i < B.size(); i ++){
//         if(i < A.size())t += A[i];
//         if(i < B.size())t += B[i];
//         C.push_back(t % 10);
//         t /= 10;
//     }

//     if(t) C.push_back(t);
//     return C;
// }

// int main(){
//     string a, b;
//     cin >> a >> b;
//     vector<int> A, B;

//     for(int i = a.size() - 1; i >= 0; i --) A.push_back(a[i] - '0');
//     for(int i = b.size() - 1; i >= 0; i --) B.push_back(b[i] - '0');

//     auto C = add(A, B);

//     for(int i = C.size() - 1; i >= 0; i --) printf("%d", C[i]);
//     return 0;
// }
// using namespace std;

// typedef long long ll;

// ll a, b, p;

// int ans = 1;

// ll qmi(ll a, ll b, ll p){
//     ll res = 1;
//     while(b){
//         if(b & 1) res = res * a % p;
//         b >>= 1;
//         a = a * a % p;//时时刻刻代表相应位置的下一个位置的二进制所表达的数字-----第一次后更新为a^2  2 —— a^4 3 -- a^8 都可以写成a^{2^相应位号}
//     }

//     return res;
// }

// int main(){
//     cin >> a >> b >> p;

//     printf("%d^%d mod %d=%d", a, b, p, qmi(a, b, p));
//     // printf("%lld", qmi(a, b, p));
//     return 0;
// }
// using namespace std;

// typedef long long ll;

// double a, b, c, d;
// double x, y;
// int sum = 0;
// int num = 0;
// double j;
// void find(double a, double b, double c, double d, double i, double j){
//     double l = i, r = j;

//     while(r - l >= 1e-3){
//         double m = (l + r) / 2;

//         if(a * m * m * m + b * m * m + c * m + d >= 0) r = m ;
        
//         else if(a * m * m * m + b * m * m + c * m + d <= 0) l = m ;
//     }

//     printf("%.2lf ", l);
// }

// int main(){
//     cin >> a >> b >> c >> d;

 
//     for(double i = -100; i <= 3 ; i  = j + 1){
//         x = a * i * i * i + b * i * i + c * i + d;
//         if(i == 1){
//             cout << a * i * i * i << endl;
//             cout << b * i * i << endl;
//             cout << c * i << endl;
//             cout << d << endl;
//             cout << a * i * i * i + b * i * i + c * i << endl;
//         }
//         if(x == 0){
//             printf("%d.00 ", i);
//             sum ++;
//             if(sum == 3) return 0;
//             continue;
//         }
//         for(j = i + 1; j <= 99; j ++){
//             y = a * j * j * j + b * j * j + c * j + d;
//             if(y == 0){
//                 printf("%d.00 ", j);
//                 sum ++;
//                 if(sum == 3) return 0;
//                 break;
//             }
//             if(x * y < 0){
//                 find(a, b, c, d, x, y);
//                 sum ++;
//                 if(sum == 3) return 0;
//                 break;
//             }
//         }
//     }

//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int x[N];
// string b[N];
// int a, s;
// // string left[N], right[N];
// int num = 1;

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> x[i] >> b[i];

//     for(int i = 1; i <= m; i ++){
//         cin >> a;
//         if(a == 0){
//             cin >> s;

//             if(x[num] == 0){
//                 if((n - s + num) % n == 0) num = n;
//                 else num = (n - s + num) % n;
//             }
//             else {
//                 if((num + s) % n == 0) num = n;
//                 else num = (num + s) % n;
//             }

//         }
//         else if(a == 1){
//             cin >> s;
            
//            if(x[num] == 0){
//                 if((num + s) % n == 0) num = n;
//                 else num = (num + s) % n;
//             }
//             else {
//                 if((n - s + num) % n == 0) num = n;
//                 else num = (n - s + num) % n;
//             }

//         }

       
//     }

//     cout << b[num] << endl;
    
//     return 0;
// }

// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;
// ll a[N];

// void quick_sort(ll a[], int l, int r){
//     if(l >= r) return ;

//     int i = l - 1, j = r + 1, x = a[l + r >> 1];

//     while(i < j){
//         do i ++; while(a[i] < x);
//         do j --; while(a[j] > x);
//         if(i < j) swap(a[i], a[j]);
//     }

//     quick_sort(a, l, j);
//     quick_sort(a, j + 1, r);

// }

// int main(){
//     cin >> n;

//     for(int i = 0; i < n; i ++) cin >> a[i];

//     quick_sort(a, 0, n - 1);

//     for(int i = 0; i < n; i ++) cout << a[i] << " ";

//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int a[N];
// int n;

// void quick_sort(int q[], int l, int r){
//     if(l >= r) return ;

//     int i = l - 1, j = r + 1, x = a[l + r >> 1];

//     while(i < j){
//         do i ++; while(a[i] < x);
//         do j --; while(a[j] > x);
//         if(i < j) swap(a[i], a[j]); 
//     }

//     quick_sort(a, l, j);    
//     quick_sort(a, j + 1, r);
// }

// int main(){
//     cin >> n;
//     for(int i = 0; i < n ; i ++) cin >> a[i];

//     quick_sort(a, 0, n - 1);

//     for(int i = 0; i < n; i ++) cout << a[i] << " ";
//     return 0;
// }
// using namespace std;

// int n;
// const int N = 1e6 + 10;
// int q[N];

// void quick_sort(int q[], int l, int r){

//     if(l >= r) return ;

//     int x = q[l + r >> 1], i = l - 1, j = r + 1;
//     while(i < j){
//         do i ++; while(q[i] < x);
//         do j --; while(q[j] > x);
//         if(i < j) swap(q[i], q[j]);
//     }

//     quick_sort(q, l, j);
    
//     quick_sort(q, j + 1, r);

// }

// int main(){
//     cin >> n;
//     for(int i = 0; i < n; i ++) cin >> q[i];

//     quick_sort(q, 0, n - 1);
    
//     for(int i = 0; i < n; i ++) printf("%d ", q[i]);

//     return 0;
// }

// using namespace std;

// const int N = 203;
// int n;
// int na, nb;
// int a[N], b[N];
// int sa = 0, sb = 0;

// int main(){
//     cin >> n;
//     cin >> na >> nb;

//     for(int i = 1; i <= na; i ++){
//         cin >> a[i];
//         if(a[i] == 0) a[i] = 1;
//         else if(a[i] == 1) a[i] = 5;
//         else if(a[i] == 2) a[i] = 3;
//         else if(a[i] == 3) a[i] = 2;
//         else a[i] = 4;
//     }

//     for(int j = 1; j <= nb; j ++){
//         cin >> b[j];
//         if(b[j] == 0) b[j] = 1;
//         else if(b[j] == 1) b[j] = 5;
//         else if(b[j] == 2) b[j] = 3;
//         else if(b[j] == 3) b[j] = 2;
//         else b[j] = 4;
//     }
 
//     for(int i = 1; i <= n; i ++){
//         if(i % na == 0){
//             a[i] = a[na];
//         }else{
//             a[i] = a[i % na];
//         }
//         if(i % nb == 0){
//             b[i] = b[nb];
//         }else{
//             b[i] = b[i % nb];
//         }
//         if(a[i] == b[i]) continue;
//         else if(((a[i] % 5) + 1) == b[i] || ((a[i] % 5) + 2) == b[i] || (((a[i] % 5) + 2) % 5) == b[i]) sa ++;
//         else sb ++;

//     }

//     cout << sa << " " << sb;

//     return 0;
// }

// #include<bits/stdc++.h>
// using namespace std;
// int main(){
//     int n,a;
//     cin>>n;
//     for(int i=n;i>=0;i--){
//         cin>>a;
//         if(a){   
//             if(i!=n&&a>0)cout<<"+";    
//             if(abs(a)>1||i==0)cout<<a;    
//             if(a==-1&&i)cout<<"-";    
//             if(i>1)cout<<"x^"<<i;    
//             if(i==1)cout<<"x";    
//         }
//     }
// }
// using namespace std;

// const int N = 1e3 + 10;

// int n;
// int a[N];

// int main(){
//     cin >> n;

//     for(int i = 0; i <= n; i ++) cin >> a[i];

//     if(n == 0){
//         if(a[0] > 0) printf("%d", abs(a[0]));
//         else if(a[0] < 0) printf("-%d", abs(a[0])); 
//         return 0; 
//     }
//     //开头如果为正则不输出符号S
//     for(int i = 0; i <= 0; i ++){
//         if(a[i] > 0){
//             if(a[i] != 1) printf("%dx^%d", abs(a[i]), n - i);
//             else printf("x^%d", n - i);
//             break;
//         }    
//         else if(a[i] < 0){
//             if(a[i] != -1) printf("-%dx^%d", abs(a[i]), n - i);
//             else printf("-x^%d", n - i);
//             break;
//         }
//     }
//     //系数为1或-1时只输出符号不输出系数
//     for(int i = 1; i <= n; i ++){
//         if(a[i] > 0 && i != n){
//             if(a[i] == 1){
//                 if(n - i == 1) printf("+x");
//                 else printf("+x^%d", n - i);
//             }
//             else {
//                 if(n - i == 1) printf("+%dx", abs(a[i]));
//                 else printf("+%dx^%d", abs(a[i]), n - i);
//             }
//         }
//         else if(a[i] < 0 && i != n){
//             if(a[i] == -1){
//                 if(n - i == 1) printf("-x");
//                 else printf("-x^%d", n - i);
//             }
//             else {
//                 if(n - i == 1) printf("-%dx", abs(a[i]));
//                 else printf("-%dx^%d", abs(a[i]), n - i);
//             }
//         }
//         else if(a[i] == 0) continue;
//         else if(i == n){
//             if(a[i] > 0) printf("+%d", abs(a[i]));
//             else if(a[i] < 0) printf("-%d", abs(a[i]));
//         }
//     }

//     return 0;
// }

// using namespace std;

// const int N = 1e4 + 10;

// int a, b, g ,k;
// int n;
// // int d[N][N];
// int x, y;
// int aa[N], bb[N], gg[N], kk[N];
// int num = 0;
// int sum = 0;

// int main(){
//     cin >> n;

//     while(n --){
//         cin >> a >> b >> g >> k;    
//         aa[++ num] = a;
//         bb[num] = b;
//         gg[num] = g;
//         kk[num] = k;
//     }

//     cin >> x >> y;

//     for(int i = 1; i <= num; i ++){
//         if(x >= aa[i] && x <= (gg[i] + aa[i]) && y >= bb[i] && y <= (kk[i] + bb[i])) sum = i;
//     }

//     if(!sum) cout << "-1";
//     else cout << sum;

//     return 0;
// }
// using namespace std;

// const int N = 25;

// int n, m, x, y;
// int dp[N][N];//到点（i， j）总共有dp[i][j]条路线
// int a[N][N];
// int nx[8] = {-1, -2, -2, -1, 1, 2, 2, 1};
// int ny[8] = {-2, -1, 1, 2, 2, 1, -1, -2};

// int main(){
//     cin >> n >> m >> x >> y;  
    
//     a[x][y] = 1;

//     dp[0][0] = 1;
//     for(int i = 0; i < 8; i ++){
//         a[x + nx[i]][y + ny[i]] = 1;
//     }

//     for(int i = 1; i <= n; i ++){
//         if(dp[i - 1][0] == 0){
//             dp[i][0] = 0;
//             continue;
//         }
//         if(a[i][0] == 1) dp[i][0] = 0;
//         else dp[i][0] = 1;
//     }

//     for(int j = 1; j <= m; j ++){
//         if(dp[0][j - 1] == 0){
//             dp[0][j] = 0;
//             continue;
//         }
//         if(a[0][j] == 1) dp[0][j] = 0;
//         else dp[0][j] = 1;
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j ++){
//             if(a[i][j] != 1) dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//             else dp[i][j] = 0;
//         }
//     }

//     cout << dp[n][m];
    
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int prime[N];
// int sum = 0;
// int num = 0;

// int find(int u){
//     int x = 1;

//     for(int i = 4; i <= u; i ++){
//         num = 0;
//         for(int j = 2; j <= i / j; j ++){
//             if(i % j == 0){
//                 num = 1;
//                 break;                
//             }
//         }
//         if(num == 0){
//             sum += i;
//             if(sum > u - 5) break;
//             else prime[++ x] = i;
//         }
//     }
    
//     return x;
// }

// int main(){
//     cin >> n;
    
//     if(n <= 1){
//         cout << 0;
//         return 0;
//     }
//     prime[0] = 2;
//     prime[1] = 3;

//     int end = find(n);

//     for(int i = 0; i <= end; i ++)cout << prime[i] << endl;

//     cout << end + 1;
//     return 0;
// }
// using namespace std;

// const int N = 26;

// int a[N];
// string T;
// int ST;

// int main(){
//     cin >> T;
//     cin >> ST;

//     for(int i = 0; i < T.length(); i ++) a[i] = ST + T[i] - 'A';

//     for(int i = )
//     // }
//     // for(int i = 0; i < T.length(); i ++){
//     //     while(a[i]){
//     //         sum += a % 10;
//     //         a[i] /= 10; 
//     //     }

//     //     a[i] = sum;
//     //     sum = 0;
//     // }

//     // for(int i = 0; i < T.length(); i ++){

//     // }

//     return 0;
// }
// using namespace std;

// typedef long long ll;

// const int N = 1e5 + 10;

// int n;
// ll a[N];
// int T;
// ll b[N];

// int main(){
//     cin >> T;

//     while(T --){
//         cin >> n;

//         for(int i = 1; i <= n; i ++){
//             cin >> a[i];
//             b[a[i]] ++;
//             if(b[a[i]] >= 2){
//                 a[i] = 0;
//             }
//         }

//         for(int i = 1; i <= n; i ++){
//             b[a[i]] = 0;
//             if(a[i] != 0){
//                 cout << a[i];
//                 if(i != n) cout << " ";
//             }
//         }

//         cout << endl;
//     }

//     return 0;
// }
// #include <bits/stdc++.h>
// using namespace std;

// int ans=0, n, k, a[20];

// bool prime(int x) {
// 	int i;
// 	for (i = 2; i <= floor(sqrt(x)); i++) {
// 		if (x%i == 0) {
// 			return false;
// 		}
// 	}
// 	return true;
// }

// //上面判素数不解释
// void rec(int start, int count, int sum) {
// 	//rec：递归英文recursion缩写
//     //start：开始选数的地方
//     //count：已经选了几个数
//     //sum：到目前为止选的数的和
	
//     int i;
// 	if (count == k && prime(sum)) {
// 		ans++;
//     	//如果已经选了k个数，判断和是不是素数
//         //ans是符合条件的和的个数
// 	}
	
// 	for (i = start; i <= n; i++) {
//     	//从开始选数的地方到n
//         //每重循环都是一种可能性
//         //例如：选第二个数，start=2,n=4,有三种可能性，调用自身3次
		
//         rec(i + 1, count + 1, sum + a[i]);
		
//         //参数1：从a里面下一个数开始选
//         //参数2：已经选的数的个数+1
//         //参数3：sum加上这次选的数
        
//         //因为直接从下一个数开始选，所以不可能选到之前选过的数
//         //无需判断当前的数是否被选过
// 	}
// }	

// int main() {
	
// 	int i;
// 	cin >> n >> k;
// 	for (i = 1; i <= n; i++) {
// 		cin >> a[i];
// 	}
// 	rec(1, 0, 0);
//     //从第一个数开始找，已经找了0个数，目前的和是0
// 	cout << ans << endl;
// 	return 0;
// }

// using namespace std;

// const int N = 30;

// const int M  = 1e8 + 10;

// int c[M];
// int a[N];
// int n, k;
// bool b[N];
// long long sum = 0;//数字之和
// int num = 0;//种类之和


// bool isprime(long long x){
//     for(int i = 2; i <= x / i; i ++){
//         if(x % i == 0) return false;
//     }
//     return true;
// }
// // int prime[M];
// // bool d[M];
// // int cnt = -1;

// // void merge(int x){
// //     for(int i = 2; i <= x; i ++){
// //         if(!d[i]) prime[++ cnt] = i;
// //         for(int j = 0; prime[j] <= x / i; j ++){
// //             d[prime[j] * i] = 1;
// //             if(i % prime[j] == 0) break;
// //         }
// //     }
// // }

// void dfs(int u, int start, int sum){
//     if(u == k && isprime(sum)){
//         // c[sum] ++;
//         // cout << sum << endl;
//         // if(c[sum] == 1 && isprime(sum))
//         num ++;
//     }
//     for(int i = start; i <= n; i ++){
//         dfs(u + 1, start + 1, sum + a[i]);
//     }
// }

// int main(){
//     cin >> n >> k;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     // merge(M);
//     dfs(0, 1, 0);

//     cout << num;
//     return 0;
// }
// using namespace std;

// int main(){
//     string a, b, c = "";
//     int sum, k = -1;

//     getline(cin, a);
//     getline(cin, b);
//     b += ' ';

//     int len = a.length();
//     int len1 = b.length();
//     for(int i = 0; i < len; i ++){
//         if(a[i] >= 'a') a[i] -= 32;
//     }
//     for(int i = 0; i < len1; i ++){
//         if(b[i] >= 'a') b[i] -= 32;
//     }
//     for(int i = 0; i < len1; i ++){
//         if(b[i] == ' '){
//             if(c == a){
//                 sum ++;
//                 if(k == -1) k = i - a.length();
//             }

//             c = "";
//         }

//         else c += b[i];
//     }

//     if(k == -1) cout << k;
//     else cout << sum << " " << k;
//     return 0;
// }

// using namespace std;

// const int N = 1e6 + 10;

// int n;

// bool a[N];
// int prime[N];
// int j = -1;

// void merge(int x){
//     for(int i = 2; i <= x; i ++){
//         if(!a[i]) prime[++ j] = i;
//         for(int k = 0; prime[k] <= x / i; k ++){
//             a[prime[k] * i] = 1;
//             if(i % prime[k] == 0) break;
//         }
//     }
// }

// int main(){
//     cin >> n;

//     merge(n);

//     cout << j + 1;
//     return 0;
// }
// using namespace std;

// int main()
// {
// 	string a,b,c="";
// 	int len,len1,s=0,k=-1;
// 	getline(cin,b);
// 	getline(cin,a);
// 	a+=' ';
// 	len=a.length();
// 	len1=b.length();
// 	for(int i=0;i<len;i++)
// 		if(a[i]>='A'&&a[i]<='Z') a[i]+=32;
// 	for(int i=0;i<len1;i++)
// 		if(b[i]>='A'&&b[i]<='Z') b[i]+=32;
// 	for(int i=0;i<len;i++)
// 	{
// 		if(a[i]==' ')
// 		{
// 			if(c==b)
// 			{
// 				s++;
// 				if(k==-1) k=i-b.length();
// 			}
// 			c="";
// 		}
// 		else c+=a[i];
// 	}
// 	if(k==-1) cout<<k;
// 	else cout<<s<<' '<<k;
// 	return 0;
// }

// using namespace std;

// const int N = 1e6 + 10;

// int a[N], b[N];
// int bg[N];

// char ch1, ch2;

// int main(){
//     int i = 0;

//     while(1){
//         cin >> ch1;
//         if(ch1 == '\n') break;
//         else a[i ++] = ch1;
//     }
    
//     cout << 1 << endl;
//     int j = 0;

//     while(1){
//         cin >> ch2;
//         if(ch2 == '\n') break;
//         else b[j ++] = ch2;
//     }
    
//     int num = 0;
//     int sum = 0;
//     int x = 0;

//     for(int k = 0; k <= j - 1; k ++){
//         num = 0;
//         for(int l = 0;l <= i - 1; l ++){
//             if(b[k] != a[l] && b[k] != a[l] - 32 && b[k] != a[l] + 32){
//                 num = 1;
//                 break;
//             }
//         }

//         if(num == 0){
//             sum ++;
//             bg[x ++] = k;
//         }
//     }

//     if(sum == 0) cout << "-1";
//     else{
//         cout << sum << " " << bg[0];
//     }
//     return 0;
// }
// using namespace std;

// int a[11] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
// int n;

// // void dfs(int n){

// // }

// int sum = 0;

// int main(){
//     cin >> n;
//     n -= 4;

//     for(int i = 0; i <= 9; i ++){
//         for(int j = 0; j <= 9; j ++){
//             for(int k = 0; k <= 9; k ++){
//                 if((i + j == k) && (a[i] + a[j] + a[k] == n)) sum ++;
//             }
//         }
//     }

//     cout << sum;
//     // dfs(n);
//     return 0;
// }
// using namespace std;

// const int N = 20;

// char a[N][N];
// int n;
// double x0, y0;
// double x1, y1, x2, y2;
// int cum;

// int main(){
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= n; j ++){
//             cin >> a[i][j];
//         }
//     }

//     for(int j = 1; j <= n; j ++){
//         if(a[1][j] == '1') x0 = i * 1.0, break;
//     }

//     for(int j = 1; j <= n; j ++){
//         if(a[j][1] == '1') y0 = j * 1.0, break;
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= n; j ++){
//             if(a[i][j] == 'x'){
//                 if(x1 == 0 && y1 ==0){
//                     x1 = i;
//                     y1 = j;
//                 }else{
//                     x2 = i;
//                     y2 = j;
//                     cum = 1;
//                 }
//                 break;
//             }
//         }
//         if(cum == 1) break;
//     }

//     if((y2 > y1 && x2 > x1) || (y2 < y1 && x2 < x1)){
//         double k = (y2 - y1) * 1.0 / (x2 - x1) * 1.0; 
//         if((y2 - k * x2) == 0) printf("y = -%.4f", k);
//         else printf("y = %.4fx + %.4f", k, y2 - k * x2);
//     }

//     else if(y2 == y1){
//         if((y2 - y0) > 0)
//         printf("y = -%.4lf", y2 - y0);
//         else if((y2 - y0) < 0)
//         printf("y = %.4lf", y2 - y0);
//         else 
//         printf("y = %.4lf", y2 - y0);
//     }

//     else if(x2 == x1){
//         if((x2 - x0) > 0)
//         printf("x = %.4lf", x2 - x0);
//         else if((x2 - x0) < 0)
//         printf("x = -%.4lf", x2 - x0);
//         else
//         printf("x = %.4lf", x2 - x0);
//     }

//     else {
//         double k = (y2 - y1) * 1.0 / (x2 - x1) * 1.0; 
//         if((y2 - k * x2) == 0) printf("y = -%.4lf", k);
//         else printf("y = -%.4lfx + %.4lf", k, y2 - k * x2);
//     }

//     return 0;
// }

// using namespace std;

// typedef long long ll;

// ll q[40][40][40];

// ll w(ll a, ll b, ll c){
//     if(a <= 0 || b <= 0 || c <= 0){
//         return 1;
//     }
//     if(a > 20 || b > 20 || c > 20){
//         q[20][20][20] = w(20, 20, 20);
//         return q[20][20][20];
//     }

//     if(q[a][b][c] == 0){
//         if(a < b && b < c){
//             q[a][b][c] = w(a, b, c - 1) + w(a, b - 1, c - 1) - w(a, b - 1, c);
//             return q[a][b][c];
//         }

//         else{
//             q[a][b][c] = w(a - 1, b, c) + w(a - 1, b - 1, c) + w(a - 1, b, c - 1) - w(a - 1, b - 1, c - 1);
//             return q[a][b][c];
//         }  
//     }

//     return q[a][b][c];
// }

// int main(){
//     ll a, b ,c;
//     while(1){
//         cin >> a >> b >> c;
//         if(a == -1 && b == -1 && c == -1) break;
//         printf("w(%lld, %lld, %lld) = %lld\n", a, b, c, w(a, b, c));
//     }
//     return 0;
// }

// #include<iostream>
// #include<cstdio>
// using namespace std;
// long long q[25][25][25];//定义数组,后面用来优化。 
// long long w(long long a,long long b,long long c)//这里要用long long,不然会报错。 
// {
// 	if(a<=0 or b<=0 or c<=0)return 1;
// 	else if(a>20 or b>20 or c>20)
// 	{
// 		q[20][20][20]=w(20,20,20);//将结果存入数组后再返回。 
// 		return q[20][20][20];
// 	}
// 	if(q[a][b][c]==0) 
// 	{
// 		if(a<b&&b<c)
// 		{
// 			q[a][b][c]=w(a,b,c-1)+w(a,b-1,c-1)-w(a,b-1,c);//将结果存入数组后再返回。 
// 			return q[a][b][c];
// 		}
// 		else
// 		{
// 			q[a][b][c]=w(a-1,b,c)+w(a-1,b-1,c)+w(a-1,b,c-1)-w(a-1,b-1,c-1);//将结果存入数组后再返回。 
// 			return q[a][b][c];
// 		}
// 	}
// 	else return q[a][b][c];
// }
// int main()
// {
// 	long long a,b,c;
// 	while(scanf("%lld%lld%lld",&a,&b,&c)==3)
//     {
//         if(a==-1&&b==-1&&c==-1)break;
//         printf("w(%lld, %lld, %lld) = %lld\n",a,b,c,w(a,b,c));
//     }
// 	return 0;
// }	
// using namespace std;

// int a, b, c;

// int find(int a, int b, int c){
//     if(a <= 0 || b <= 0 || c <= 0) return 1;

//     if(a > 20 || b > 20 || c > 20) return find(20, 20 ,20);

//     if(a < b && b < c) return find(a, b, c - 1) + find(a, b - 1, c - 1) - find(a, b - 1, c);

//     return find(a - 1, b, c) + find(a - 1, b - 1, c) + find(a - 1, b, c - 1) - find(a - 1, b - 1, c - 1);
// }

// int main(){
//     while(a != -1 && b != -1 && c != -1){
//         cin >> a >> b >> c;   
//         int x = find(a, b, c);

//         printf("w(%d, %d, %d) = %d\n", a, b, c, x);
//     }
    
//     return 0;
// }
// using namespace std;

// int n,f[505][505];

// int main(){
// 	scanf("%d",&n);
// 	f[1][1]=1;
// 	for (int i=2;i<=n+n;i++)
// 		for (int j=(i+1)>>1;j<=i;j++)
// 			f[i][j]=(f[i-1][j]+f[i-1][j-1])%100;
// 	printf("%d",f[n+n][n]);
// 	return 0;
// }

// using namespace std;

// int n;
// int sum = 1;

// int main(){
//     cin >> n;

//     for(int i = n; i <= 2 * n; i ++) sum = sum * i % 100;

//     cout << 2 * sum;
//     return 0;
// }

// using namespace std;

// const int N = 2e4 + 10;

// int n, m;
// int a[N];
// int b[N];

// bool cmp(int x, int y){
//     return x > y;
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     int num = a[n] - a[1] + 1; 

//     for(int i = 1; i < n; i ++)b[i - 1] = a[i + 1] - a[i];

//     sort(b, b + n - 1, cmp);

//     for(int i = 0; i < m - 1; i ++) num = num - b[i] + 1;

//     cout << num;

//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// int a[N];

// int main(){
//     cin >> n >> m;
    
//     for(int i = 1; i <= n; i ++) a[i] = 1;

//     int b = 1;

//     for(int i = 1; i <= m; i ++){
//         if((b + 1 + (i + 1) * (i + 1) * (i + 1) % 5 + 1) >= n) b = 1;
//         cout << "b = " << b << endl;
//         int num = (i * i * i % 5 + 1);
//         a[num + b] = 0;
//         cout << "num + 1 + b = " << num + b << endl;
//         b =  num + b + 1;
//     }

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int a[N];
// int sum;

// int find(int i){
//     if(a[i] == i) return i;
//     return a[i] = find(a[i]);
// }

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) a[i] = i;

//     while(m --){
//         int p, q;
//         cin >> p >> q;
//         a[find(q)] = find(p);
//     }

//     for(int i = 1; i <= n; i ++){
//         if(a[i] == i)
//             sum ++; 
//     }
//     cout << sum;
//     return 0;
// }
// using namespace std;

// int x;//>X的牌
// int y;//<=X的牌
// int sumx;//>X的牌的数量
// int sumy;//<=X的牌的数量
// // if(sum x >= sumy)cout << DOSTA;
// int n;
// int sum;
// int a[60];
// int sm[14];
// int num;

// int main(){
//     cin >> n;

//     for(int i = 2; i <= 9; i ++) sm[i] = 4;
//     sm[10] = 16;
//     sm[11] = 4;

//     while(n --){
//         int i;
//         cin >> i;
//         sm[i] --;
//         sum += i;
//     }

//     if(sum > 21){
//         cout << "DOSTA";
//         return 0;
//     }
//     else{
//         num = 21 - sum;
//         for(int i = 2; i <= 11; i ++){
//             if(i > num) sumx += sm[i];
//             else sumy += sm[i];
//         }

//         if(sumx >= sumy){
//             cout << "DOSTA";
//             return 0;
//         }
//     }

//     cout << "VUCI";
//     return 0;
// }
// using namespace std;

// int main(){
//     int sum =  0;
//     for(int i = 100000000; i <= 999999999; i ++){
//         if(i * i % 1000000000 == 987654321){
//             sum ++;
//             cout << i << endl;
//             cout << 111111111 * 111111111 % 1000000000 << endl;
//         }
//     }

//     cout << sum;
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int dp[N];//dp[i]代表到数字n满足条件的数列的个数为dp[i]
// int sum;
// int m[N];

// int main(){
//     cin >> n;

//     m[1] = dp[1] = 1;
//     m[2] = dp[2] = 2;
    
//     for(int i = 3; i <= n / 2; i ++){
//         for(int j = 1; j <= i / 2; j ++){
//             m[i] += dp[j];
//             cout << dp[j] << "111" <<  endl;
//         }
//         m[i] ++;
//         cout << m[i] << endl;
//     }

//     for(int i = 1; i <= n / 2; i ++) {
//         sum += m[i];
//     }

//     cout << sum + 1;
//     return 0;
// }
// using namespace std;

// int n;
// int sum = 1;
// int m;

// void dfs(int u){
//     if(!u)return ;

//     if(u % 2 == 0) m = u / 2;
//     else m = (u - 1) / 2;
//     for(int i = m; i >= 1; i --){
//         sum ++;
//         dfs(i);
//     }

//     return ;
// }

// int main(){
//     cin >> n;
//     // if(n % 2 == 0)cout << n;
//     // else cout << n - 1;
//     dfs(n);

//     cout << sum;
//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int n, dist;
// int a[N];
// int sum = 0;

// int main(){
//     cin >> n >> dist;
//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     sort(a + 1, a + n + 1);

//     for(int i = 2; i <= n - 1; i ++){
//         if(a[i + 1] - a[i - 1] <= dist){
//             sum ++;
//             a[i] = a[i - 1];
//         }
//     }

//     cout << sum;

//     return 0;
// }
// #include <cmath>

// using namespace std;

// double x, y;
// double vx, vy;
// double xt, yt;
// double t;
// int main(){
//     cin >> x >> y;

//     vx = sin(y) * x;
//     vy = cos(y) * x;
//     t = vy / 10;
//     xt = vx * t;
//     yt = 0.5 * 10 * t * t;
//     // yt = (x * x * cos(y) * cos(y))/ (2 * 10 * sin(y) * sin(y)) ;
//     // cout << yt << endl;
//     // t = sqrt(2 * yt / 10);
//     // cout << t << endl;
//     // xt = 0.5 * yt * t;
    
//     printf("%.3lf %.3lf", xt, yt);

//     return 0;
// }
// #include <cmath>
// #include <math.h>
// using namespace std;

// const int N = 1e3 + 10;

// int n;
// double r;
// double a[N], b[N];
// double sum = 0.0;

// int main(){
//     cin >> n >> r;

//     for(int i = 1; i <= n; i ++){
//         cin >> a[i];
//         cin >> b[i];
//     }

//     a[n + 1] = a[1];
//     b[n + 1] = b[1];
    

//     for(int i = 1; i <= n; i ++){
//         sum += sqrt(pow(abs(a[i + 1] - a[i]), 2) + pow(abs(b[i + 1] - b[i]), 2));
//     }

//     sum += 2 * acos(-1) * r;

//     printf("%.2lf", sum);
//     return 0;
// }
// using namespace std;

// int main(){
//     int a, h;
//     cin >> a >> h;
    
//     if((a * h) % 2 != 0)printf("%d.5", a * h);
//     else printf("%.0f", 0.5 * a * h);
//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int n, m;
// int a[N];

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> a[is];

//     while(m --){
//         int l1, r1, l2, r2;
//         cin >> l1 >> r1 >> l2 >> r2;

//         for(int i = l2; i <= r2; i ++){
//             int sum = 0;
//             for(int j = l1; j <= r1; j ++){
//                 if(a[i] >= a[j])sum ++;
//             }

//             cout << sum << " ";
//         }

//         cout << endl;

//     }
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int a[N];
// int dp[N];
// int res = 0;

// int main(){
//     cin >> n;
//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++){
//         dp[i] = max(dp[i - 1] + a[i], a[i]);
//         res = max(res, dp[i]);
//     }

//     cout << res ;

//     return 0;
// }
// using namespace std;

// const int N = 1e6 + 10;

// int n, k;
// int a[N], q[N];
// int hh, tt = -1;

// int main(){
//     cin >> n >> k;
//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) ++ hh;
//         while(hh <= tt) && a[i] <= a[q[tt]] -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n;
// int a[N];
// long long sum[N];
// int mx = 0;
// int mi = 0x3f3f3f3f;
// int up, down;

// int main(){
//     cin >> n;
//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     for(int i = 1; i <= n; i ++){
//         sum[i] = sum[i - 1] + a[i];
//         if(sum[i] >= mx) mx = sum[i], up = i;
//         if(sum[i] <= mi) mi = sum[i], down = i;
//     }

//     if(up > down) cout << mx - mi;
//     else cout << mx;
    
//     return 0;
// }
// using namespace std;

// int n;

// void qmi(int a, int b){

// }

// int main(){
//     cin >> n;

//     while(n --){
//         int a, b;
//         cin >> a >> b;
//         qmi(a, b);
//     }
//     return 0;
// }
// using namespace std;

// const int N = 1e6 + 10;

// int a[N];
// int n;

// int main(){
//     cin >> n;

//     for(int i = 0; i < n; i ++)cin >> a[i];
//     return 0;
// }
// #include <iostream>
// #include <vector>

// using namespace std;

// // 计算两条直线相交得到一个交点（增加区域数）
// int intersect(vector<int>& a, vector<int>& b, vector<int>& c, vector<int>& d) {
//     int cnt = 0;
//     for (int i = 0; i < a.size(); ++i) {
//         for (int j = i + 1; j < a.size(); ++j) {
//             // 计算并判断两条直线是否相交
//             // 这里简化为两条线的斜率乘积是否不等于-1（实际应用中需要更精确的判断方法）
//             if ((d[i] - b[i]) * (c[j] - a[j]) != (d[j] - b[j]) * (c[i] - a[i])) {
//                 ++cnt;
//             }
//         }
//     }
//     return cnt;
// }

// int main() {
//     int n;
//     cin >> n;

//     vector<int> a(n), b(n), c(n), d(n);
//     for (int i = 0; i < n; ++i) {
//         cin >> a[i];
//     }
//     for (int i = 0; i < n; ++i) {
//         cin >> b[i];
//     }
//     for (int i = 0; i < n; ++i) {
//         cin >> c[i];
//     }
//     for (int i = 0; i < n; ++i) {
//         cin >> d[i];
//     }

//     // 每多一条直线，会增加n条已存在的直线的交点，即增加n-1个新的区域
//     int regions = (n + 1) - intersect(a, b, c, d);

//     cout << regions << endl;

//     return 0;
// }
// using namespace std;

// const int N = 60;

// int a[N], b[N], c[N], d[N];
// double x[N];
// int n;;

// int main(){
//     int cnt = 0;
//     cin >> n;

//     for(int i = 1; i <= n; i ++)cin >> a[i];

//     for(int i = 1; i <= n; i ++)cin >> b[i];

//     for(int i = 1; i <= n; i ++)cin >> c[i];
    
//     for(int i = 1; i <= n; i ++)cin >> d[i];

//     for(int i = 1; i <= n; i ++){
//         if((d[i] - b[i]) != 0 && (c[i] - a[i]) != 0){
//             x[i] = (d[i] - b[i]) * 1.0 / (c[i] - a[i]) * 1.0;
//         }
//         else if((d[i] - b[i]) == 0){
//             x[i] = 20001;
//         }
//         else if((c[i] - a[i]) == 0){
//             x[i] = 0;
//         }
//     }
//     sort(x + 1, x + n + 1);


//     for(int i = 1; i <= n; i ++){
//         for(int j = i + 1; j <= n; j ++){
//             if(x[j] != x[i])cnt ++;
//         }
//     }

//     cout << cnt - n + 1;
//     return 0;
// }
// using namespace std;

// const int N = 1e8 + 10;

// int a[N];
// int t = 0;

// int main(){
//     int n, m, k;
//     cin >> n >> m >> k;

//     for(int i = 0; i < n; i ++) cin >> a[i];

//     while(m --){
//         int x;
//         cin >> x;

//         t += x;

//         while(t < 0){
//             t += n;
//         }

//         for(int i = t; i < t + k; i ++) cout << a[i % n] << " ";
//         cout << endl;
//     }

//     return 0;
// }
// using namespace std;

// const int N = 1e8 + 10;

// int main(){
//     int n, m, k, mi = 0;
//     cin >> n >> m >> k;

//     for(int i = 0; i < m; i ++)cin >> a[i];

//     for(int i = 0; i < m; i ++){
//         int c;
//         cin >> c;
//         mi += c;
//         while(mi < 0){
//             mi = n + mi;
//         }
//         for(int j = mi; j < mi + k; j ++){
//             cout << a[j % n] << " ";
//         }
//         cout << endl;
//     }

//     return 0;
// }
// using namespace std;

// const int N = 1e6 + 10;
// const int M = 1e5 + 10;

// int n, m, k, x, y;
// long long mi;
// int a[N], b[N];

// int main(){
//     cin >> n >> m >> k;
//     for(int i = 1; i <= n; i ++) cin >> a[i];

//     while(m --){
//         cin >> mi;

//         if(mi <= 0){
//             for(int i = 1; i <= n; i ++){
//                 x = (i + mi % n);
//                 if(i + mi % n <= 0)x += n;
//                 b[i] = a[x % (n + 1)];
//             }
//         }else{
//             for(int i = 1; i <= n; i ++){
//                 y = (i + mi % n);
//                 if(y > n)y = y % n;
//                 else if(y == n)y = n;
//                 else if(y < n)y = y;
//                 b[i] = a[y];
//             }
//         }

//         for(int i = 1; i <= n; i ++)a[i] = b[i];
//         for(int i = 1; i <= k; i ++)printf("%d ", b[i]);

//         cout << endl;
//     }
//     return 0;
// }
// using namespace std;

// const int X = 1e3 + 10;

// int N, M;
// int A[X][X];
// int B[X][X];
// long long C[X][X];
// long long mx = 0;
// long long sum = 0;

// int main(){
//     cin >> N >> M;

//     for(int i = 1; i <= N; i ++){
//         for(int j = 1; j <= M; j ++){
//             cin >> A[i][j];
//         }
//     }

//     for(int i = 1; i <= N; i ++){
//         for(int j = 1; j <= M; j ++){
//             cin >> B[i][j];
//         }
//     }

//     for(int i = 1; i <= N; i ++){
//         for(int j = 1; j <= M; j ++){
//             cin >> C[i][j];
//         }
//     }

//     for(int j = 1; j <= N; j ++){
//         mx = 0;
//         for(int i = 1; i <= M; i ++){
//             if(min(A[j][i], B[j][i]) * C[j][i] >= mx)mx = min(A[j][i], B[j][i]) * C[j][i];
//         }        
//         sum += mx;
//     }

//     cout << sum;
//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// int n;
// int a[N];

// int main(){
//     cin >> n;
//     for(int i = 1; i <= n; i ++)cin >> a[i];


//     return 0;
// }
// using namespace std;

// typedef long long LL;

// const int N = 1e6 + 10;

// LL n, m;

// LL a[N];
// LL b[N];
// LL s[N];

// int main(){
//     while(cin >> n && cin >> m){

//         for(int i = 1; i <= n; i ++) cin >> a[i];

//         while(m --){
//             string str;
//             cin >> str;
//             int x, c;
//             if(str == "ADD"){
//                 cin >> x >> c;
//                 a[x] += c;
//             }
//             for(int i = 1; i <= n; i ++){
//                 s[i] = s[i - 1] + a[i];
//             }

//             if(str == "QUERY"){
//                 LL l, r;
//                 cin >> l >> r;
//                 cout << s[r] - s[l - 1] << endl;
//             }
//         }
//     }
//     return 0;
// }
// using namespace std;

// int N = 1e6 + 10;
// int n, m;
// int a[N];

// int main(){
//     cin >> n >> m;
//     for(int i = 1; i <= n; i ++){
//         cin >> a[i];
//     }

//     while(m --){
//         int sum = 0;
//         string op;
//         int x, y;

//         cin >> op >> x >> y;
//         if(op == "QUERY"){
//             for(int i = x; i <= y; i ++){
//                 sum += a[i];
//             }
//             cout << sum;
//         }

//         if(op == "ADD"){
//         }
//     }
// }
// using namespace std;

// typedef long long LL;

// const int N = 1e6 + 10;

// LL n, m;

// LL a[N];
// LL b[N];
// LL s[N];

// int main(){
// 	while(cin >> n && cin >> m){

// 		for(int i = 1; i <= n; i ++) cin >> a[i];
		
// 		while(m --){
// 			string str;
// 			cin >> str;
// 			int x ,c;
// 			if(str == "ADD"){
// 				cin >> x >> c;
// 				a[x] += c;
// 			}
// 			for(int i = 1; i <= n; i ++){
// 				s[i] = s[i - 1] + a[i];
// 			}
// 			if(str == "QUERY"){
// 				LL l, r;
// 				cin >> l >> r;
// 				cout <<	s[r] - s[l - 1] << endl;
// 			}
// 		}
// 	}
// 	return 0;
// }
// using namespace std;

// const int N = 10;

// bool col[N];
// int n;
// int ans = 1;

// void dfs(int row){

//     if(row > n) return ;
//     for(int i = 1; i <= n; i ++){
//         if(!col[i]){
//             col[i] = true;
//             ans ++;
//             dfs(row + 1);
//             col[i] = false;
//         }
//     }

//     dfs(row + 1);
// }

// int main(){
//     cin >> n;
//     dfs(1);
//     cout << ans;
//     return 0;
// }
// using namespace std;

// int N;
// long long ans = 1;
// bool visited[10];

// void dfs(int step){
//     if(step > N) return ;
//     for(int i = 1; i <= N; i ++){
//         if(!visited[i]){
//             visited[i] = true;
//             ans ++;
//             dfs(step + 1);
//             visited[i] = false;
//         }
//     }

//     dfs(step + 1);
// }

// int main(){
//     cin >> N;
//     dfs(1);
//     cout << ans;
//     return 0;
// }
// using namespace std;

// const int N = 10;

// int n, m;
// int a[N], d[N];
// int temp = 0x3f3f3f3f;
// bool st[N];
// int cnt = 1;

// void dfs(int u){
//     if(u == n){
//         int s1 = d[0], s2 = d[0];
//         for(int i = 0; i < m; i ++){
//             s1 = max(s1, d[i]);
//             s2 = min(s2, d[i]); 
//         }

//         temp = min(temp, s1 - s2);

//         return ;
//     }

//     for(int i = u; i < n; i ++){
//         for(int j = 0 ; j < m; j ++){
//             if(!st[i]){
//                 d[j] += a[i];
//                 st[i] = true;
//                 dfs(u + 1);
//                 d[j] -= a[i];
//                 st[i] = false;
//             }
//         }
//     }
// }

// int main(){
//     cin >> n >> m;
//     for(int i = 0; i < n; i ++)cin >> a[i];
//     dfs(0);
//     cout << temp;
//     return 0;
// }
// using namespace std;

// const int N = 10;

// int n, m, a[N];//读入木棍长度;
// int d[N];//用来储存每个长木棍的长度;
// int temp = 0x3f3f3f3f;
// bool st[N];

// void dfs(int u){
//     if(u == n){
//         int s1 = d[0], s2 = d[0];
//         for(int i = 0 ; i < m; i ++){
//             s1 = max(s1, d[i]);//用来更新最长木棍的长度
//             s2 = min(s2, d[i]);//用来更新最短木棍的长度
//         }
//         temp = min(temp, s1 - s2);//用来更新答案--最小差值
//         return ;
//     }

//     for(int i = u;  i < n; i ++){
//         for(int j = 0; j < m; j ++){
//             if(!st[i]){
//                 d[j] += a[i];
//                 st[i] = true;
//                 dfs(u + 1);
//                 d[j] -= a[i];
//                 st[i] = false;
//             }
//         }
//     }
// }

// int main(){
//     cin >> n >> m;
//     for(int i = 0; i < n; i ++)cin >> a[i];
//     dfs(0);
//     cout << temp;
//     return 0;
// }
// using namespace std;

// int n, r;
// vector<int> L;
// vector<int> R;

// void g(vector<int> vc){
//     sort(vc.begin(), vc.end());
//     for(int i = 0; i < vc.size(); i ++){
//         cout << vc[i] << " ";
//     }

//     cout << endl;
// }

// bool f(vector<int> L, vector<int> R, int nums[], bool flag[], int d, int r, int n){
//     if(abs(d) > r){
//         return 0;
//     }

//     if(L.size() + R.size() == n){
//         int i;
//         for(int i = 0 ; i < L.size(); i ++){
//             if(L[i] == nums[0]){
//                 g(L);
//                 break;
//             }
//         }

//         g(R);
//         if(i == L.size()){
//             g(L);
//         }

//         return 1;
//     }

//     int i;
//     for(int i = 0; i < n; i ++){
//         if(!flag[i]){
//             flag[i] = true;
//             L.push_back(nums[i]);
//             if(f(L, R, nums, flag, d + nums[i], r, n)){
//                 break;
//             }

//             L.pop_back();

//             R.push_back(nums[i]);
//             if(f(L, R, nums, flag, d - nums[i], r, n)){
//                 break;
//             }
//             R.pop_back();
//             flag[i] = false;
//         }
//     }

//     return !(i == n);
// }

// int main(){
//     cin >> n >> r;
//     int nums[n];
//     bool flag[n];

//     for(int i = 0; i < n; i ++){
//         flag[i] = 0;
//     }

//     for(int i = 0; i < n; i ++){
//         cin >> nums[i];
//     }

//     f(L, R, nums, flag, 0, r, n);

//     return 0;
// }

// using namespace std;

// int n;		//n个正整数 
// int r;		//两组数字和之差不能超过r
// vector<int> L;	//存放左组数字 
// vector<int> R;	//存放右组数字 

// //函数功能：从小到大打印数组 
// void g(vector<int> vc){
// 	sort(vc.begin(), vc.end());
// 	for(int i=0; i<vc.size(); i++){
// 		cout<<vc[i]<<" ";
// 	}
// 	cout<<endl;
// }

// //函数功能：左组数字L[], 右组数字R[],  保证两者和之差的绝对值 abs(d) < r ,  返回是否找到这样的两组数字 , 并按要求打印 
// bool f(vector<int> L, vector<int> R, int nums[], bool flag[], int d, int r, int n){
// 	//两者和之差的绝对值 abs(d) > r , 不合题意， 回溯 
// 	if(abs(d)>r){
// 		return 0;
// 	}
	
// 	//全部分完，按要求打印两组数 
// 	if(L.size() + R.size() == n){
// 		//查找第一个数是否在L中 
// 		int i;
// 		for(i=0; i<L.size(); i++){
// 			if(L[i]==nums[0]){
// 				//在L中，先打印L 
// 				g(L);
// 				break;
// 			}
// 		}
// 		g(R);
// 		//不在L中，先打印R 
// 		if(i==L.size()){
// 			g(L);
// 		}
// 		return 1;
// 	}
	
// 	int i;
// 	//遍历每一个数 
// 	for(i=0; i<n; i++){
// 		//如果该数字未被使用 , 则该数字有3种情况 
// 		if(!flag[i]){
// 			flag[i] = true;		//标记使用 
			
// 			//1.放L组 
// 			L.push_back(nums[i]);		//放进去 
// 			//如果符合要求, 结束递归 
// 			if(f(L, R, nums, flag, d+nums[i], r, n)){
// 				break;
// 			}	
// 			//不符合要求, 恢复现场 
// 			L.pop_back();		//拿出来 
			
// 			//2.放R组 
// 			R.push_back(nums[i]);       //放进去 
// 			//如果符合要求, 结束递归 
// 			if(f(L, R, nums, flag, d-nums[i], r, n)){
// 				break;
// 			}
// 			//不符合要求, 恢复现场 
// 			R.pop_back();       //拿出来 
			
// 			flag[i] = false;	//恢复现场 
			
// 			//3.都不放 
// 		}
// 	}

// 	return !(i==n);
// }


// int main(){	
// 	cin>>n>>r;
// 	int nums[n];				//存放n个数 
// 	bool flag[n];				//标记第i+1个数是否被使用 
// 	//初始化标记 
// 	for(int i=0; i<n; i++){
// 		flag[i] = 0;
// 	}
	
// 	for(int i=0; i<n; i++){
// 		cin >> nums[i];
// 	} 

// 	f(L, R, nums, flag, 0, r, n);

// 	return 0; 
// } 

// using namespace std;

// int P = 5218;

// int qmi(long long x){

//     int res = 1;
//     int a = 3;

//     while(x){
//         if(x & 1)res = res * a % 5218;
//         x >>= 1;
//         a = a * a % 5218;
//     }

//     return res;
// }

// int main(){ 

//     long long N;
//     cin >> N;

//     long long x = N % 3;
//     long long y = N / 3;

//     int z;

//     if(N == 1){
//         cout << 1;
//         return 0;
//     }

//     if(x == 0){
//         z =  qmi(y);
//     }
//     else if(x == 1){
//         z = qmi(y - 1) * 4;
//     }
//     else if(x == 2){
//         z =  qmi(y) * 2;
//     }

//     cout << z % 5218;

//     return 0;
// }
// using namespace std;

// typedef long long LL;

// int qmi(int a, int k, int p){
//     int res = 1;
//     while(k){
//         if(k & 1) res = (LL) res * a % p;
//         k >>= 1;
//         a = (LL)a * a % p;
//     }

//     return res;
// }

// int main(){
//     int n;
//     scanf("%d", &n);

//     while(n --){
//         int a, k, p;
//         scanf("%d%d%d", &a, &k, &p);

//         printf("%d\n", qmi(a, k, p));
//     }
    
//     return 0;
// }
// using namespace std;

// int cnt = 0;

// int main(){

//     int x[13] = {0, 31,28, 31,30,31,30,31,31,30,31,30,31};

//     for(int y = 1900; y <= 9999; y ++){
//         int a = y / 1000;
//         int b = y / 100 % 10;
//         int c = y / 10 % 10;
//         int d = y % 10;
//         if((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)x[2] = 29;
//         else x[2] = 28;
//         for(int m = 1; m <= 12; m ++){
//             int e = m / 10;
//             int f = m % 10;
//             for(int day = 1; day <= x[m]; day ++){
//                 int g = day / 10;
//                 int h = day % 10;
//                 if(a + b + c + d == e + f + g + h)
//                 cnt ++ ;
//             }
//         }
//     }

//     cout << cnt;
//     return 0;
// }
// using namespace std;

// int main(){
//     int n;
//     cin >> n;

//     long long dp[n + 1];
//     dp[0] = 1;

//     for(int i = 1; i < n; i ++){
//         for(int j = n; j >= i; j ++){
//             dp[j] += dp[j - i];
//         }
//     }

//     printf("%d", dp[n]);

//     return 0;
// }
// using namespace std;

// int sum = 0;

// int main(){
//     int n;
//     cin >> n;
//     for(int i = 1; i < = 100; i ++){
//         sum += i;
//         if(sum == n)cnt = i;
//         else if(sum > n) cnt = i - 1;
//         if(sum >= n){
//             printf("%d", i)
//         }
//     }

//     return 0;
// }
// using namespace std;

// const int N = 1e3 + 10;

// char a[N][N];
// int n, m;
// int cnt = 0;

// int main(){
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= m; j++){
//             cin >> a[i][j];
//         }
//     }    

//     for(int x = 2; x < n; x ++){
//         for(int y = 2; y < m; y ++){
//             for(int k = 1; k < min(n, m) ; k ++){
//                 if(a[x - k][y - k] == a[x][y] && a[x - k][y + k] == a[x][y] && a[x + k][y - k] == a[x][y] && a[x + k][y + k] == a[x][y]){
//                     cnt ++ ;
//                 }
//                 else break;
//             }
//         }
//     }

//     printf("%d", cnt);
//     return 0;
// }
// #include <unordered_map>

// int cnt = 0;
// using namespace std;
// int main(){
//     int n;
//     cin >> n;

//     unordered_map<string, int>p;

//     for(int i = 0; i < n; i ++){
//         string temp;
//         cin >> temp;
//         p[temp] ++;
//         if(p[temp] > 1){
//             cnt = 1;
//         }
//     }

//     if(cnt == 1)cout << 1;
//     else cout << 0;

//     return 0;
// }
// using namespace std;

// int N = 1e3 + 3;

// char a[N][N];
// int b[N];

// int main(){
//     int n;
//     cin >> n;

//     for(int i = 0; i < n; i ++){
//         cin >> a[i];
//         b[a[i]] ++;
//     }
    
//     for(int i = 0; i < n; i ++){
//         if(b[a[i]] > 1){
//             printf("1");
//             break;
//         }
//     }

//     printf("0");

//     return 0;
// }
// using namespace std;

// const int N = 102;

// int dp[N][N];
// int a[N][N];

// int main(){
//     int n;
//     cin >> n;

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= i; j ++){
//             cin >> a[i][j];
//         }
//     }

//     for(int i = 1; i <= n; i ++){
//         for(int j = 1; j <= i; j ++){
//             dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - 1]) + a[i][j];
//         }
//     }

//     int max = 0;
//     for(int i = 1; i <= n; i ++)if(dp[n][i] >= max)max = dp[n][i];
//     printf("%d", max);

//     return 0;
// }
// using namespace std;

// int nb[41];

// int main(){
//     int n, k;
//     cin >> n >> k;
//     int idx = -1;
//     while(n --){
//         cin >> nb[++ idx];
//     }
// }
// using namespace std;

// const int N = 1e7 + 10;

// int primer[N];
// bool st[N];
// int cnt = 0;

// void merge(int x){
//     for(int i = 2; i <= x; i ++){
//         if(!st[i])primer[++cnt] = i;

//         for(int j = 0; primer[j] <= x / i; j ++){//error 数组存储应该按顺序存储不能跳着存储（primer[0]未存储）
//             st[primer[j] * i] = true;
//             if(i % primer[j] == 0) break;
//         }
//     }

//     printf("%d", cnt);
// }

// int main(){
//     int n;
//     cin >> n;

//     merge(n);
//     return 0;
// }
// using namespace std;

// int main(){

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int prime[N], cnt = -1;
// bool st[N];

// void merge(int x){
//     for(int i = 2; i <= x; i ++){
//         if(!st[i]){
//             prime[++ cnt] = i;
//         }
//         for(int j = 0; prime[j] <= x / i; j ++){
//             st[prime[j] * i] = true;
//             if(i % prime[j] == 0) break;
//         }
//     }

//     cout << cnt;
// }

// int main(){
//     int n;
//     scanf("%d", &n);

//     merge(n);
//     return 0;
// }
// using namespace std;

// void divide(int x){
//     for(int i = 2; i <= x / i; i ++){
//         int s = 0;
//         if(x % i == 0){
//             while(x % i == 0){
//                 x /= i;
//                 s ++;
//             }

//             printf("%d %d\n", i, s);
//         }
//     }

//     if(x != 1)printf("%d %d\n", x, 1);
// }

// int main(){
//     int n;
//     scanf("%d", &n);
//     while(n --){
//         int x;
//         scanf("%d", &x);
//         divide(x);
//         puts("");
//     }

//     return 0;
// }
// using namespace std;

// int cnt = 0;

// void isPrimer(int x){
//     for(int i = 2; i <= x / i; i ++){
//         cnt = 0;
//         while(x % i == 0){
//             cnt ++ ;
//             x /= i;
//         }

//         if(cnt >= 1){
//             printf("%d %d\n", i, cnt);
//         }
//     }
//     if(x != 1) printf("%d %d\n", x, 1);
// } 

// int main(){
//     int n, x;
//     scanf("%d", &n);

//     while(n --){
//         scanf("%d", &x);
//         isPrimer(x);
//         puts("");
//     }

//     return 0;
// }
// using namespace std;

// bool isPrimer(int x){
//     if(x < 2) return false;
//     for(int i = 2; i <= x / i; i ++){
//         if(x % i == 0)
//             return false;
//     }

//     return true;
// }

// int main(){
//     int x, n;
//     scanf("%d", &x);
//     while(x --){
//         scanf("%d", &n);
//         if(isPrimer(n))printf("Yes\n");
//         else printf("No\n");
//     }

//     return 0;
// }
// using namespace std;

// typedef long long LL;

// int qmi(int a, int k, int p){
//     int res = 1;
//     while(k){
//         if(k & 1) res = (LL) res * a % p;
//         k >>= 1;
//         a = (LL)a * a % p;
//     }

//     return res;
// }

// int main(){
//     int n;
//     scanf("%d", &n);

//     while(n --){
//         int a, k, p;
//         scanf("%d%d%d", &a, &k, &p);

//         printf("%d\n", qmi(a, k, p));
//     }
    
//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int a[N];

// int lowbit(int x){
//     return x & (-x);
// }

// int main(){
//     int n;
//     cin >> n;
    
//     while(n --){
//         int x;
//         int t = 0;
//         cin >> x;

//         while(x) x -= lowbit(x), t ++;
//         cout << t << " ";
//     }
//     return 0;
// }
// using namespace std;

// const int N = 1e6 + 10;

// int n, k;
// int a[N], q[N];
// int hh, tt = -1;

// int main(){
//     cin >> n >> k;
//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) ++ hh;
//         while(hh <= tt && a[i] <= a[q[tt]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
//     puts("");
//     int hh = 0, tt = -1;

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) ++ hh;
//         while(hh <= tt && a[i] >= a[q[hh]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " "; 
//     }

//     return 0;
// }

// using namespace std;

// const int N = 1e6 + 10;

// int n, k;
// int a[N], q[N];
// int hh, tt = -1;

// int main(){
//     cin >> n >> k;
//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0 ;i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) ++ hh;//当窗口不为空集时，若窗口长度大于规定长度则窗口的首元素位置应后移一位
//         while(hh <= tt && a[i] <= a[q[tt]])-- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
//     puts("");
//     int hh = 0, tt = -1;

//     for(int i = 0; i < n; i ++){
//         if(hh <= tt && i - q[hh] + 1 > k) ++ hh;
//         while(hh <= tt && a[i] >= a[q[tt]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
    
//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int n, k;
// int a[N], q[N];
// int hh, tt = -1;

// int main(){
//     cin >> n >> k;
//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0; i < n; i ++){
//         if(i - k + 1 > q[hh]) ++ hh;//
//         while(hh <= tt && a[i] <= a[q[tt]]) -- tt;
//         q[++ tt] = i;//初始化q[N]数组
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }
//     puts("");
//     int hh = 0, tt = -1;

//     for(int i = 0; i < n; i ++){
//         if(i - k + 1 > q[hh]) ++ hh;
//         while(hh <= tt && a[i] >= a[q[tt]]) -- tt;
//         q[++ tt] = i;
//         if(i >= k - 1) cout << a[q[hh]] << " ";
//     }

//     return 0;
// }

// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int p[N];

// int find(int x){
//     if(x != p[x]) p[x] = find(p[x]);//如果x的父节点不是它本身，那么就往下继续找父节点;
//     return p[x];
// }

// int main(){
//     cin >> n >> m;
//     for(int i = 1; i <= n; i ++) p[i] = i;

//     while(m --){
//         char op;
//         int a, b;
        
//         cin >> op >> a >> b;

//         if(op == 'M') p[find(a)] = find(b);//找到a节点的源节点并与b节点的源节点合并成一个集合
//         else{
//             if(find(a) != find(b)) cout << "No" << endl;
//             else cout << "Yes" << endl;
//         }
//     }    

//     return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int p[N];//代表N节点的父节点是p[N]; 

// int find(int x){
//     if(x != p[x]) p[x] = find(p[x]);
//     return p[x];
// }

// int main(){
//     cin >> n >> m;
//     for(int i = 1; i <= n; i ++) p[i] = i;//初始化i节点的父节点

//     while(m --){
//         int a, b;
//         char op;

//         cin >> op >> a >> b;

//         if(op == 'M') p[find(a)] = find(b);
//         else{
//             if(find(a) == find(b)) cout << "Yes" << endl;
//             else cout << "No" << endl;
//         }
//     }

//     return 0;
// }

//高精度加法
// using namespace std;

// string a, b;

// vector<int> add(vector<int> A, vector<int> B){
//     vector<int> C;

//     int t = 0;

//     for(int i = 0; i < A.size() || i < B.size(); i ++){
//         t = A[i] + B[i] + t;
//         C.push_back(t % 10);
//         t /= 10;
//     }  

//     return C;
// }   

// int main(){
//     cin >> a >> b;

//     vector<int> C;
//     vector<int> A, B;

//     for(int i = a.size() - 1; i >= 0; i --) A.push_back(a[i] - '0');

//     for(int j = b.size() - 1; j >= 0; j --) B.push_back(b[i] - '0');

//     add(A, B);
// }

//蓝桥序列求和
// using namespace std;

// int main(){
//     unsigned long long n;
//     cin >> n;
//     cout << (1 + n) * n / 2;
//     return 0;
// }
//蓝桥算法训练--斐波那契
// using namespace std;

// const int N = 1e6 + 10;

// int ne[N];

// int main(){
//     int n, m;
//     cin >> n >> m;

//     for(int i = 1; i <= n; i ++) cin >> ne[i];

//     while(m --){
//         int b, c;
//         cin >> b >> c;
        
//         while(c --){
//             b = ne[b];
//         }

//         cout << b << " ";
//     }
//     return 0;
// }
//蓝桥算法训练--最小距离
// using namespace std;

// const int N = 1e6 + 10;

// int a[N];

// int main(){
//     int n;
//     cin >> n;

//     for(int i = 0; i < n; i ++) cin >> a[i];

//     int min = 0x3f3f3f3f;
    
//     sort(a, a + n - 1);

//     for(int i = 0; i < n - 1; i ++)
//         if(abs(a[i + 1] - a[i]) <= min)
//             min = abs(a[i + 1] - a[i]);
//     // for(int i = 0; i < n - 1; i ++){
//     //     for(int j = i + 1; j < n; j ++){
//     //         if(abs(a[j] - a[i]) <= min) min = abs(a[j] - a[i]);
//     //     }
//     // }

//     cout << min;
//     return 0;
// }
//acwing 二进制中1的个数
// #include<iostream>
// #include<vector>
// #include<algorithm>

// using namespace std;

// int n;

// //lowbit函数得到数字二进制最后一位1所代表的十进制数
// int lowbit(int x){
//     return x & (-x);
// }
// int main(){
//     cin >> n;

//     while(n --){
//         cnt = 0;

//         int x;
//         cin >> x;

//         while(x) x -= lowbit(x), cnt ++;

//         cout << cnt << " ";
//     }

//     return 0;
// }
//acwing 双指针-1
// #include<iostream>
// #include<vector>
// #include<algorithm>

// using namespace std;

// const int N = 1e6 + 10;

// int n, res;
// int a[N], s[N];

// int main(){
//     cin >> n;
//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0, j = 0; i < n; i ++){
//         s[a[i]] ++;
        
//         //j卡在倒数第二个重复数字的后面
//         while(s[a[i]] > 1){
//             // if(j <= i){
//             //     s[a[j]] --;
//             //     j ++;
//             // }
//              s[a[j]] --;
//                 j ++;
//         }

//         res = max(res, i - j + 1);
//     }

//     cout << res;

//     return 0;
// }
// #include<iostream>
// #include<vector>
// #include<algorithm>

// using namespace std;

// const int N = 1e6 + 10;

// int n, res;
// int a[N], s[N];

// int main(){
//     cin >> n;
//     for(int i = 0; i < n; i ++) cin >> a[i];

//     for(int i = 0, j = 0; i < n; i ++){
//         s[a[i]] ++;
//         while(s[a[i]] > 1){
//             if(j <= i){
//                 s[a[j]] --;
//                 j ++;
//             }
//         }

//         res = max(res, i - j + 1);

//         cout << res <<  "----" << endl;
//     }

//     cout << res;

//     return 0;
// }
//蓝桥基础矩阵方程
// using namespace std;

// int main(){
//     int a[31][31];
//     int b[31][31];
//     int c[31][31] = {0};
//     int N, M;
//     cin >> N >> M;  

//     for(int i = 0; i < N; i ++){
//         for(int j = 0; j < N; j ++){
//             cin >> a[i][j];
//             b[i][j] = a[i][j];
//         }
//     }

//     if(M > 0){
//         while(M - 1){
//             for(int i = 0; i < N; i ++){
//                 for(int j = 0; j < N; j ++){
//                     c[i][j] = 0;
//                     for(int k = 0; k < N; k ++){
//                         c[i][j] += a[i][k] * b[k][j];
//                     }
//                 }
//             }

//             if(M > 1){
//                 for(int i = 0; i < N; i ++){
//                     for(int j = 0; j < N; j ++){
//                         a[i][j] = c[i][j];
//                     }
//                 }
//             }

//             M --;
//         }

//         for(int i = 0; i < N; i ++){
//             for(int j = 0; j < N; j ++){
//                 cout << a[i][j] << " ";
//             }
//             puts(" ");
//         }
//     }
//     else if(M == 0){
//         for(int i = 0; i < N; i ++){
//             for(int j = 0; j < N; j ++){
//                 if(i == j){
//                     c[i][j] = 1;
//                 }

//                 cout << c[i][j] << " ";
//             }

//             puts(" ");
//         }
//     }
// }
//
//蓝桥提高最小字符串

// using namespace std;

// const int N = 1e6 + 10;
// string a[N];

// int main(){
//     int t;
//     cin >> t;

//     while(t --){
//         int x;
//         cin >> x;

//         for(int i = 0; i < x; i ++) cin >> a[i];

//         for(int i = 1; i <= x; i ++){
//             for(int j = 0; j <= x - i - 1; j ++){
//                 if(strcmp(a[j], a[j + 1]) == -1) swap(a[j], a[j + 1]);
//             }
//         }

//         for(int i = 0; i < x; i ++) cout << a[i];

//         puts(" ");
//     }
//     return 0;
// }
// using namespace std;
// const int N = 1e6 + 10;
// int a[N];
// int main(){
//     int n;
//     cin >> n;

//     for(int i = 0; i <= n - 1; i ++) cin >> a[i];

//     for(int i = 1; i <= n - 1; i ++){
//         for(int j = 0; j <= n - i - 1; j ++){
//             if(a[j] >= a[j + 1]) swap(a[j], a[j + 1]);
//         }
//     }

//     for(int i = 0; i < n; i ++) cout << a[i] << " ";

//     return 0;
// }
// using namespace std;

// const int N = 1e6 + 10; 
// int a[N];

// int main(){
//     int n;
//     cin >> n;

//     for(int i = 0; i <= n - 1; i ++){
//         cin >> a[i];
//     }

//     for(int i = 1; i <= n - 1; i ++){
//         for(int j = 0; j <= n - i - 1; j ++){
//             if(a[j] >= a[j + 1])swap(a[j], a[j + 1]);

//         }
//     }
    
//     for(int i = 0; i <= n - 1; i ++){
//         cout << a[i] << " ";
//     }
//     return 0;
// }
//洛谷 P2021 faebdc玩扑克 
//思路:我们假设原序列是有序的则经过洗牌后得到的答案就是无序的，而我们本应该得到的洗牌后序列是按顺序的
//那么就应该1号牌与乱序得到的一号牌换顺序后面的牌同理，那么我们本来假设的序列就会变成乱序(即我们要求的序列)
//pop----队头元素出队，或栈头元素出栈

// using namespace std;

// const int N = 1e6 + 10;

// int a[N];
// int st[N];
// int prime[N];
// int cnt = 0;
// int main(){
// 	int n, m;
// 	cin >> n >> m;

// 	while(n--){
// 		cnt = 0;
// 		int l, r;
// 		cin >> l >> r;
// 		if(r > m){
// 			cout << "Crossing the line" << endl;;
// 			continue;
// 		}

// 		for(int i = 2; i <= n; i ++){
// 			if(!st[i])prime[cnt ++ ] = i, st[i] = 1;
// 			for(int j = 0; prime[j] <= n / i; j ++){
// 				st[prime[j] * i] = 1;
// 				if(i % prime[j] == 0) break;
// 			}
// 		}

// 		cout << cnt - 1 << endl;
// 	}
// 	return 0;
// }
// using namespace std;

// int a[103][103];
// int b[103][103];

// int main(){
// 	int n, m;
// 	cin >> n >> m;

// 	for(int i = 1; i <= n; i ++){
// 		for(int j = 1; j <= m; j ++){
// 			cin >> a[i][j];
// 		}
// 	}

// 	for(int i = 1; i <= n; i ++){
// 		for(int j = 1; j <= m; j ++){
// 			b[i][j] += a[i][j] + a[i][j - 1] + a[i - 1][j];			
// 			cout << b[i][j] << " ";
// 		}

// 		puts("");
// 	}
// 	return 0;
// }
//取整游戏 -- 50 %
// using namespace std;

// int p[10][10];
// int st[10][10];
// int n, m;
// int sum = 0;
// int mx;

// //应该dfs括号里就控制x和y的坐标而不是dfs里写个循环坐标
// void dfs(int x, int y){
// // void dfs(int x, int a, int b){


// 	// for(int i = x; i < n; i ++){
// 	// 	for(int j = y; j < m; j ++){
// 	// 		if(!st[i][j]){
// 	// 			for(int k = i - 1; k <= i + 1; k ++){
// 	// 				for(int l = j - 1; l <= j + 1; l ++){
// 	// 					st[k][l] ++;//周围数字都变为不可取状态
// 	// 				}
// 	// 			}
				
// 	// 			sum += p[i][j];
// 	// 			if(sum >= mx) mx = sum;

// 	// 			if(j + 1 >= m) dfs(x + 1, 0);
// 	// 			else dfs(x, j + 1);

// 	// 			for(int k = i - 1; k <= i + 1; k ++){
// 	// 				for(int l = j - 1; l <= j + 1; l ++){
// 	// 					// st[k][l] = 0;//有问题，会将上一个dfs的数变为可取状态，会进入死循环
// 	// 					st[k][l] --;//将取过一次的数回档
// 	// 				}
// 	// 			}

// 	// 			sum -= p[i][j];
// 	// 		}
// 	// 	}
// 	// }
// }

// int main(){
// 	int t;
// 	cin >> t;

// 	while(t --){
// 		mx = 0;
// 		cin >> n >> m;

// 		for(int i = 0; i < n; i ++){
// 			for(int j = 0; j < m; j ++){
// 				cin >> p[i][j];
// 			}
// 		}

// 		dfs(0, 0);
// 		// dfs(1, 0, 0);
// 		cout << mx << endl;
// 	}

// 	return 0;
// }
//天使的起誓
// using namespace std;

// vector<int>div(int n, vector<int> B, int &p){
// 	vector<int> C;

// 	int t = 0;

// 	for(int i = 0; i <= B.size() - 1; i ++){
// 		t = t * 10 + B[i];
// 		C.push_back(t / n);
// 		t %= n;
// 	}

// 	p = t;

// 	reverse(C.begin(), C.end());

// 	while(C.size() > 1 && C.back() == 0) C.pop_back();

// 	return C;
// }

// int main(){
// 	int n, p = 0;
// 	string m;
// 	cin >> n >> m;

// 	vector<int> B;

// 	for(int i = m.size() - 1; i >= 0; i --) B.push_back(m[i] - '0');

// 	reverse(B.begin(), B.end());

// 	auto C = div(n, B, p);

// 	if(p == 0) cout << n;//判断天使手上的数是否是最后一位
// 	else cout << p;

// 	return 0;
// }

//纸牌游戏
// using namespace std;

// const int N = 100;

// int a[N];
// bool st[1003];
// int main(){
// 	int m, n;
// 	cin >> m >> n;

// 	int ans = n;

// 	for(int i = 1; i <= n; i ++){
// 		cin >> a[i];
// 		st[a[i]] = 1;//判断是否用过a[i]这张牌
// 	}

// 	sort(a + 1, a + n + 1);
// 	for(int j = n; j >= 1; j --){
// 		int i = a[j] + 1;
// 		while(i <= n * m){
// 			if(!st[i]){
// 				st[i] = 1;
// 				ans --;
// 				break;
// 			}
// 			i ++;
// 		}
// 	}

// 	cout << ans;
// 	return 0;
// }
//2038年问题
// using namespace std;

// typedef long long LL;

// int x, y, m, d, h, ms, s;

// int main(){
// 	int a[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
// 	int ss = 86400;
// 	int t;
// 	int sd, res;//天，秒
// 	cin >> t;
// 	while(t --){
// 		cin >> x >> y >> m >> d >> h >> ms >> s;
// 		//相应进制下秒数的最大值	
// 		LL sms = pow(2, x - 1) - 1;

// 		//求剩余的秒数，来求出小时，分钟，秒数
// 		res = sms % ss;

// 		int i = 1;
// 		while(i --){

// 			s += res % 60;//秒

// 			if(s >= 60){
// 				s -= 60;
// 				m +=1;
// 			}
// 			ms += res % 3600 / 60;//分
			
// 			if(ms >= 60){
// 				ms -= 60;
// 				h += 1;
// 			}
// 			h += res / 3600;//小时
// 		}

// 		//求该进制下有多少个完整的天数,来求出年份，月份，日期
// 		sd = sms / ss;
		
// 		int flag;
// 		if(h >= 24){
// 			h -= 24;
// 			d += 1;
// 		}
// 		while(sd){
// 			//判断是否是闰年
// 			if((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) a[2] = 29;
// 			else a[2] = 28;

// 			while(sd){
// 				flag = 0;
// 				sd --;
// 				d ++;
// 				if(d > a[m]){
// 					flag = 1;
// 					d = 1;
// 					break;
// 				}
// 			}
// 			if(flag == 1) m ++;
// 			if(m > 12){
// 				m = 1;
// 				y ++;
// 			}
// 		}

// 		cout << y << " " << m << " " << d << " " << h << " " << ms << " " << s << endl;
// 	}

// 	return 0;
// }
// using namespace std;

// const int N = 1e4 + 10;

// int a[N];

// int main(){
// 	int n;
// 	cin >> n;
// 	for(int i = 0; i < n; i ++) cin >> a[i];

// 	//cout << a[i];
// 	int ans = 0;
// 	for(int k = 1; k <= n - 1; k ++){
// 		for(int j = 0; j < n - k; j++){
// 			if(a[j + 1] <= a[j]){
// 				swap(a[j], a[j + 1]);
// 				ans ++;
// 			}
// 		}
// 	}

// 	cout << ans;

// 	return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int a[N];

// int main(){
// 	int n, dist;
// 	cin >> n >> dist;

// 	for(int i = 1; i <= n; i ++) cin >> a[i];

// 	sort(a + 1, a + n + 1);
// 	int mx = 0;
// 	int ans;

// 	for(int i = 1; i <= n; i ++) cout << a[i] << " ";
// 	puts("");
// 	for(int i = 1; i <= n; i ++){
// 		ans = -1;
// 		for(int j = i + 1; j <= n; j ++){
// 			if(abs(a[j] - a[i]) <= dist){
// 				sort(a + 1, a + n + 1);
// 				ans ++;
// 				if(ans >= mx) mx = ans;
// 			}
// 			else break;
// 		}
// 	}

// 	cout << mx;

// 	return 0;
// }
// using namespace std;

// unsigned long long n;

// bool isSame(unsigned long long x, unsigned long long y){
// 	int t = 0;
// 	while(x){
// 		if((x % 10 * y + t) % 10 != 1 && (x % 10 * y + t) % 10 != 2){return false;}
// 		x /= 10;
// 		t = (x % 10 * y) / 10 % 10;
// 	}

// 	return true;
// }

// int main(){
// 	cin >> n;

// 	for(unsigned long long i = 1; i < n; i ++){
// 		if(isSame(n, i)){
// 			cout << i;
// 			return 0;
// 		}
// 	}

// 	cout << "No Solution";

// 	return 0;
// }
// using namespace std;

// typedef pair<int, int> PII;

// const int N = 53;

// int a[N];
// int b[N];

// int main(){
// 	int g;
// 	cin >> g;

// 	while(g --){
// 		int x0 = 0, y0 = 0;
// 		int flag = 1;
// 		int n;
// 		cin >> n;
// 		vector<PII> ans;
// 		ans.push_back({0, 0});
// 		for(int i = 1; i <= n; i++) cin >> a[i] >> b[i],ans.push_back({b[i], a[i]});

// 		sort(ans.begin(), ans.end());

// 		for(int i = 0; i < n; i ++){
// 			// cout << ans[i + 1].second - ans[i].second << "--" << ans[i + 1].first - ans[i].first << endl;
// 			if(abs(ans[i + 1].second - ans[i].second) > abs(ans[i + 1].first - ans[i].first)){
// 				cout << "Notabletocatch" << endl;
// 				flag = 0;
// 				break;
// 			}
// 		}

// 		if(flag) cout << "Abletocatch" << endl;

// 	} 

// 	return 0;
// }
// const int N = 1e5 + 10;

// int a[N];//对方母舰防御系统数量
// int b[N];//我方母舰攻击系统数量

// int main(){
// 	int sa = 0, sb = 0;
// 	int m, n;
// 	cin >> m >> n;

// 	for(int i = 1; i <= m; i ++) cin >> a[i];

// 	for(int j = 1; j <= n; j ++) cin >> b[j], sb += b[j];

// 	sort(a + 1, a + m + 1);
// 	sort(b + 1, b + n + 1);

// 	if(m > n){
// 		cout << 0;
// 		return 0;
// 	}
// 	else{
// 		int k = 1;
// 		for(int i = 1; i <= m; i ++){
// 			int flag = 0;//未破甲
// 			for(int j = k; j <= n; j ++){
// 				if(b[j] <= a[i]) continue;
// 				else{
// 					flag = 1;//已破甲
// 					sb -= b[j];
// 					k = j + 1;
// 					break;
// 				}
// 			}
// 			if(!flag){
// 				cout << 0;
// 				return 0;
// 			}
// 		}
// 	}

// 	cout << sb;

// 	return 0;
// }
// using namespace std;

// const int N = 1003;

// int x[N];

// int main(){
// 	int ans = 0;
// 	int n, m;
// 	cin >> n >> m;

// 	while(m --){
// 		int a, b;
// 		cin >> a >> b;

// 		x[a] ++;
// 		x[b] ++;
// 	}

// 	for(int i = 1; i <= n;  i++){
// 		if(x[i] & 1 == 1){
// 			ans ++;
// 		}
// 	}

// 	if(ans){
// 		cout << ans / 2;
// 	}
// 	else{
// 		cout << ans + 1;
// 	}

// 	return 0;
// }
// const int N = 1e6 + 10;

// queue <int> a;

// int sc[N], ans[N];

// int n;

// int main(){
// 	cin >> n;

// 	for(int i = 1; i <= n; i ++) a.push(i);

// 	for(int i = 1; !a.empty(); i ++){
// 		a.push(a.front());
// 		a.pop();
// 		sc[i] = a.front();
// 		a.pop();
// 	}

// 	for(int i = 1; i <= n; i ++) ans[sc[i]] = i;

// 	for(int i = 1; i <= n; i ++)  cout << ans[i] << " ";

// 	return 0;
// }
// using namespace std;

// typedef unsigned long long LL;

// const int N = 1e9;
// int fb[100];


// void fib(){
// 	fb[0] = {1};
// 	fb[1] = {1};
	
// 	int i = 2;

// 	while(i){
// 		if(fb[i - 1] >= N){
// 			i --;
// 			break;
// 		}
// 		fb[i] = fb[i - 1] + fb[i - 2];
// 		i ++;
// 	}
// }

// int main(){
// 	int t;
// 	cin >> t;

// 	fib();

// 	while(t --){
// 		int x;
// 		cin >> x;

// 		stack<LL> s;

// 		printf("%d=", x);
		
// 		for(int i = 44; i >= 0; i --){
// 			while(x >= fb[i]){
// 				s.push(fb[i]);
// 				x -= fb[i];
// 			}

// 			if(x == 0) break;
// 		}

// 		while(s.size()){
// 			if(s.size() == 1){
// 				cout << s.top();
// 			}
// 			else{
// 				cout << s.top() << "+";
// 			}
// 			s.pop();
// 		}

// 		puts("");
// 	}

// 	return 0;
// }
// int main(){
// 	int x1, y1;
// 	cin >> x1 >> y1;
// 	int x2, y2;
// 	cin >> x2 >> y2;

// 	double k = (y1 - y2) * 1.0 / (x1 - x2);

// 	double b = (((x1 - x2) * y1 * 1.0 - ((y1 - y2) * x1) * 1.0) / (x1 - x2) * 1.0);

// 	printf("y=");
// 	if(k < 0){
// 		printf("-");
// 	}
// 	if(k == 0){
// 		printf("0");
// 		return 0;
// 	}
// 	if((y1 - y2) % (x1 - x2) == 0){
// 		printf("%dx", abs((y1 - y2) / (x1 - x2)));
// 	}
// 	else{
// 		printf("%d/%d*x", abs(y1 - y2), abs(x1 - x2));
// 	}
// 	if((int)(b) == b){
// 		if(b > 0){
// 			printf("+%d", abs((int)(b)));
// 			return 0;
// 		}
// 		if(b < 0){
// 			printf("-%d",abs((int)(b)));
// 			return 0;
// 		}
// 	}
// 	else{
// 		if(b > 0){
// 			printf("+%d/%d", abs((x1 - x2) * y1 - ((y1 - y2) * x1)), abs((x1 - x2)));
// 			return 0;
// 		}
// 		if(b < 0){
// 			printf("-%d/%d",abs((x1 - x2) * y1 - ((y1 - y2) * x1)), abs((x1 - x2)));
// 			return 0;
// 		}
// 	}

// 	return 0;
// }
// using namespace std;

// #define PI 3.1415926535

// const int N = 1e5 + 10;

// int x[N], y[N];

// double mxd, mnr = 0x3f3f3f3f;

// int main(){
// 	int n;
// 	cin >> n;

// 	for(int i = 1; i <= n; i ++) cin >> x[i] >> y[i];
	
// 	for(int i = 1; i <= n; i ++){//选一个点作为放伞点
// 		mxd = 0;//用来更新根点到最远点的距离
// 		for(int j = 1; j <= n; j ++){//遍历另一个点
// 			if(j == i) continue;
// 			double d = sqrt((x[j] - x[i]) * (x[j] - x[i]) + (y[j] - y[i]) * (y[j] - y[i]));
// 			//int d = (x[j] - x[i]) * (x[j] - x[i]) + (y[j] - y[i]) * (y[j] - y[i]);
// 			mxd = max(mxd, d);

// 		}

// 		mnr = min(mnr, mxd);

// 	}

// 	printf("%.4lf", (double)PI * mnr * mnr);

// 	return 0;
// }
// #define PI 3.1415926

// using namespace std;

// int main(){
// 	printf("%.2f", 2 * PI * (2 + 2 * sqrt(2)));
// 	return 0;
// }
// using namespace std;

// const int N = 10003;

// int a[N];

// int main(){
// 	int n, sum = 0;
// 	cin >> n;

// 	for(int i = 0; i < n; i ++) cin >> a[i];

// 	for(int i = 0, j = n - 1; i <= j; i ++, j --){
// 		sum += max(a[i], a[j]);
// 	}

// 	cout << sum;
// 	return 0;
// }
// using namespace std;

// int n, m;

// int a[10003];

// int main(){
// 	int sa = 0, sb = 0;
// 	vector<int> A, B;

// 	scanf("%d%d", &n, &m);

// 	for(int i = 0; i < n; i ++) cin >> a[i];

// 	A.push_back(a[0]);
// 	sa += a[0];

// 	for(int i = 1; i < n; i ++) {
// 		if(sa + a[i] - m >= sb || sb + a[i] - m >= sa){
// 			sa += a[i];
// 			A.push_back(a[1]);
// 		}
// 		else{
// 			sb += a[i];
// 			B.push_back(a[i]);
// 		}
// 	}
// 	return 0;
// }
// using namespace std;
// using namespace std;

// const int N = 1e5 + 10;

// int l[N], r[N];
// int a[N];
// int n, rr;
// int sa = 0, sb = 0;

// int main(){
// 	int k = -1, m = 0;

// 	scanf("%d%d", &n, &rr);

// 	for(int i = 0; i < n; i ++) cin >> a[i];

// 	sort(a, a + n);
// 	int i, j = n - 1;
// 	r[0] = a[n - 1];
// 	int sb = a[n - 1];
// //	cout << sb << "sd";
// 	for(i = 0; i < n - 1; i ++){
// 		l[++ k] = a[i];
// 		sa += l[k];

// 		while(j > i){
// 			//cout << m << "------" << endl;
// 			if(sb - 10 >= sa){
// 				break ;
// 			}
// 			else if(sa - 10 >= sb){
// 				j --;
// 				r[++ m] = a[j];
// 				sb += r[m];
// 				continue ;
// 			}
// 			else{
// 				if(sa >= sb){
// 					j --;
// 					r[++ m] = a[j];
// 					sb += r[m];
// 					continue ;
// 				}
// 				else{
// 					break ;
// 				}
// 			}
// 		}
// 	}

// 	for(int i = 0; i <= k; i ++) cout << l[i] << " ";

// 	puts(" ");

// 	for(int j = 0; j <= m; j ++) cout << r[j] << " ";

// 	return 0;
// }
// using namespace std;

// const int N = 5003;
// const int X = 1e3 + 10;
// int a[N];
// int b[N][X];

// int main(){
// 	int n;
// 	cin >> n;

// 	int flag = 0;

// 	for(int i = 1; i <= n; i ++) cin >> a[i];

// 	for(int j = 1; j <= a[n]; j ++){
// 		for(int i = 1; i <= n; i ++){
// 			b[a[i] % j][j] ++;
// 			if(b[a[i] % j][j] >= 2){
// 				flag = 1;
// 				break ;
// 			}
// 		}

// 		if(flag == 0){
// 			cout << j;
// 			return 0;
// 		}
// 	}
// 	return 0;
// }

//洛谷 UVA307

// #include<iostream>
// #include<cstdio>
// #include<cstring>
// #include<algorithm>
// using namespace std;
// int len,n;
// bool v[101];
// int a[101];
// int cnt,sum,val;
// //stick 即正在拼第stick根木棒(确保前面的都拼好了)
// //第stick根木棒的当前长度为cab
// //拼第stick根木棒的上一根小木棒为last(有些小朋友可能会有疑问，为什么第一次搜索是dfs(1,0,1)而不是dfs(1,0,0)首先，如果dfs(1,0,0)也能过并且更正确一些，而即使dfs(1,0,1)在bfs中的第三个if中，因为v[i]==1也不会进入)
// bool dfs(int stick,int cab,int last)
// {
// 	if(stick>cnt)
// 	  return true;//所有的木棒都已经拼好
// 	if(cab==len)
// 	  return dfs(stick+1,0,1); //当前的一根已经拼好，开始下一根
// 	int fail=0;//第二个剪枝开始了：对于每根木棒，fail记录的是最近一次尝试拼接的木棍长度。这样再回溯时就不会再尝试相同长度的木棍。
// 	for(int i=last;i<=n;i++)//第三个剪枝开始了：限制先后加入一根原始木棍的长度是递减的。因为先拼上一个长为x的木棍再拼上一个长为y的木棍，等效于先拼上一个长为y的木棍再拼上一个长为x的木棍。所以只需要搜索其中一种即可。
// 	{
// 		if(v[i]==0&&a[i]+cab<=len&&fail!=a[i])
// 		{
// 			v[i]=1;
// 			if(dfs(stick,cab+a[i],i))
// 			  return true;
// 		    v[i]=0;//还原搜索前的状态。
// 			fail=a[i];
// 			if(cab==0||cab+a[i]==len)
// 			  return false;//第四个剪枝开始了:如果在一根原始木棒中尝试拼接的第一根木棍的递归分支就以失败返回，直接判断当前分支无解。与此同时，第五个剪枝开始了，如果两个木棍的长度和与一个木棍的一样，只尝试一个的就行了(因为前两个可能会有更大的效用)
// 		}
// 	}
// 	return false;//所有分支都尝试过，搜索失败。
// }
// int main()
// {
// 	while(cin>>n&&n)
// 	{
// 		sum=0;
// 		val=0;
// 		for(int i=1;i<=n;i++)
// 		{
// 			scanf("%d",&a[i]);
// 			sum+=a[i];//为啥要累加尼？为了求原始木棒的根数，即sum/len(len是枚举的答案)
//             val=max(val,a[i]);
// 		}
// 	    sort(a+1,a+n+1);//第一个剪枝开始了：此为优化搜索顺序，优先尝试较长的木棍
// 	    reverse(a+1,a+n+1);//sort从小到大排序，而reverse会将整个数组翻转(懒人做法)，这样就可以达到从大到小排的结果
// 	    for(len=val;len<=sum;len++)
// 	    {
// 	    	 if(sum%len)
// 	    	   continue;
// 	    	 cnt=sum/len;
// 	    	 memset(v,0,sizeof(v));
// 	    	 if(dfs(1,0,1))//(1,0,1具体是啥，看上方的dfs)
// 	    	   {
// 	    	    cout<<len<<endl;
// 	    	   	break;//确定枚举正确的第一次即为答案，立即结束
// 	    	   }   
// 	    }
// 	}
//         return 0;
// } 

// #include<bits/stdc++.h>

// using namespace std;

// const int N = 1e8 + 10;

// typedef long long LL;

// int n, m;

// bool st[N];
// int prime[N];

// int main(){
// 	while (~scanf("%d%d", &n, &m)){
// 		LL res = n;
// 		int cnt = 1;

// 		for(int i = 2; i <= n / i; i ++){
// 			if(!st[i]) prime[i] ++;
// 			for(int j = 0; prime[j] <= n / i; j ++){
// 				st[prime[j] * i] = 1;
// 				if(i % prime[j] == 0) break;
// 			}
// 		}
// 		if(m == 1){
// 			cout << cnt;
// 			continue ;
// 		}
// 		for(int i = 2; i <= n / i; i ++){
// 			if(n % i == 0){
// 				while(n % i == 0){
// 					n /= i;
// 				}
// 			}
// 			else{
// 				if(prime[i]){
// 					cnt ++;
// 					if(cnt == m){
// 						cout << i << endl;
// 						break ;
// 					}
// 				}
// 			}
// 		}

// 	}

// 	return 0;
// }
// #include<bits/stdc++.h>

// typedef long long LL;

// using namespace std;

// int main(){
// 	int n;
// 	scanf("%d", &n);

// 	while(n--){
// 		int x;
// 		scanf("%d", &x);

// 		LL res = x;

// 		for(int i = 2; i <= x / i; i ++){
// 			if(x % i == 0){
// 				res = res / i * (i - 1);
// 				while(x % i == 0){
// 					x /= i;
// 				}
// 			}
// 		}

// 		if(x > 1) res = res / x * (x - 1);

// 		cout << res << endl;
// 	}

// 	return 0;
// }

// #include<bits/stdc++.h>

// typedef long long LL;

// using namespace std;

// int main(){
// 	int n;
// 	scanf("%d", &n);

// 	while(n --){
// 		int x;
// 		scanf("%d", &x);

// 		LL res = x;
// 		for(int i = 2; i <= x / i; i ++){
// 			if(x % i == 0){
// 				res = res / i * (i - 1);
// 				while(x % i == 0){
// 					x /= i;
// 				}
// 			}
// 		}

// 		if(x > 1) res = res / x * (x - 1);

// 		cout << res << endl;
// 	}
// 	return 0;
// }
// #include<bits/stdc++.h>

// using namespace std;

// typedef long long LL;

// const int N = 1e8 + 10;

// LL n;
// LL m;

// int prime[N];
// bool st[N];
// int main(){
// 	while (~scanf("%d%d", &n, &m)){

// 		prime[0] = 1;
// 		int t = -1;	
// 		for(int i = 2; i <= n; i ++){
// 			if(!st[i])prime[++t] = i;
// 			for(int j = 0; prime[j] <= n / i; j ++){
// 				st[prime[j] * i] = true;
// 				if(i % prime[j] == 0) break;
// 			}
// 		}
		
// //		for(int i = 0; i <= t; i ++) cout << prime[i] << endl;
// 		int cnt = 0;
// 		if(m == 1){
// 			cout << 1 << endl;
// 		}
// 		else {
// 			cnt = 1;
// 			for(int i = 1; i <= t; i ++){
// 				if(n % prime[i] != 0){
// 					cnt ++;
// 					if(cnt == m){
// 						cout << prime[i] << endl;
// 						break;
// 					}
// 				}
// 			}
// 		}
// 	}
// 	return 0;
// }
// #include<bits/stdc++.h>

// using namespace std;

// const int N = 20;

// int n;

// char g[N][N];
// bool col[N], dg[N], udg[N];

// void dfs(int u){
// 	if(u == n){
// 		for(int i = 0; i < n; i ++) puts(g[i]);
// 		puts(" ");
// 		return ;
// 	}

// 	for(int i = 0; i < n; i ++){
// 		if(!col[i] && !dg[u + i] && !udg[n - u + i]){
// 			g[u][i] = 'Q';
// 			col[i] = dg[u + i] = udg[n - u + i] = true;
// 			dfs(u + 1);
// 			col[i] = dg[u + i] = udg[n - u + i] = false;
// 			g[u][i] = '.';
// 		}
// 	}
// }

// int main(){
// 	cin >> n;

// 	for(int i = 0; i < n; i ++){
// 		for(int j = 0; j < n; j ++){
// 			g[i][j] = '.';
// 		}
// 	}

// 	dfs(0);

// 	return 0;
// }

// #include<bits/stdc++.h>

// const int N = 20;

// int n;
// char g[N][N];
// bool col[N], dg[N], udg[N];

// void dfs(int u){
// 	if(u == n){
// 		for(int i = 0; i < n; i ++) puts(g[i]);
// 		puts(" ");
// 		return ;
// 	}

// 	for(int i = 0; i < n; i ++){
// 		if(!col[i] && dg[u + i] && udg[n - u + i]){
// 			g[u][i] = 'Q';
// 			col[i] = dg[u + i] = udg[n - u + i] = true;
// 			dfs(u + 1);
// 			col[i] = dg[u + i] = udg[n - u + i] = false;
// 			g[u][i] = '.';
// 		}
// 	}
// }

// int main(){
// 	cin >> n;

// 	for(int i = 0 ; i < n; i ++)
// 		for(int j = 0 ; j < n; j ++)
// 			g[i][j] = '.';

// 	dfs(0);

// 	return 0;
// }
// #include<bits/stdc++.h>
// using namespace std;

// const int N = 1e6 + 10;

// int n;
// int path[N];//走的哪条
// int st[N];//走没走过

// void dfs(int u){
// 	if(u == n){
// 		for(int i = 0; i < n ; i ++) cout << path[i] << " ";
// 		puts(" ");
// 		return ;
// 	}

// 	for(int i = 1; i <= n; i ++){
// 		if(!st[i]){
// 			st[i] = 1;
// 			path[u] =  i;
// 			dfs(u + 1);
// 			st[i] = 0;
// 		}
// 	}
// }

// int main(){
// 	cin >> n;

// 	dfs(0);
// 	return 0;
// }
// using namespace std;

// const int N = 1e6 + 10;

// int n;
// int path[N];
// bool st[N];

// void dfs(int x){
// 	if(x == n){
// 		for(int i = 0 ; i < n; i ++) cout << path[i] << " ";
// 		puts(" ");
// 		return ;
// 	}

// 	for(int i = 1; i <= n; i ++){
// 		if(!st[i]) {
// 			st[i] = 1;
// 			path[x] = i;
// 			dfs(x + 1);
// 			st[i] = 0;
// 		}
// 	}
// }

// int main(){
	
// 	cin >> n;

// 	dfs(0);
// 	// for(int i = 1; i <= n; i ++){
// 	// 	if(!st[i]) st[i] = 1;
// 	// 	dfs(i);
// 	// 	st[i] = 0;
// 	// }

// 	return 0;
// }
// typedef long long LL;

// const int N = 1e6 + 10;

// LL n, m;

// LL a[N];
// LL b[N];
// LL s[N];

// int main(){
// 	while(cin >> n && cin >> m){

// 		for(int i = 1; i <= n; i ++) cin >> a[i];
		
// 		while(m --){
// 			string str;
// 			cin >> str;
// 			int x ,c;
// 			if(str == "ADD"){
// 				cin >> x >> c;
// 				a[x] += c;
// 			}
// 			for(int i = 1; i <= n; i ++){
// 				s[i] = s[i - 1] + a[i];
// 			}
// 			if(str == "QUERY"){
// 				LL l, r;
// 				cin >> l >> r;
// 				cout <<	s[r] - s[l - 1] << endl;
// 			}
// 		}
// 	}
// 	return 0;
// }
// int main(){
// 	int N;
// 	cin >> N;

// 	cout << N;
// }
// const int N = 1e6 + 10;

// int a[N];

// int main(){
// 	int n;
// 	cin >> n;
// 	vector<int> cnt;
// 	int sum = 0;
// 	while(n != 1){
// 		int k = -1;
// 		for(int i = 1; i <= n / i; i ++){
// 			if(n % i == 0){
// 				a[++ k] = i;
// 				//cnt.push_back(i);
// 				if(n / i != i){
// 					a[++ k] = n / i;
// 					// cnt.push_back(n / i);
// 				}
// 			}
// 			// sort(cnt.begin(), cnt.end());
// 		}

// 		sort(a, a + k + 1);
		
// 		n = a[k - 1];
// 		sum += n;
// 	}

// 	cout << sum;
// 	return 0;
// }
// int n, m;

// int a[8];
// int b[10];

// int main(){
// 	cin >> n >> m;

// 	for(int i = 1; i <= n; i ++) cin >> a[i];

// 	for(int i = 1; i < n; i ++) b[i] = a[i + 1] - a[i];	

// 	sort(b, b + n - 1);

	
// }
// int main(){
// 	int n;
// 	cin >> n;


// 	while(n --){
// 		int x;
// 		cin >> x;

// 		vector<int> cnt;
		
// 		for(int i = 1; i <= x / i; i ++){
// 			if(x % i == 0){
// 				cnt.push_back(i);
// 				if(x / i != i){
// 					cnt.push_back(x / i);
// 				}
// 			}
// 			// while(x % i == 0){
// 			// 	cnt.push_back(i);
// 			// 	if(x / i != i)
// 			// 	cnt.push_back(x / i);
// 			// 	x /= i;
// 			// }

// 		}

// 		sort(cnt.begin(), cnt.end());

// 		for(auto item : cnt){
// 			cout << item << " ";
// 		}
// 		puts("");
// 	}
// 	return 0;
// }
// typedef long long LL;

// const int mod = 1e9 + 7;

// int main(){
// 	int n;
// 	cin >> n;

// 	unordered_map<int, int>Hasp;

// 	while(n --){
// 		int x;
// 		cin >> x;

// 		for(int i = 2; i <= x / i; i ++){
// 			while(x % i == 0){
// 				x /= i;
// 				Hasp[i] ++;
// 			}
// 		}

// 		if(x > 1) Hasp[x] ++;

// 	}

// 	LL res = 1;

// 	for(auto prime : Hasp){
// 		int a = prime.second;
// 		res = res * (a + 1) % mod;
// 	}

// 	cout << res;
// 	return 0;
// }
// const int mod = 1e9 + 7;

// int n;

// int main(){
// 	cin >> n;
	
// 	vector<int> cnt;

// 	while(n --){
// 		int x;
// 		cin >> x;

// 		for(int i = 2; i <= x / i; i ++){
// 			while(x % i == 0){
// 				cnt.push_back(i);
// 				if(x / i != i)
// 				cnt.push_back(x / i);
// 				x /= i;
// 			}
// 		}

// 		if(x > 1) cnt.push_back(x);

// 		cout << cnt.size() << "-----" << endl;
// 	}

// 	cout << cnt.size();

// 	return 0;
// }
// typedef long long LL;

// const int mod = 1e9 + 7;

// int n;

// int main(){
// 	cin >> n;

// 	unordered_map<int, int>Hasp;

// 	while(n --){
// 		int x;
// 		cin >> x;

// 		for(int i = 2; i <= x / i; i ++){
// 			while(x % i == 0){
// 				x /= i;
// 				Hasp[i] ++;
// 			}
// 		}

// 		if(x > 1) Hasp[x] ++;

// 	}

// 	LL res = 1;

// 	for(auto prime : Hasp){
// 		int b = prime.second;

// 		res = (res * (b + 1)) % mod;
// 	}

// 	cout << res;

// 	return 0;
// }
// typedef long long LL;

// const int mod = 1e9 + 7;

// int main(){
// 	IOS;
// 	int n;
// 	cin >> n;

// 	unordered_map<int, int> Hasp;
	
// 	while(n--){
// 		int x;
// 		cin >> x;

// 		for(int i = 2; i <= x / i; i ++){
// 			while(x % i == 0){
// 				x /= i;
// 				Hasp[i] ++;
// 			}
// 		}

// 		if(x > 1) Hasp[x] ++;
// 	}

// 	LL res = 1;

// 	for(auto prime : Hasp){
// 		LL t = 1;
// 		int p = prime.first, x = prime.second;
// 		while(x --) t = (p * t + 1) % mod;
// 		res = res * t % mod;
// 	}

// 	cout << res;

// 	return 0;
// }
// typedef long long LL;

// const int mod = 1e9 + 7;

// int main(){
// 	int n;
// 	cin >> n;

// 	unordered_map<int, int> Hasp;

// 	while(n--){
// 		int x;
// 		cin >> x;
// 		for(int i = 2; i <= x; i ++){
// 			while(x % i == 0){
// 				x /= i;
// 				Hasp[i] ++;
// 			}
// 		}

// 		if(x > 1) Hasp[x] ++;
// 	}

// 	LL res = 1;

// 	for(auto prime : Hasp){
// 		int p = prime.first, a = prime.second;
// 		LL t = 1;

// 		while(a--) t = (p * t + 1) % mod;
// 		res = res * t % mod;
// 	}

// 	cout << res << endl;

// 	return 0;
// }
// using namespace std;

// int N, M;

// int a[103][103];
// int b[103][103];
// int c[103][103];

// int main(){
// 	cin >> N >> M;

// 	for(int i = 1; i <= N; i ++){
// 		for(int j = 1; j <= N; j ++){
// 			cin >> a[i][j];
// 			b[i][j] = a[i][j];
// 			c[i][j] = a[i][j];
// 		}
// 	}
// 	M --;
// 	while(M--){
// 		// for(int i = 1; i <= 2; i ++){
// 		// 	for(int j = 1; j <= 2; j ++){
// 		// 		b[i][j] = b[i][j] * a[i][j] + b[i][2] * b[2][];
// 		// 	}
// 		// }	
// 		b[1][1] = c[1][1] * a[1][1] + c[1][2] * a[2][1];
// 		b[1][2] = c[1][1] * a[1][2] + c[1][2] * a[2][2];
// 		b[2][1] = c[2][1] * a[1][1] + c[2][2] * a[2][1];
// 		b[2][2] = c[2][1] * a[1][2] + c[2][2] * a[2][2];
// 		c[1][1] = b[1][1];
// 		c[1][2] = b[1][2];
// 		c[2][1] = b[2][1];
// 		c[2][2] = b[2][2];
// 	}

// 	for(int i = 1; i <= N; i ++){
// 		for(int j = 1; j <= N; j ++){
// 			cout << b[i][j] << " ";
// 		}

// 		puts("");
// 	}
// 	return 0;
// }
// using namespace std;

// typedef long long LL;

// const int x = 903;

// int N, M;

// LL a[x][x];

// int main(){
// 	cin >> N >> M;

// 	for(int i = 1; i <= N; i ++){
// 		for(int j = 1; j <= N; j ++){
// 			cin >> a[i][j];
// 		}
// 	}

// 	for(int i = 1; i <= N; i ++){
// 		for(int j = 1; j <= N; j ++){
// 			cout << pow(a[i][j], M);
// 		}
// 	}
	
// 	return 0;
// }
// using namespace std;

// int n;

// vector<int>mul(vector<int>A, int b){
// 	vector<int> C;

// 	int t = 0;

// 	for(int i = 0; i < A.size() || t; i ++){
// 		if(i < A.size()) t = A[i] * b + t;
// 		C.push_back(t % 10);
// 		t /= 10;
// 	}

// 	//while(C.size() > 1 && C.back() == 0) C.pop_back();

// 	return C;
// }

// int main(){
// 	cin >> n;

// 	vector<int> C;
// 	C.push_back(1);

// 	for(int i = 1; i <= n; i ++){
// 		C = mul(C, i);
// 	}

// 	for(int i = C.size() - 1; i >= 0; i --) cout << C[i]; 

// 	return 0;
// }

// using namespace std;

// string a, b;

// vector<int>add(vector<int>A, vector<int>B){
// 	vector<int> C;

// 	int t = 0;
// 	for(int i = 0; i < A.size() || i < B.size(); i ++){
// 		if(i < A.size()) t += A[i];
// 		if(i < B.size()) t += B[i];
// 		C.push_back(t % 10);
// 		t /= 10;
// 	}

// 	if(t) C.push_back(t);

// 	while(C.size() > 1 && C.back() == 0) C.pop_back();

// 	return C;
// }

// int main(){
// 	cin >> a >> b;

// 	vector<int> C;
// 	vector<int> A, B;

// 	for(int i = a.size() - 1; i >= 0; i --) A.push_back(a[i] - '0');
	
// 	for(int i = b.size() - 1; i >= 0; i --) B.push_back(b[i] - '0');

// 	C = add(A, B);

// 	for(int i = C.size() - 1; i >= 0; i --) cout << C[i];

// 	return 0;
// }
// using namespace std;

// int n;
// string a;

// int main(){
// 	int sum = 0;
// 	int x = 1;
// 	cin >> n;
// 	while(n--){
// 		getline(cin, a);

// 		for(int i = a.size() - 1; i >= 0; i ++){
// 			sum += (int)(a[i]) * x;
// 			x *= 16;
// 		}


// 	}

// 	return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int p[N];
// int n, m;

// int find(int x){
// 	if(p[x] != x) p[x] = find(p[x]);
// 	return p[x];
// }

// int main(){
// 	cin >> n >> m;
// 	for(int i = 1; i <= n ; i ++) p[i] = i;

// 	while(m--){
// 		char op;
// 		int a, b;
// 		cin >> op >> a >> b;

// 		if(op == 'M') p[find(a)] = find(b);//?
// 		else {
// 			if(find(a) != find(b)) cout << "No" << endl;
// 			else{
// 				cout << "Yes" << endl;
// 			}
// 		}
// 	}

// 	return 0;
// }
// using namespace std;

// int main(){

// 	return 0;
// }
// using namespace std;

// char a[27];

// string s1, s2, s3, s4;

// int main(){
// 	getline(cin,s1);
//     getline(cin,s2);
//     getline(cin,s3);
//     getline(cin,s4);
//     //读入数据
//     for(int i=0;i<=s1.size();i++){
//         if(s1[i]>='A'&&s1[i]<='Z'){
//             a[s1[i]-'A'+1]++;
//         }
//     }
//     for(int i=0;i<=s2.size();i++){
//     	if(s2[i]>='A'&&s2[i]<='Z'){
//            a[s2[i]-'A'+1]++;
//     	}
//     }
//     for(int i=0;i<=s3.size();i++){
//         if(s3[i]>='A'&&s3[i]<='Z'){
//             a[s3[i]-'A'+1]++;
//         }
//     }
//     for(int i=0;i<=s4.size();i++){
//         if(s4[i]>='A'&&s4[i]<='Z'){
//             a[s4[i]-'A'+1]++;
//         }
//     }
// //	char ch;
// //	for(int i = 1; i <= 4;  i++)
// //		while((ch = getchar()) != '\n'){
// //			if(ch <= 'Z' && ch >= 'A')
// //				a[ch - 'A' + 1] ++;
// //		}

// 	int mx = 0;

// 	for(int i = 0; i < 26; i ++){
// 		if(a[i] > mx){
// 			mx = a[i];
// 		}
// 	}

// 	for(int i = mx; i >= 1; i --){
// 		for(int j = 1; j <= 26; j ++){
// 			if(a[j] >= i){
// 				a[j] --;
// 				cout << "*";
// 				if(j != 26) cout << " ";
// 			}
// 			else{
// 				cout << " ";
// 				if(j != 26) cout << " ";
// 			}
// 		}
// 		cout << endl;
// 	}

// 	for(int i = 1; i <= 26; i ++){
// 		cout << (char)('A' + i - 1);
// 		if(i != 26) cout << " ";
// 	}
	
// 	return 0;
// }
// using namespace std;

// const int N = 103;

// const int M = 1003;

// int n, m;
// string a[M][N];

// int main(){
// 	int flag = 0;

// 	cin >> n >> m;
// 	for(int i = 1; i <= m; i ++)
// 		for(int j = 1; j <= n; j ++)
// 			cin >> a[i][j];
	
// 	int i, j;
// 	for(j = 1; j <= n; j ++){
// 		flag = 0;
// 		for(i = 1; i < m; i ++){
// 			//cout << a[i][j] << "--" << a[i + 1][j] << "----";
// 			if(a[i][j] != a[i + 1][j]){
// 				flag = 1;
// 				break;
// 			}
// 		}
// 		if(flag == 0) cout << a[i][j] << " ";
// 		else cout << "* ";
// 	}

// 	return 0;
// }
// int n, m;
// int p[N];

// int main(){
// 	cin >> n >> m;


// 	return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int p[N];

// int find(int x){
// 	if(p[x] != x) p[x] = find(p[x]);
// 	return p[x];
// }

// int main(){
// 	cin >> n >> m;
// 	for(int i = 0; i < n; i ++) p[i] = i;

// 	while(m--){
// 		int a, b;
// 		char op;

// 		cin >> op >> a >> b;

// 		if(op == 'M') p[find(a)] = find(b);
// 		else{
// 			if(find(a) == find(b)) cout << "Yes" << endl;
// 			else cout << "No" << endl;
// 		} 
// 	}

// 	return 0;
// }
// using namespace std;

// const int N = 1e5 + 10;

// int n, m;
// int p[N];

// int find(int x){
// 	if(p[x] != x) p[x] = find(p[x]);
// 	return p[x];
// }

// int main(){
// 	cin >> n >> m;
// 	for(int i = 1; i <= n; i ++) p[i] = i;

// 	while(m--){
// 		int x, y;
// 		char op;
		
// 		cin >> op >> x >> y;

// 		if(op == 'M') p[find(x)] = find(y);
// 		else{
// 			if(find(x) == find(y)) cout << "Yes" << endl;
// 			else cout << "No" << endl;
// 		}
// 	}
// 	return 0;
// }
// using namespace std;

// const int N = 1e6 + 10;

// int n, m;
// int p[N];

// int find(int x){
// 	if(p[x] != x) p[x] = find(p[x]);
// 	return p[x];
// }

// int main(){
// 	cin >> n >> m;
// 	for(int i = 1; i <= n; i ++) p[i] = i;

// 	while(m--){
// 		char op;
// 		int a, b;
// 		cin >> op >> a >> b;

// 		if(op == 'M')p[find(a)] = find(b);
// 		else{
// 			if(find(a) == find(b)) cout << "Yes" << endl;
// 			else cout << "No" << endl;
// 		}
// 	}

// 	return 0;
// }
// using namespace std;

// int lowbit(int x){
// 	return x & -x;	
// }

// int main(){
// 	int n, cnt;
// 	cin >> n;
	
// 	while(n--){
// 		cnt = 0;
		
// 		int x;
// 		cin >> x;
		
// 		while(x) x -= lowbit(x), cnt ++;
	
// 		cout << cnt << " ";
// 	}
		
// 	return 0;
// }